<?php  defined("C5_EXECUTE") or die("Access Denied."); $ps = Loader::helper('form/page_selector'); ?>


<style>
	.hidden{
		display: none;
	}
	.carousel_form_holder{
		border: 1px solid black;
		margin-bottom: 20px;
		height: 46px;
		overflow: hidden;
	}
	.carousel_form_holder.open{
		height: initial;
	}
	.holder_contents{
		padding: 1em;
	}
	.carousel_form_holder textarea{
		display: block;
		width: 100%;
		height: 100px;
	}
	.carousel_form_holder input[type="text"]{
		display: block;
		width: 100%;
	}
	.carousel_form_holder label{
		display: block;
	}
	.expander{
		width: 100%;
	    background-color: #e2e2e2 !important;
	    padding: 0.5em;
	    display: block;
	    font-weight: 600;
	    font-size: 1.2em;
	}
</style>

<div class="holder_holder">
	<div class="carousel_form_holder sortable open" data-item="1">
		<a href="#" class="expander">Item 1 <span>Click to Collapse</span></a>
		<div class="holder_contents">
			<div class="form-group">
				<?php 
			    if (isset($photo1) && $photo1 > 0) {
			        $photo1_o = File::getByID($photo1);
			        if (!is_object($photo1_o)) {
			            unset($photo1_o);
			        }
			    } ?>
			    <?php  echo $form->label('photo1', t("Item 1 Image")); ?>
			    <?php  echo isset($btFieldsRequired) && in_array('photo1', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
			    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-photo1-' . Core::make('helper/validation/identifier')->getString(18), $view->field('photo1'), t("Choose Image"), $photo1_o); ?>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 1 Copy</label>
			   <textarea name="copy1" id="copy1"><?php echo $copy1; ?></textarea>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 1 Link</label>
				<input type="text" name="link1" id="link1" value="<?php echo $link1; ?>">
			</div>
			
			<div class="form-group">
				<label class="control-label">Item 1 Type of Link (Leave Unchecked if No Link)</label>
				<input type="radio" name="type1" value="1" <?php if ($type1 == 1){ echo "checked"; } ?>> URL (inline)<br>
				<input type="radio" name="type1" value="2" <?php if ($type1 == 2){ echo "checked"; } ?>> Video (modal)
			</div>
			<a href="#" class="pull-right btn btn-danger deleteitem">Remove Item</a>
		</div>
	</div>
	
	<div class="carousel_form_holder sortable <?php if (!empty($copy2)){echo "open"; } else {echo "hidden";} ?>" data-item="2">
		<a href="#" class="expander">Item 2 <span>Click to <?php if (empty($copy2)){echo "Expand"; } else {echo "Collapse"; } ?></span></a>
		<div class="holder_contents">
			<div class="form-group">
				<?php 
			    if (isset($photo2) && $photo2 > 0) {
			        $photo2_o = File::getByID($photo2);
			        if (!is_object($photo2_o)) {
			            unset($photo2_o);
			        }
			    } ?>
			    <?php  echo $form->label('photo2', t("Item 2 Image")); ?>
			    <?php  echo isset($btFieldsRequired) && in_array('photo2', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
			    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-photo2-' . Core::make('helper/validation/identifier')->getString(28), $view->field('photo2'), t("Choose Image"), $photo2_o); ?>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 2 Copy</label>
			   <textarea name="copy2" id="copy2"><?php echo $copy2; ?></textarea>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 2 Link</label>
				<input type="text" name="link2" id="link2" value="<?php echo $link2; ?>">
			</div>
			
			<div class="form-group">
				<label class="control-label">Item 2 Type of Link (Leave Unchecked if No Link)</label>
				<input type="radio" name="type2" value="1"  <?php if ($type2 == 1){ echo "checked"; } ?>> URL (inline)<br>
				<input type="radio" name="type2" value="2"  <?php if ($type2 == 2){ echo "checked"; } ?>> Video (modal)
			</div>
			<a href="#" class="pull-right btn btn-danger deleteitem">Remove Item</a>
		</div>
	</div>
	
	<div class="carousel_form_holder sortable <?php if (!empty($copy3)){echo "open"; } else {echo "hidden";} ?>" data-item="3">
		<a href="#" class="expander">Item 3 <span>Click to <?php if (empty($copy3)){echo "Expand"; } else {echo "Collapse"; } ?></span></a>
		<div class="holder_contents">
			<div class="form-group">
				<?php 
			    if (isset($photo3) && $photo3 > 0) {
			        $photo3_o = File::getByID($photo3);
			        if (!is_object($photo3_o)) {
			            unset($photo3_o);
			        }
			    } ?>
			    <?php  echo $form->label('photo3', t("Item 3 Image")); ?>
			    <?php  echo isset($btFieldsRequired) && in_array('photo3', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
			    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-photo3-' . Core::make('helper/validation/identifier')->getString(38), $view->field('photo3'), t("Choose Image"), $photo3_o); ?>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 3 Copy</label>
			   <textarea name="copy3" id="copy3"><?php echo $copy3; ?></textarea>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 3 Link</label>
				<input type="text" name="link3" id="link3" value="<?php echo $link3; ?>">
			</div>
			
			<div class="form-group">
				<label class="control-label">Item 3 Type of Link (Leave Unchecked if No Link)</label>
				<input type="radio" name="type3" value="1" <?php if ($type3 == 1){ echo "checked"; } ?>> URL (inline)<br>
				<input type="radio" name="type3" value="2" <?php if ($type3 == 3){ echo "checked"; } ?>> Video (modal)
			</div>
			<a href="#" class="pull-right btn btn-danger deleteitem">Remove Item</a>
		</div>
	</div>
	
	<div class="carousel_form_holder sortable <?php if (!empty($copy4)){echo "open"; } else {echo "hidden";} ?>" data-item="4">
		<a href="#" class="expander">Item 4 <span>Click to <?php if (empty($copy4)){echo "Expand"; } else {echo "Collapse"; } ?></span></a>
		<div class="holder_contents">
			<div class="form-group">
				<?php 
			    if (isset($photo4) && $photo4 > 0) {
			        $photo4_o = File::getByID($photo4);
			        if (!is_object($photo4_o)) {
			            unset($photo4_o);
			        }
			    } ?>
			    <?php  echo $form->label('photo4', t("Item 4 Image")); ?>
			    <?php  echo isset($btFieldsRequired) && in_array('photo4', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
			    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-photo4-' . Core::make('helper/validation/identifier')->getString(48), $view->field('photo4'), t("Choose Image"), $photo4_o); ?>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 4 Copy</label>
			   <textarea name="copy4" id="copy4"><?php echo $copy4; ?></textarea>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 4 Link</label>
				<input type="text" name="link4" id="link4" value="<?php echo $link4; ?>">
			</div>
			
			<div class="form-group">
				<label class="control-label">Item 4 Type of Link (Leave Unchecked if No Link)</label>
				<input type="radio" name="type4" value="1" <?php if ($type4 == 1){ echo "checked"; } ?>> URL (inline)<br>
				<input type="radio" name="type4" value="2" <?php if ($type4 == 2){ echo "checked"; } ?>> Video (modal)
			</div>
			<a href="#" class="pull-right btn btn-danger deleteitem">Remove Item</a>
		</div>
	</div>
	
	<div class="carousel_form_holder sortable <?php if (!empty($copy5)){echo "open"; } else {echo "hidden";} ?>" data-item="5">
		<a href="#" class="expander">Item 5 <span>Click to <?php if (empty($copy5)){echo "Expand"; } else {echo "Collapse"; } ?></span></a>
		<div class="holder_contents">
			<div class="form-group">
				<?php 
			    if (isset($photo5) && $photo5 > 0) {
			        $photo5_o = File::getByID($photo5);
			        if (!is_object($photo5_o)) {
			            unset($photo5_o);
			        }
			    } ?>
			    <?php  echo $form->label('photo5', t("Item 5 Image")); ?>
			    <?php  echo isset($btFieldsRequired) && in_array('photo5', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
			    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-photo5-' . Core::make('helper/validation/identifier')->getString(58), $view->field('photo5'), t("Choose Image"), $photo5_o); ?>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 5 Copy</label>
			   <textarea name="copy5" id="copy5"><?php echo $copy5; ?></textarea>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 5 Link</label>
				<input type="text" name="link5" id="link5" value="<?php echo $link5; ?>">
			</div>
			
			<div class="form-group">
				<label class="control-label">Item 5 Type of Link (Leave Unchecked if No Link)</label>
				<input type="radio" name="type5" value="1" <?php if ($type5 == 1){ echo "checked"; } ?>> URL (inline)<br>
				<input type="radio" name="type5" value="2" <?php if ($type5 == 2){ echo "checked"; } ?>> Video (modal)
			</div>
			<a href="#" class="pull-right btn btn-danger deleteitem">Remove Item</a>
		</div>
	</div>
	
	<div class="carousel_form_holder sortable <?php if (!empty($copy6)){echo "open"; } else {echo "hidden";} ?>" data-item="6">
		<a href="#" class="expander">Item 6 <span>Click to <?php if (empty($copy6)){echo "Expand"; } else {echo "Collapse"; } ?></span></a>
		<div class="holder_contents">
			<div class="form-group">
				<?php 
			    if (isset($photo6) && $photo6 > 0) {
			        $photo6_o = File::getByID($photo6);
			        if (!is_object($photo6_o)) {
			            unset($photo6_o);
			        }
			    } ?>
			    <?php  echo $form->label('photo6', t("Item 6 Image")); ?>
			    <?php  echo isset($btFieldsRequired) && in_array('photo6', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
			    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-photo6-' . Core::make('helper/validation/identifier')->getString(68), $view->field('photo6'), t("Choose Image"), $photo6_o); ?>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 6 Copy</label>
			   <textarea name="copy6" id="copy6"><?php echo $copy6; ?></textarea>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 6 Link</label>
				<input type="text" name="link6" id="link6" value="<?php echo $link6; ?>">
			</div>
			
			<div class="form-group">
				<label class="control-label">Item 6 Type of Link (Leave Unchecked if No Link)</label>
				<input type="radio" name="type6" value="1" <?php if ($type6 == 1){ echo "checked"; } ?>> URL (inline)<br>
				<input type="radio" name="type6" value="2" <?php if ($type6 == 2){ echo "checked"; } ?>> Video (modal)
			</div>
			<a href="#" class="pull-right btn btn-danger deleteitem">Remove Item</a>
		</div>
	</div>
	
	<div class="carousel_form_holder sortable <?php if (!empty($copy7)){echo "open"; } else {echo "hidden";} ?>" data-item="7">
		<a href="#" class="expander">Item 7 <span>Click to <?php if (empty($copy7)){echo "Expand"; } else {echo "Collapse"; } ?></span></a>
		<div class="holder_contents">
			<div class="form-group">
				<?php 
			    if (isset($photo7) && $photo7 > 0) {
			        $photo7_o = File::getByID($photo7);
			        if (!is_object($photo7_o)) {
			            unset($photo7_o);
			        }
			    } ?>
			    <?php  echo $form->label('photo7', t("Item 7 Image")); ?>
			    <?php  echo isset($btFieldsRequired) && in_array('photo7', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
			    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-photo7-' . Core::make('helper/validation/identifier')->getString(78), $view->field('photo7'), t("Choose Image"), $photo7_o); ?>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 7 Copy</label>
			   <textarea name="copy7" id="copy7"><?php echo $copy7; ?></textarea>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 7 Link</label>
				<input type="text" name="link7" id="link7" value="<?php echo $link7; ?>">
			</div>
			
			<div class="form-group">
				<label class="control-label">Item 7 Type of Link (Leave Unchecked if No Link)</label>
				<input type="radio" name="type7" value="1" <?php if ($type7 == 1){ echo "checked"; } ?>> URL (inline)<br>
				<input type="radio" name="type7" value="2" <?php if ($type7 == 2){ echo "checked"; } ?>> Video (modal)
			</div>
			<a href="#" class="pull-right btn btn-danger deleteitem">Remove Item</a>
		</div>
	</div>
	
	<div class="carousel_form_holder sortable <?php if (!empty($copy8)){echo "open"; } else {echo "hidden";} ?>" data-item="8">
		<a href="#" class="expander">Item 8 <span>Click to <?php if (empty($copy8)){echo "Expand"; } else {echo "Collapse"; } ?></span></a>
		<div class="holder_contents">
			<div class="form-group">
				<?php 
			    if (isset($photo8) && $photo8 > 0) {
			        $photo8_o = File::getByID($photo8);
			        if (!is_object($photo8_o)) {
			            unset($photo8_o);
			        }
			    } ?>
			    <?php  echo $form->label('photo8', t("Item 8 Image")); ?>
			    <?php  echo isset($btFieldsRequired) && in_array('photo8', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
			    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-photo8-' . Core::make('helper/validation/identifier')->getString(88), $view->field('photo8'), t("Choose Image"), $photo8_o); ?>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 8 Copy</label>
			   <textarea name="copy8" id="copy8"><?php echo $copy8; ?></textarea>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 8 Link</label>
				<input type="text" name="link8" id="link8" value="<?php echo $link8; ?>">
			</div>
			
			<div class="form-group">
				<label class="control-label">Item 8 Type of Link (Leave Unchecked if No Link)</label>
				<input type="radio" name="type8" value="1" <?php if ($type8 == 1){ echo "checked"; } ?>> URL (inline)<br>
				<input type="radio" name="type8" value="2" <?php if ($type8 == 2){ echo "checked"; } ?>> Video (modal)
			</div>
			<a href="#" class="pull-right btn btn-danger deleteitem">Remove Item</a>
		</div>
	</div>
	
	<div class="carousel_form_holder sortable <?php if (!empty($copy9)){echo "open"; } else {echo "hidden";} ?>" data-item="9">
		<a href="#" class="expander">Item 9 <span>Click to <?php if (empty($copy9)){echo "Expand"; } else {echo "Collapse"; } ?></span></a>
		<div class="holder_contents">
			<div class="form-group">
				<?php 
			    if (isset($photo9) && $photo9 > 0) {
			        $photo9_o = File::getByID($photo9);
			        if (!is_object($photo9_o)) {
			            unset($photo9_o);
			        }
			    } ?>
			    <?php  echo $form->label('photo9', t("Item 9 Image")); ?>
			    <?php  echo isset($btFieldsRequired) && in_array('photo9', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
			    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-photo9-' . Core::make('helper/validation/identifier')->getString(98), $view->field('photo9'), t("Choose Image"), $photo9_o); ?>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 9 Copy</label>
			   <textarea name="copy9" id="copy9"><?php echo $copy9; ?></textarea>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 9 Link</label>
				<input type="text" name="link9" id="link9" value="<?php echo $link9; ?>">
			</div>
			
			<div class="form-group">
				<label class="control-label">Item 9 Type of Link (Leave Unchecked if No Link)</label>
				<input type="radio" name="type9" value="1" <?php if ($type9 == 1){ echo "checked"; } ?>> URL (inline)<br>
				<input type="radio" name="type9" value="2" <?php if ($type9 == 2){ echo "checked"; } ?>> Video (modal)
			</div>
			<a href="#" class="pull-right btn btn-danger deleteitem">Remove Item</a>
		</div>
	</div>
	
	<div class="carousel_form_holder sortable <?php if (!empty($copy10)){echo "open"; } else {echo "hidden";} ?>" data-item="10">
		<a href="#" class="expander">Item 10 <span>Click to <?php if (empty($copy10)){echo "Expand"; } else {echo "Collapse"; } ?></span></a>
		<div class="holder_contents">
			<div class="form-group">
				<?php 
			    if (isset($photo10) && $photo10 > 0) {
			        $photo10_o = File::getByID($photo10);
			        if (!is_object($photo10_o)) {
			            unset($photo10_o);
			        }
			    } ?>
			    <?php  echo $form->label('photo10', t("Item 10 Image")); ?>
			    <?php  echo isset($btFieldsRequired) && in_array('photo10', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
			    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-photo10-' . Core::make('helper/validation/identifier')->getString(108), $view->field('photo10'), t("Choose Image"), $photo10_o); ?>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 10 Copy</label>
			   <textarea name="copy10" id="copy10"><?php echo $copy10; ?></textarea>
			</div>
			
			<div class="form-group">
			    <label class="control-label">Item 10 Link</label>
				<input type="text" name="link10" id="link10" value="<?php echo $link10; ?>">
			</div>
			
			<div class="form-group">
				<label class="control-label">Item 10 Type of Link (Leave Unchecked if No Link)</label>
				<input type="radio" name="type10" value="1" <?php if ($type10 == 1){ echo "checked"; } ?>> URL (inline)<br>
				<input type="radio" name="type10" value="2" <?php if ($type10 == 2){ echo "checked"; } ?>> Video (modal)
			</div>
			<a href="#" class="pull-right btn btn-danger deleteitem">Remove Item</a>
		</div>
	</div>
</div>

<input type="hidden" name="order_boxes" id="order_boxes">

<a href="#" class="pull-right btn btn-primary additem">Add New Item</a>

<script>
	$(document).ready(function() {
		$("body").on("click",".btn.additem",function(e) {
			e.preventDefault();
			$(".carousel_form_holder.hidden").eq(0).removeClass("hidden");
		});
		$("body").on("click",".carousel_form_holder .expander",function(e) {
			e.preventDefault();
			if ($(this).parent(".carousel_form_holder").hasClass("open")){
				$(this).parent(".carousel_form_holder").removeClass("open");
				$(this).children("span").text("Click to Expand");
			} else {
				$(".carousel_form_holder").removeClass("open");
				$(".carousel_form_holder").children("a").children("span").text("Click to Expand");
				$(this).parent(".carousel_form_holder").addClass("open");
				$(this).children("span").text("Click to Collapse");
			}
		});

		$("body").on("click",".btn.deleteitem",function(e) {
			e.preventDefault();
			numActive = 10 - $(".carousel_form_holder.hidden").length;
			if (numActive < 2){
				alert("You cannot delete the only remaining item!")
			} else {
				elem = $(this).parents(".carousel_form_holder.open");
				boxNum = $(this).parent(".holder_contents").parent(".carousel_form_holder").data("item");
				elem.find("a.expander span").text("Click to Expand");
				elem.find("a.expander").text("Item "+boxNum);
				elem.find("input[name='photo"+boxNum+"']").val("");
				elem.find("input[name='photo"+boxNum+"']").parent(".ccm-file-selector-file-selected").addClass("ccm-file-selector-file-selected").html('<input type="hidden" name="photo'+boxNum+'" value="0"> Choose Image').removeClass("ccm-file-selector-file-selected");
				elem.find("input[name='photo"+boxNum+"']").siblings(".ccm-file-selector-file-selected-thumbnail").remove();
				elem.find("input[name='photo"+boxNum+"']").siblings(".ccm-file-selector-file-selected-title").remove();
				elem.find("#copy"+boxNum+"").val("").text("");
				elem.find("#link"+boxNum+"").val("").text("");
				elem.find("input[name='type"+boxNum+"']").prop('checked', false);
				elem.removeClass("open").addClass("hide_box").addClass("hidden");
				elem.insertAfter($(".holder_holder .carousel_form_holder:last"));
 				reorderDiv();
			}
		});
		$( ".holder_holder" ).sortable({
			stop         : function(event,ui){ 
				reorderDiv();
			}
		});
		function reorderDiv(){
			var divOrder = [];
			$('.carousel_form_holder').each(function() {
				divOrder.push($(this).attr("data-item"));
			});
			orderString = divOrder + "";
			$("#order_boxes").val(orderString).text(orderString);
			console.log(divOrder);
		}
	});
</script>