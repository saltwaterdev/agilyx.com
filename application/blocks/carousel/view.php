<?php  defined("C5_EXECUTE") or die("Access Denied."); 
	
?>

<div class="carousel">
	<?php if ($photo1) { ?>
		<div class="item active <?php if ($type1 == 2){ echo "video"; } ?>" data-item="1">
			<a href="<?php echo $link1; ?>" <?php if ($type1 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo1){echo $photo1->getURL();} ?>)"></a>
			<?php if ($type1 == 2){ ?><a href="<?php echo $link1; ?>" <?php if ($type1 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy1; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo2) { ?>
		<div class="item <?php if ($type2 == 2){ echo "video"; } ?>" data-item="2">
			<a href="<?php echo $link2; ?>" <?php if ($type2 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo2){echo $photo2->getURL();} ?>)"></a>
			<?php if ($type2 == 2){ ?><a href="<?php echo $link2; ?>" <?php if ($type2 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy2; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo3) { ?>
		<div class="item <?php if ($type3 == 2){ echo "video"; } ?>" data-item="3">
			<a href="<?php echo $link3; ?>" <?php if ($type3 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo3){echo $photo3->getURL();} ?>)"></a>
			<?php if ($type3 == 2){ ?><a href="<?php echo $link3; ?>" <?php if ($type3 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy3; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo4) { ?>
		<div class="item <?php if ($type4 == 2){ echo "video"; } ?>" data-item="4">
			<a href="<?php echo $link4; ?>" <?php if ($type4 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo4){echo $photo4->getURL();} ?>)"></a>
			<?php if ($type4 == 2){ ?><a href="<?php echo $link4; ?>" <?php if ($type4 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy4; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo5) { ?>
		<div class="item <?php if ($type5 == 2){ echo "video"; } ?>" data-item="5">
			<a href="<?php echo $link5; ?>" <?php if ($type5 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo5){echo $photo5->getURL();} ?>)"></a>
			<?php if ($type5 == 2){ ?><a href="<?php echo $link5; ?>" <?php if ($type5 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy5; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo6) { ?>
		<div class="item <?php if ($type6 == 2){ echo "video"; } ?>" data-item="6">
			<a href="<?php echo $link6; ?>" <?php if ($type6 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo6){echo $photo6->getURL();} ?>)"></a>
			<?php if ($type6 == 2){ ?><a href="<?php echo $link6; ?>" <?php if ($type6 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy6; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo7) { ?>
		<div class="item <?php if ($type7 == 2){ echo "video"; } ?>" data-item="7">
			<a href="<?php echo $link7; ?>" <?php if ($type7 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo7){echo $photo7->getURL();} ?>)"></a>
			<?php if ($type7 == 2){ ?><a href="<?php echo $link7; ?>" <?php if ($type7 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy7; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo8) { ?>
		<div class="item <?php if ($type8 == 2){ echo "video"; } ?>" data-item="8">
			<a href="<?php echo $link8; ?>" <?php if ($type8 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo8){echo $photo8->getURL();} ?>)"></a>
			<?php if ($type8 == 2){ ?><a href="<?php echo $link8; ?>" <?php if ($type8 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy8; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo9) { ?>
		<div class="item <?php if ($type9 == 2){ echo "video"; } ?>" data-item="9">
			<a href="<?php echo $link9; ?>" <?php if ($type9 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo9){echo $photo9->getURL();} ?>)"></a>
			<?php if ($type9 == 2){ ?><a href="<?php echo $link9; ?>" <?php if ($type9 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy9; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo10) { ?>
		<div class="item <?php if ($type10 == 2){ echo "video"; } ?>" data-item="10">
			<a href="<?php echo $link10; ?>" <?php if ($type10 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo10){echo $photo10->getURL();} ?>)"></a>
			<?php if ($type10 == 2){ ?><a href="<?php echo $link10; ?>" <?php if ($type10 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy10; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo1) { ?>
		<div class="item <?php if ($type1 == 2){ echo "video"; } ?>" data-item="1">
			<a href="<?php echo $link1; ?>" <?php if ($type1 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo1){echo $photo1->getURL();} ?>)"></a>
			<?php if ($type1 == 2){ ?><a href="<?php echo $link1; ?>" <?php if ($type1 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy1; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo2) { ?>
		<div class="item <?php if ($type2 == 2){ echo "video"; } ?>" data-item="2">
			<a href="<?php echo $link2; ?>" <?php if ($type2 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo2){echo $photo2->getURL();} ?>)"></a>
			<?php if ($type2 == 2){ ?><a href="<?php echo $link2; ?>" <?php if ($type2 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy2; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo3) { ?>
		<div class="item <?php if ($type3 == 2){ echo "video"; } ?>" data-item="3">
			<a href="<?php echo $link3; ?>" <?php if ($type3 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo3){echo $photo3->getURL();} ?>)"></a>
			<?php if ($type3 == 2){ ?><a href="<?php echo $link3; ?>" <?php if ($type3 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy3; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo4) { ?>
		<div class="item <?php if ($type4 == 2){ echo "video"; } ?>" data-item="4">
			<a href="<?php echo $link4; ?>" <?php if ($type4 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo4){echo $photo4->getURL();} ?>)"></a>
			<?php if ($type4 == 2){ ?><a href="<?php echo $link4; ?>" <?php if ($type4 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy4; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo5) { ?>
		<div class="item <?php if ($type5 == 2){ echo "video"; } ?>" data-item="5">
			<a href="<?php echo $link5; ?>" <?php if ($type5 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo5){echo $photo5->getURL();} ?>)"></a>
			<?php if ($type5 == 2){ ?><a href="<?php echo $link5; ?>" <?php if ($type5 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy5; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo6) { ?>
		<div class="item <?php if ($type6 == 2){ echo "video"; } ?>" data-item="6">
			<a href="<?php echo $link6; ?>" <?php if ($type6 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo6){echo $photo6->getURL();} ?>)"></a>
			<?php if ($type6 == 2){ ?><a href="<?php echo $link6; ?>" <?php if ($type6 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy6; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo7) { ?>
		<div class="item <?php if ($type7 == 2){ echo "video"; } ?>" data-item="7">
			<a href="<?php echo $link7; ?>" <?php if ($type7 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo7){echo $photo7->getURL();} ?>)"></a>
			<?php if ($type7 == 2){ ?><a href="<?php echo $link7; ?>" <?php if ($type7 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy7; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo8) { ?>
		<div class="item <?php if ($type8 == 2){ echo "video"; } ?>" data-item="8">
			<a href="<?php echo $link8; ?>" <?php if ($type8 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo8){echo $photo8->getURL();} ?>)"></a>
			<?php if ($type8 == 2){ ?><a href="<?php echo $link8; ?>" <?php if ($type8 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy8; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo9) { ?>
		<div class="item <?php if ($type9 == 2){ echo "video"; } ?>" data-item="9">
			<a href="<?php echo $link9; ?>" <?php if ($type9 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo9){echo $photo9->getURL();} ?>)"></a>
			<?php if ($type9 == 2){ ?><a href="<?php echo $link9; ?>" <?php if ($type9 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy9; ?></p>
		</div>
	<?php } ?>
	<?php if ($photo10) { ?>
		<div class="item <?php if ($type10 == 2){ echo "video"; } ?>" data-item="10">
			<a href="<?php echo $link10; ?>" <?php if ($type10 == 2){ echo ' data-fancybox '; }?> class="img" style="background-image: url(<?php if($photo10){echo $photo10->getURL();} ?>)"></a>
			<?php if ($type10 == 2){ ?><a href="<?php echo $link10; ?>" <?php if ($type10 == 2){ echo ' data-fancybox '; }?> class="play"></a><?php } ?>
			<p><?php echo $copy10; ?></p>
		</div>
	<?php } ?>
	<a href="#" class="prev"></a>
	<a href="#" class="next"></a>
</div>