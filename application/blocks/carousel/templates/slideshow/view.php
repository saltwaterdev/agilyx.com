<?php  defined("C5_EXECUTE") or die("Access Denied."); 
	if ($photo1){
		$mainphoto = $photo1->getURL();
	} else if ($photo2){
		$mainphoto = $photo2->getURL();
	} else if ($photo3){
		$mainphoto = $photo3->getURL();
	} else if ($photo4){
		$mainphoto = $photo4->getURL();
	} else if ($photo5){
		$mainphoto = $photo5->getURL();
	} else if ($photo6){
		$mainphoto = $photo6->getURL();
	} else if ($photo7){
		$mainphoto = $photo7->getURL();
	} else if ($photo8){
		$mainphoto = $photo8->getURL();
	} else if ($photo9){
		$mainphoto = $photo9->getURL();
	} else if ($photo10){
		$mainphoto = $photo10->getURL();
	}
?>

<div class="carousel slideshow">
	<div class="main_stage">
		<div class="img" style="background-image: url(<?php echo $mainphoto; ?>)" data-photo="<?php echo $mainphoto; ?>"></div>
		<div class="newimg"></div>
	</div>
	<div class="new_stage">
		<?php if ($photo1){ echo '<a href="#" class="load_photo" style="background-image: url('.$photo1->getURL().')" data-photo="'.$photo1->getURL().'"></a>'; } ?>
		<?php if ($photo2){ echo '<a href="#" class="load_photo" style="background-image: url('.$photo2->getURL().')" data-photo="'.$photo2->getURL().'"></a>'; } ?>
		<?php if ($photo3){ echo '<a href="#" class="load_photo" style="background-image: url('.$photo3->getURL().')" data-photo="'.$photo3->getURL().'"></a>'; } ?>
		<?php if ($photo4){ echo '<a href="#" class="load_photo" style="background-image: url('.$photo4->getURL().')" data-photo="'.$photo4->getURL().'"></a>'; } ?>
		<?php if ($photo5){ echo '<a href="#" class="load_photo" style="background-image: url('.$photo5->getURL().')" data-photo="'.$photo5->getURL().'"></a>'; } ?>
		<?php if ($photo6){ echo '<a href="#" class="load_photo" style="background-image: url('.$photo6->getURL().')" data-photo="'.$photo6->getURL().'"></a>'; } ?>
		<?php if ($photo7){ echo '<a href="#" class="load_photo" style="background-image: url('.$photo7->getURL().')" data-photo="'.$photo7->getURL().'"></a>'; } ?>
		<?php if ($photo8){ echo '<a href="#" class="load_photo" style="background-image: url('.$photo8->getURL().')" data-photo="'.$photo8->getURL().'"></a>'; } ?>
		<?php if ($photo9){ echo '<a href="#" class="load_photo" style="background-image: url('.$photo9->getURL().')" data-photo="'.$photo9->getURL().'"></a>'; } ?>
		<?php if ($photo10){ echo '<a href="#" class="load_photo" style="background-image: url('.$photo10->getURL().')" data-photo="'.$photo10->getURL().'"></a>'; } ?>
	</div>
</div>