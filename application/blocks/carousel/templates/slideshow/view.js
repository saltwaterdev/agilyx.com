$(document).ready(function() {
	var slideshowPlay = setInterval(function(){ playSlide() }, 15000);
	$(".new_stage .load_photo").eq(0).addClass("active");
	$("body").on("click",".new_stage .load_photo",function(e) {
		e.preventDefault();
		photoUrl = $(this).data("photo");
		currentUrl = $(".main_stage .img").attr("data-photo");
		if (photoUrl !== currentUrl){
			clearInterval(slideshowPlay);
			$(".new_stage .load_photo").removeClass("active");
			$(this).addClass("active");
			$(".main_stage .newimg").css("background-image","url("+photoUrl+")");
			$(".main_stage .img").animate({
				opacity: 0
			}, 150, function() {
				$(".main_stage .img").css("background-image","url("+photoUrl+")").css("opacity","1").attr("data-photo",photoUrl);
				$(".main_stage .newimg").attr("style","");
				var slideshowPlay = setInterval(function(){ playSlide() }, 15000);
			});
		}
	});
	function playSlide() {
		sliderLength = $(".new_stage .load_photo").length;
		currentSlide = parseInt($(".new_stage .load_photo.active").index()) + 1;
		if (sliderLength == currentSlide){
			currentSlide = 0;
		}
		photoUrl = $(".new_stage .load_photo").eq(currentSlide).data("photo");
 		$(".new_stage .load_photo").removeClass("active");
		$(".new_stage .load_photo").eq(currentSlide).addClass("active");
		$(".main_stage .newimg").css("background-image","url("+photoUrl+")");
		$(".main_stage .img").animate({
			opacity: 0
		}, 150, function() {
			$(".main_stage .img").css("background-image","url("+photoUrl+")").css("opacity","1").attr("data-photo",photoUrl);
			$(".main_stage .newimg").attr("style","");
		});
	}
});