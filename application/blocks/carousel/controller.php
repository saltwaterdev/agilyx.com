<?php  namespace Application\Block\Carousel;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use File;
use Page;
use Concrete\Core\Editor\LinkAbstractor;

class Controller extends BlockController
{
    public $helpers = array('form');
    protected $btExportFileColumns = array('title', 'content_type', 'tag', 'num');
    protected $btTable = 'btCarousel';
    protected $btInterfaceWidth = 900;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = false;
    protected $btCacheBlockOutput = false;
    protected $btCacheBlockOutputOnPost = false;
    protected $btCacheBlockOutputForRegisteredUsers = false;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Carousel");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->copy1;
        $content[] = $this->copy2;
        $content[] = $this->copy3;
        $content[] = $this->copy4;
        $content[] = $this->copy5;
        $content[] = $this->copy6;
        $content[] = $this->copy7;
        $content[] = $this->copy8;
        $content[] = $this->copy9;
        $content[] = $this->copy10;
        $content[] = $this->copy1_link;
        $content[] = $this->copy2_link;
        $content[] = $this->copy3_link;
        $content[] = $this->copy4_link;
        $content[] = $this->copy5_link;
        $content[] = $this->copy6_link;
        $content[] = $this->copy7_link;
        $content[] = $this->copy8_link;
        $content[] = $this->copy9_link;
        $content[] = $this->copy10_link;
        return implode(" ", $content);
    }

    public function view()
    {
         if ($this->photo1 && ($f = File::getByID($this->photo1)) && is_object($f)) {
            $this->set("photo1", $f);
        } else {
            $this->set("photo1", false);
        }
         if ($this->photo2 && ($f = File::getByID($this->photo2)) && is_object($f)) {
            $this->set("photo2", $f);
        } else {
            $this->set("photo2", false);
        }
         if ($this->photo3 && ($f = File::getByID($this->photo3)) && is_object($f)) {
            $this->set("photo3", $f);
        } else {
            $this->set("photo3", false);
        }
         if ($this->photo4 && ($f = File::getByID($this->photo4)) && is_object($f)) {
            $this->set("photo4", $f);
        } else {
            $this->set("photo4", false);
        }
         if ($this->photo5 && ($f = File::getByID($this->photo5)) && is_object($f)) {
            $this->set("photo5", $f);
        } else {
            $this->set("photo5", false);
        }
         if ($this->photo6 && ($f = File::getByID($this->photo6)) && is_object($f)) {
            $this->set("photo6", $f);
        } else {
            $this->set("photo6", false);
        }
         if ($this->photo7 && ($f = File::getByID($this->photo7)) && is_object($f)) {
            $this->set("photo7", $f);
        } else {
            $this->set("photo7", false);
        }
         if ($this->photo8 && ($f = File::getByID($this->photo8)) && is_object($f)) {
            $this->set("photo8", $f);
        } else {
            $this->set("photo8", false);
        }
         if ($this->photo9 && ($f = File::getByID($this->photo9)) && is_object($f)) {
            $this->set("photo9", $f);
        } else {
            $this->set("photo9", false);
        }
         if ($this->photo10 && ($f = File::getByID($this->photo10)) && is_object($f)) {
            $this->set("photo10", $f);
        } else {
            $this->set("photo10", false);
        }
        $this->set('copy1', LinkAbstractor::translateFrom($this->copy1));
		$this->set('link1', LinkAbstractor::translateFrom($this->link1));
		$this->set('type1', LinkAbstractor::translateFrom($this->type1));
		$this->set('copy2', LinkAbstractor::translateFrom($this->copy2));
		$this->set('link2', LinkAbstractor::translateFrom($this->link2));
		$this->set('type2', LinkAbstractor::translateFrom($this->type2));
		$this->set('copy3', LinkAbstractor::translateFrom($this->copy3));
		$this->set('link3', LinkAbstractor::translateFrom($this->link3));
		$this->set('type3', LinkAbstractor::translateFrom($this->type3));
		$this->set('copy4', LinkAbstractor::translateFrom($this->copy4));
		$this->set('link4', LinkAbstractor::translateFrom($this->link4));
		$this->set('type4', LinkAbstractor::translateFrom($this->type4));
		$this->set('copy5', LinkAbstractor::translateFrom($this->copy5));
		$this->set('link5', LinkAbstractor::translateFrom($this->link5));
		$this->set('type5', LinkAbstractor::translateFrom($this->type5));
		$this->set('copy6', LinkAbstractor::translateFrom($this->copy6));
		$this->set('link6', LinkAbstractor::translateFrom($this->link6));
		$this->set('type6', LinkAbstractor::translateFrom($this->type6));
		$this->set('copy7', LinkAbstractor::translateFrom($this->copy7));
		$this->set('link7', LinkAbstractor::translateFrom($this->link7));
		$this->set('type7', LinkAbstractor::translateFrom($this->type7));
		$this->set('copy8', LinkAbstractor::translateFrom($this->copy8));
		$this->set('link8', LinkAbstractor::translateFrom($this->link8));
		$this->set('type8', LinkAbstractor::translateFrom($this->type8));
		$this->set('copy9', LinkAbstractor::translateFrom($this->copy9));
		$this->set('link9', LinkAbstractor::translateFrom($this->link9));
		$this->set('type9', LinkAbstractor::translateFrom($this->type9));
		$this->set('copy10', LinkAbstractor::translateFrom($this->copy10));
		$this->set('link10', LinkAbstractor::translateFrom($this->link10));
		$this->set('type10', LinkAbstractor::translateFrom($this->type10));
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
        
        
        
    }

    protected function addEdit()
    {
        
    }

    public function save($args)
    {
       
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}