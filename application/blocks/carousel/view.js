$(document).ready(function(){
	$( window ).resize(function() {
		mainHeight();
	});
	mainHeight();
	function mainHeight(){
		topHeight = $(".carousel .item.active").outerHeight();
		$(".carousel").attr("style","");
		$(".carousel").css("height",topHeight);
	};
	$(".carousel .item").eq(0).attr("data-item","1");
	$(".carousel .item").eq(1).attr("data-item","2").addClass("next");
	$(".carousel .item").eq(2).attr("data-item","3");
	$(".carousel .item").eq(3).attr("data-item","4");
	$(".carousel .item").eq(4).attr("data-item","5");
	$(".carousel .item").eq(5).attr("data-item","6");
	$(".carousel .item").eq(6).attr("data-item","7");
	$(".carousel .item").eq(7).attr("data-item","8");
	$(".carousel .item").eq(8).attr("data-item","9");
	$(".carousel .item").eq(9).attr("data-item","10");
	$(".carousel .item").last().addClass("prev");
	$("body").on("click",".prev, .next",function(e) {
		e.preventDefault();
		maxCount = $(".carousel .item").length;
		currentItem = parseInt($(".carousel .item.active").data("item"));
		if ($(this).hasClass("next")){
			newItem = currentItem + 1;
			nextItem = currentItem + 2;
			if (newItem > maxCount){
				newItem = 1;
				nextItem = 2;
			}
			if (newItem == maxCount){
				nextItem = 1;
			}
			outgoingLeft = "-24";
			outgoingRight = "75";
			outgoingoutLeft = "-50";
			incomingLeft = "0";
			incomingRight = "0";
			incominginLeft = "68";
			$(".carousel .item.active").addClass("outgoing");
			$(".carousel .item.prev").addClass("outgoingout");
			$(".carousel .item[data-item='"+newItem+"']").addClass("incoming");
			$(".carousel .item[data-item='"+nextItem+"']").addClass("incomingin");
		} else {
			newItem = currentItem - 1;
			nextItem = currentItem - 2;
			if (newItem < 1){
				newItem = maxCount;
				nextItem = maxCount - 1;
			}
			if (nextItem == 0){
				nextItem = 6;
			}
			outgoingLeft = "68";
			incomingLeft = "0";
			incomingRight = "0";
			outgoingRight = "initial";
			outgoingoutLeft = "100";
			incominginLeft = "-24";
			$(".carousel .item.active").addClass("outgoing");
			$(".carousel .item.prev").addClass("incoming");
			$(".carousel .item.next").addClass("outgoingout");
			$(".carousel .item[data-item='"+newItem+"']").addClass("incoming");
			$(".carousel .item[data-item='"+nextItem+"']").addClass("incomingin").css("left","-50%");
		}

		$(".carousel .item.active").animate({
			left: outgoingLeft+"%",
			right: outgoingRight+"%"
		}, 333, function() {
			if (outgoingLeft == "68"){
				$(".carousel .item.outgoing").removeClass("active").attr("style","").addClass("next").removeClass("outgoing");
			} else {
				$(".carousel .item.outgoing").removeClass("active").attr("style","").addClass("prev").removeClass("outgoing");
			}
		});
		$(".carousel .item.outgoingout").animate({
			left: outgoingoutLeft+"%"
		}, 333, function() {
			$(".carousel .item.outgoingout").attr("style","").removeClass("prev").removeClass("next").removeClass("outgoingout");
		});
		$(".carousel .item.incoming").animate({
			left: incomingLeft+"%",
			right: incomingRight+"%"
		}, 333, function() {
			$(".carousel .item.incoming").attr("style","").removeClass("next").removeClass("prev").addClass("active").removeClass("incoming");
		});
		$(".carousel .item.incomingin").animate({
			left: incominginLeft+"%",
		}, 333, function() {
			if (outgoingLeft == "68"){
				$(".carousel .item.incomingin").attr("style","").addClass("prev").removeClass("incomingin");
			} else {
				$(".carousel .item.incomingin").attr("style","").addClass("next").removeClass("incomingin");
			}
		});

/*
		$( "#mobile_menu" ).animate({
				left: "0"
			}, 150, function() {
			});
*/
	});
});