<div class="expand_form">
		<?php $a = new Area('Form Header'); $a->display($c); ?>
		<form method="post" action="">
			<div class="holder">
				<input type="text" class="cycle" name="question" id="question" placeholder="Enter Your Question">
				<i></i>
				<a href="#" class="expand"></a>
			</div>
			<div class="hidden">
				<div class="holder">
					<div class="col_holder">
						<div class="col">
							<p>WHO ARE YOU?</p>
							<input type="radio" name="who_are_you" value="Municipality"> <span>Municipality</span><br>
							<input type="radio" name="who_are_you" value="Waste/Recycling Company"> <span>Waste/Recycling Company</span><br>
							<input type="radio" name="who_are_you" value="Academic Institution"> <span>Academic Institution</span><br>
							<input type="radio" name="who_are_you" value="Government"> <span>Government</span><br>
							<input type="radio" name="who_are_you" value="Individual Consumer"> <span>Individual Consumer</span><br>
						</div>
						<div class="col" style="margin-top: 2.8em;">
							<input type="radio" name="who_are_you" value="Industrial/Commercial"> <span>Industrial / Commercial</span><br>
							<input type="radio" name="who_are_you" value="Grocery/Hospitality"> <span>Grocery / Hospitality</span><br>
							<input type="radio" name="who_are_you" value="Hotels/Resorts"> <span>Hotels / Resorts</span><br>
							<input type="radio" name="who_are_you" value="Non-Profit"> <span>Non-Profit</span><br>
							<input type="radio" name="who_are_you" value="Other"> <span>Other</span><br>
						</div>
						<div class="col">
							<p>INTERESTS?</p>
							<input type="checkbox" name="interests" value="Need for plastic recycling"> <span>Need for plastic recycling</span><br>
							<input type="checkbox" name="interests" value="Project Developer"> <span>Project Developer</span><br>
							<input type="checkbox" name="interests" value="Industrial Offtaker"> <span>Industrial Offtaker</span><br>
							<input type="checkbox" name="interests" value="R&D"> <span>R&D</span><br>
							<input type="checkbox" name="interests" value="Other"> <span>Other</span><br>
						</div>
					</div>
					<input type="text" name="name" id="name" placeholder="Name">
					<input type="text" name="email" id="email" placeholder="Email">
					<input type="submit" value="Submit">
					<div class="collapse"></div>
				</div>
			</div>
		</form>
	</div>
	
	<?php
		$page = Page::getCurrentPage();
		if ($page->getAttribute('form_questions')){
			$text = trim($page->getAttribute('form_questions'));
			$textAr = explode("\n", $text);
			$textAr = array_filter($textAr, 'trim'); // remove any extra \r characters left behind
			$textstring = "";
			foreach ($textAr as $line) {
				$textstring .= "'".$line."',";	
			}
			$textstring = substr($textstring, 0, -1);
		}
	
	?>
	
<script>
	$(document).ready(function() {
		var speed = 50; /* The speed/duration of the effect in milliseconds */
		getnewText();
		function getnewText(){
			if ($(".holder #question").hasClass("cycle")){
				i = 0;
				textType = [<?php echo $textstring; ?>];
				if (textType.length > 0){
					txt = textType[Math.floor(Math.random()*textType.length)];
					typeWriter();
				}
			}
		}
		function typeWriter() {
			if ($(".holder #question").hasClass("cycle")){
				if (i < txt.length) {
				    document.getElementById("question").value += txt.charAt(i);
				    i++;
				    setTimeout(typeWriter, speed);
				} else {
					setTimeout(removeType, speed*10);
				}
			}
		}
		function removeType(){
			if ($(".holder #question").hasClass("cycle")){
				if (i !== 0){
					i--;
					document.getElementById("question").value = txt.substring(0, i);
					setTimeout(removeType, speed/4);
				} else {
					setTimeout(getnewText, speed*40);
				}
			}
		}
		$("body").on("focus",".holder #question",function(e) {
			e.preventDefault();
			if ($(this).hasClass("cycle")){
				$(this).val("").removeClass("cycle");
			}
		});
/*
		$("body").on("blur",".holder #question",function(e) {
			e.preventDefault();
			if ($(this).val().length < 1){
				$(this).addClass("cycle");
				setTimeout(getnewText, speed*15);
			}
		});
*/
	});
</script>