<?php  namespace Application\Block\Twitter;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Core;
use File;
use Page;
use Concrete\Core\Editor\LinkAbstractor;

class Controller extends BlockController
{
    public $helpers = array('form');
    protected $btExportFileColumns = array('title', 'content_type', 'tag', 'num');
    protected $btTable = 'btTwitter';
    protected $btInterfaceWidth = 900;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = false;
    protected $btCacheBlockOutput = false;
    protected $btCacheBlockOutputOnPost = false;
    protected $btCacheBlockOutputForRegisteredUsers = false;
    protected $btCacheBlockOutputLifetime = 0;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("");
    }

    public function getBlockTypeName()
    {
        return t("Twitter");
    }

    public function getSearchableContent()
    {
        $content = array();
        $content[] = $this->copy1;
        $content[] = $this->copy2;
        $content[] = $this->copy3;
        $content[] = $this->copy4;
        $content[] = $this->copy5;
        $content[] = $this->copy6;
        $content[] = $this->copy7;
        $content[] = $this->copy8;
        $content[] = $this->copy9;
        $content[] = $this->copy10;
        $content[] = $this->copy1_link;
        $content[] = $this->copy2_link;
        $content[] = $this->copy3_link;
        $content[] = $this->copy4_link;
        $content[] = $this->copy5_link;
        $content[] = $this->copy6_link;
        $content[] = $this->copy7_link;
        $content[] = $this->copy8_link;
        $content[] = $this->copy9_link;
        $content[] = $this->copy10_link;
        return implode(" ", $content);
    }

    public function view()
    {
         if ($this->picture && ($f = File::getByID($this->picture)) && is_object($f)) {
            $this->set("picture", $f);
        } else {
            $this->set("picture", false);
        }
        $this->set('copy', LinkAbstractor::translateFrom($this->copy));
        $this->set('color', LinkAbstractor::translateFrom($this->color));
       
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
        
        $this->set('copy1', LinkAbstractor::translateFromEditMode($this->copy1));
        
        $this->set('copy2', LinkAbstractor::translateFromEditMode($this->copy2));
        
        $this->set('copy3', LinkAbstractor::translateFromEditMode($this->copy3));
        
        $this->set('copy4', LinkAbstractor::translateFromEditMode($this->copy4));
        
        $this->set('copy5', LinkAbstractor::translateFromEditMode($this->copy5));
        
        $this->set('copy6', LinkAbstractor::translateFromEditMode($this->copy6));
        
        $this->set('copy7', LinkAbstractor::translateFromEditMode($this->copy7));
        
        $this->set('copy8', LinkAbstractor::translateFromEditMode($this->copy8));
        
        $this->set('copy9', LinkAbstractor::translateFromEditMode($this->copy9));
        
        $this->set('copy10', LinkAbstractor::translateFromEditMode($this->copy10));
        
        $this->set('copy1_link', LinkAbstractor::translateFromEditMode($this->copy1_link));
        
        $this->set('copy2_link', LinkAbstractor::translateFromEditMode($this->copy2_link));
        
        $this->set('copy3_link', LinkAbstractor::translateFromEditMode($this->copy3_link));
        
        $this->set('copy4_link', LinkAbstractor::translateFromEditMode($this->copy4_link));
        
        $this->set('copy5_link', LinkAbstractor::translateFromEditMode($this->copy5_link));
        
        $this->set('copy6_link', LinkAbstractor::translateFromEditMode($this->copy6_link));
        
        $this->set('copy7_link', LinkAbstractor::translateFromEditMode($this->copy7_link));
        
        $this->set('copy8_link', LinkAbstractor::translateFromEditMode($this->copy8_link));
        
        $this->set('copy9_link', LinkAbstractor::translateFromEditMode($this->copy9_link));
        
        $this->set('copy10_link', LinkAbstractor::translateFromEditMode($this->copy10_link));
        
    }

    protected function addEdit()
    {
        
    }

    public function save($args)
    {
       
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}