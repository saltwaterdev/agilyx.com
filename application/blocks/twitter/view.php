<div class="tweets">
		<h3>Tweets<span>@agilyx</span></h3>

		<?php
	    function buildBaseString($baseURI, $method, $params) {
	        $r = array();
	        ksort($params);
	        foreach($params as $key=>$value){
	            $r[] = "$key=" . rawurlencode($value);
	        }
	        return $method."&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
	    }
	
	    function buildAuthorizationHeader($oauth) {
	        $r = 'Authorization: OAuth ';
	        $values = array();
	        foreach($oauth as $key=>$value)
	            $values[] = "$key=\"" . rawurlencode($value) . "\"";
	        $r .= implode(', ', $values);
	        return $r;
	    }
	
	    $url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
	
	    $oauth_access_token = "147754408-M29M9JAzzJ4FCVrqhQto9T3dWyVXBWPmVTP9Akd4";
	    $oauth_access_token_secret = "JyrZB1LSt4NRj22BAnKmmdZfknBrwgKVLOsws21bOlGQY";
	    $consumer_key = "Lj5fweOCloBYCsKYzgwa6yMCq";
	    $consumer_secret = "motyM5bixFTZgKEva3XgXIrEch15KUPkEXhkJJLIekquWsTULP";
	
	    $oauth = array( 'oauth_consumer_key' => $consumer_key,
	                    'oauth_nonce' => time(),
	                    'oauth_signature_method' => 'HMAC-SHA1',
	                    'oauth_token' => $oauth_access_token,
	                    'oauth_timestamp' => time(),
	                    'oauth_version' => '1.0');
	
	    $base_info = buildBaseString($url, 'GET', $oauth);
	    $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
	    $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
	    $oauth['oauth_signature'] = $oauth_signature;
	
	    // Make requests
	    $header = array(buildAuthorizationHeader($oauth), 'Expect:');
	    $options = array( CURLOPT_HTTPHEADER => $header,
	                      //CURLOPT_POSTFIELDS => $postfields,
	                      CURLOPT_HEADER => false,
	                      CURLOPT_URL => $url,
	                      CURLOPT_RETURNTRANSFER => true,
	                      CURLOPT_SSL_VERIFYPEER => false);
	
	    $feed = curl_init();
	    curl_setopt_array($feed, $options);
	    $json = curl_exec($feed);
	    curl_close($feed);
	
	    $twitter_data = json_decode($json);
	
	//print it out
// 	print_r($twitter_data);
	
 	for($x=0;$x<3;$x++){
// 	 	echo $twitter_data[$x]->text;
		$tweetdate = strtotime($twitter_data[$x]->created_at);
		?>
		<div class="tweet">
			<div class="left"></div>
			<div class="right">
				<p class="date"><?php echo date('M j, Y', $tweetdate); ?></p>
				<p class="title">Agilyx Corporation <a href="https://twitter.com/agilyx" target="_blank">@agilyx</a></p>
		        <p class="text"><?php echo $twitter_data[$x]->text; ?></p>
			</div>
		</div>
	<?php 
 	}
	
	?>
	</div>