$(document).ready(function(){
	$( ".feature_block" ).each(function() {
		if ($(this).find(".btn")[0]){
			$(this).find(".btn").parent("a").parent("p").css("text-align","center");
		}
		if ($(this).parent().hasClass("col-sm-10")){
			equalHeights();
		}
	});
	$( window ).resize(function() {
		equalHeights();
	});
	function equalHeights(){
		maxHeight = 0;
		$(this).find(".feature_block.top").css("height","initial");
		$( ".col-sm-30 .columns" ).each(function() {
			thisHeight = $(this).find(".feature_block").outerHeight();
			if (maxHeight < thisHeight){
				maxHeight = thisHeight;
			}
		});
		$(".feature_block.top").css("height",maxHeight);
	}
});