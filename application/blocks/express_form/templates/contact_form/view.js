$(document).ready(function(){
	$( ".contactform .form-group" ).each(function() {
		labelText = $(this).find("label").text();
		if ($(this).find(".text-muted").text() == "Required"){
			labelText += "*";
		}
		$(this).find("input,textarea").attr("placeholder",labelText);
		$(this).find("label").hide();
		$(this).find(".text-muted").hide();
	});
});