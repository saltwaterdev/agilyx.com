$(document).ready(function() {
	$( ".team_member" ).click(function(e) {
		e.preventDefault();
		getMember = $(this).data("toggle");
		$(".slideout[data-slideout='"+getMember+"']").addClass("active");
		$( ".team_shield[data-shield='"+getMember+"']" ).fadeIn( 250, function() {
			$( ".team_shield[data-shield='"+getMember+"']" ).addClass("active");
		});
		$("body,html").addClass("slideout_active");
	});
	$("body").on("click",".team_shield.active, .team_shield.active .mobile_close",function(e) {
		$(".slideout.active").removeClass("active");
		$( ".team_shield" ).removeClass("active");
		$( ".team_shield" ).fadeOut( 250, function() {
		});
		$("body,html").removeClass("slideout_active");
	});
});