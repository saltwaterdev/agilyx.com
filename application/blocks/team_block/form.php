<?php defined("C5_EXECUTE") or die("Access Denied."); ?>

<div class="form-group">
    <?php echo $form->label($view->field('name'), t("Name")); ?>
    <?php echo isset($btFieldsRequired) && in_array('name', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('name'), $name, array (
  'maxlength' => 255,
  'placeholder' => 'name',
)); ?>
</div>

<div class="form-group">
    <?php
    if (isset($thumb) && $thumb > 0) {
        $thumb_o = File::getByID($thumb);
        if (!is_object($thumb_o)) {
            unset($thumb_o);
        }
    } ?>
    <?php echo $form->label($view->field('thumb'), t("employee thumb")); ?>
    <?php echo isset($btFieldsRequired) && in_array('thumb', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo Core::make("helper/concrete/asset_library")->image('ccm-b-team_block-thumb-' . $identifier_getString, $view->field('thumb'), t("Choose Image"), $thumb_o); ?>
</div>

<div class="form-group">
    <?php echo $form->label($view->field('title'), t("title")); ?>
    <?php echo isset($btFieldsRequired) && in_array('title', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('title'), $title, array (
  'maxlength' => 255,
)); ?>
</div>

<div class="form-group">
    <?php echo $form->label($view->field('bio'), t("bio")); ?>
    <?php echo isset($btFieldsRequired) && in_array('bio', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo Core::make('editor')->outputBlockEditModeEditor($view->field('bio'), $bio); ?>
</div>

<div class="form-group">
    <?php echo $form->label($view->field('greetingName'), t("Greeting Name")); ?>
    <?php echo isset($btFieldsRequired) && in_array('greetingName', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php echo $form->text($view->field('greetingName'), $greetingName, array (
  'maxlength' => 255,
)); ?>
</div>