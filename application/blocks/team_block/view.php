<div class="team_shield" data-shield="<?php echo $bID; ?>"><a href="#" class="mobile_close"><i class="fa fa-times" aria-hidden="true"></i></a></div>
<a href="#" class="team_member" data-toggle="<?php echo $bID; ?>">
	<div class="thumb" style="background-image: url(<?php echo $thumb->getURL(); ?>)"></div>
	<div class="info">
		<p class="name"><?php echo h($name); ?></p>
		<p class="title"><?php echo h($title); ?></p>
	</div>
</a>

<div class="slideout" data-slideout="<?php echo $bID; ?>">
	<div class="thumb" style="background-image: url(<?php echo $thumb->getURL(); ?>)">
		<div class="info">
			<p class="name"><?php echo h($name); ?></p>
			<p class="title"><?php echo h($title); ?></p>
		</div>
	</div>
	<div class="bio">
		<div class="container">
			<p class="title">About <?php echo h($greetingName); ?></p>
			<?php echo $bio; ?>
		</div>
	</div>
</div>