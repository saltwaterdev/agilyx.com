<?php namespace Application\Block\TeamBlock;

defined("C5_EXECUTE") or die("Access Denied.");

use Concrete\Core\Block\BlockController;
use Concrete\Core\Editor\LinkAbstractor;
use Core;
use File;
use Page;

class Controller extends BlockController
{
    public $btFieldsRequired = ['name', 'thumb', 'bio'];
    protected $btExportFileColumns = ['thumb'];
    protected $btTable = 'btTeamBlock';
    protected $btInterfaceWidth = 900;
    protected $btInterfaceHeight = 500;
    protected $btIgnorePageThemeGridFrameworkContainer = false;
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $pkg = false;
    
    public function getBlockTypeDescription()
    {
        return t("add a team member");
    }

    public function getBlockTypeName()
    {
        return t("Team Member");
    }

    public function getSearchableContent()
    {
        $content = [];
        $content[] = $this->name;
        $content[] = $this->title;
        $content[] = $this->bio;
        $content[] = $this->greetingName;
        return implode(" ", $content);
    }

    public function view()
    {
        
        if ($this->thumb && ($f = File::getByID($this->thumb)) && is_object($f)) {
            $this->set("thumb", $f);
        } else {
            $this->set("thumb", false);
        }
        $this->set('bio', LinkAbstractor::translateFrom($this->bio));
    }

    public function add()
    {
        $this->addEdit();
    }

    public function edit()
    {
        $this->addEdit();
        
        $this->set('bio', LinkAbstractor::translateFromEditMode($this->bio));
    }

    protected function addEdit()
    {
        $this->requireAsset('core/file-manager');
        $this->requireAsset('redactor');
        $this->set('btFieldsRequired', $this->btFieldsRequired);
        $this->set('identifier_getString', Core::make('helper/validation/identifier')->getString(18));
    }

    public function save($args)
    {
        $args['bio'] = LinkAbstractor::translateTo($args['bio']);
        parent::save($args);
    }

    public function validate($args)
    {
        $e = Core::make("helper/validation/error");
        if (in_array("name", $this->btFieldsRequired) && (trim($args["name"]) == "")) {
            $e->add(t("The %s field is required.", t("Name")));
        }
        if (in_array("thumb", $this->btFieldsRequired) && (trim($args["thumb"]) == "" || !is_object(File::getByID($args["thumb"])))) {
            $e->add(t("The %s field is required.", t("employee thumb")));
        }
        if (in_array("title", $this->btFieldsRequired) && (trim($args["title"]) == "")) {
            $e->add(t("The %s field is required.", t("title")));
        }
        if (in_array("bio", $this->btFieldsRequired) && (trim($args["bio"]) == "")) {
            $e->add(t("The %s field is required.", t("bio")));
        }
        if (in_array("greetingName", $this->btFieldsRequired) && (trim($args["greetingName"]) == "")) {
            $e->add(t("The %s field is required.", t("Greeting Name")));
        }
        return $e;
    }

    public function composer()
    {
        $this->edit();
    }
}