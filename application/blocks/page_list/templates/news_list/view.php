<?php
defined('C5_EXECUTE') or die("Access Denied.");

$c = Page::getCurrentPage();

/** @var \Concrete\Core\Utility\Service\Text $th */
$th = Core::make('helper/text');
/** @var \Concrete\Core\Localization\Service\Date $dh */
$dh = Core::make('helper/date');

if ($c->isEditMode() && $controller->isBlockEmpty()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Page List Block.') ?></div>
    <?php
} else {
    ?>

    <div class="ccm-block-page-list-wrapper news_list">

        <?php if (isset($pageListTitle) && $pageListTitle) {
            ?>
            <div class="ccm-block-page-list-header">
                <h5><?php echo h($pageListTitle) ?></h5>
            </div>
            <?php
        } ?>

        <?php if (isset($rssUrl) && $rssUrl) {
            ?>
            <a href="<?php echo $rssUrl ?>" target="_blank" class="ccm-block-page-list-rss-feed">
                <i class="fa fa-rss"></i>
            </a>
            <?php
        } ?>

        <div class="ccm-block-page-list-pages">

            <?php

            $includeEntryText = false;
            if (
                (isset($includeName) && $includeName)
                ||
                (isset($includeDescription) && $includeDescription)
                ||
                (isset($useButtonForLink) && $useButtonForLink)
            ) {
                $includeEntryText = true;
            }
            
            foreach ($pages as $page) {
	            $date = $dh->formatDateTime($page->getCollectionDatePublic(), true);
	            if ($page->getAttribute('date')){
					$date = strtotime($page->getAttribute('date'));
				} else {
					$date = strtotime($dh->formatDateTime($page->getCollectionDatePublic(), true));
				}
				$output[] = array("cid"=>$page->getCollectionID(), "time"=>$date);
	        }
	        
	        function sortByTime($a, $b) {
			    return $a['time'] - $b['time'];
			}
			
            if (count($output) > 0){
			    usort($output, 'sortByTime');
			    $output = array_reverse($output);

	            foreach ($output as $pageorder) {
	
					$page = Page::getByID($pageorder['cid']);
	
	                // Prepare data for each page being listed...
	                $buttonClasses = 'ccm-block-page-list-read-more';
	                $entryClasses = 'ccm-block-page-list-page-entry';
	                $title = $page->getCollectionName();
	                if ($page->getCollectionPointerExternalLink() != '') {
	                    $url = $page->getCollectionPointerExternalLink();
	                    if ($page->openCollectionPointerExternalLinkInNewWindow()) {
	                        $target = '_blank';
	                    }
	                } else {
	                    $url = $page->getCollectionLink();
	                    $target = $page->getAttribute('nav_target');
	                }
	                $target = empty($target) ? '_self' : $target;
	                $newstype = $page->getAttribute("news_location");
	                $description = $page->getCollectionDescription();
	                $description = $controller->truncateSummaries ? $th->wordSafeShortText($description, $controller->truncateChars) : $description;
	                $thumbnail = false;
	                if ($displayThumbnail) {
	                    $thumbnail = $page->getAttribute('thumbnail');
	                }
	                if (is_object($thumbnail) && $includeEntryText) {
	                    $entryClasses = 'ccm-block-page-list-page-entry-horizontal';
	                }
	                $date = $dh->formatDateTime($page->getCollectionDatePublic(), true);
					if ($newstype == "External Item"){
						$url = $page->getAttribute('news_url');
						$target = "_blank";
					}
					if ($newstype == "PDF/File"){
						$fileobject = $page->getAttribute('news_file');
						if (is_object($fileobject)) {
						    //$url = URL::to('/download_file', $fileobject->getFileID(), $c->getCollectionID());
							$url = $fileobject->getURL();
						    $target = "_blank";
						}
					}
					$date = date('F j, Y',$pageorder['time']);
					$thumbnail = "";
					$ih = Loader::helper('image');
					$img = $page->getAttribute('thumbnail'); 
					if($img){
						$thumb = $ih->getThumbnail($img, 300, 300, false);
						$thumbnail = $thumb->src;
					}
                //Other useful page data...

                //$last_edited_by = $page->getVersionObject()->getVersionAuthorUserName();

                /* DISPLAY PAGE OWNER NAME
                 * $page_owner = UserInfo::getByID($page->getCollectionUserID());
                 * if (is_object($page_owner)) {
                 *     echo $page_owner->getUserDisplayName();
                 * }
                 */

                /* CUSTOM ATTRIBUTE EXAMPLES:
                 * $example_value = $page->getAttribute('example_attribute_handle', 'display');
                 *
                 * When you need the raw attribute value or object:
                 * $example_value = $page->getAttribute('example_attribute_handle');
                 */

                /* End data preparation. */

                /* The HTML from here through "endforeach" is repeated for every item in the list... */ ?>
                

                <div class="<?php echo $entryClasses ?>">
	                <div class="left <?php  if($thumbnail == ""){ echo 'empty';  } ?>" style="background-image: url(<?php echo $thumbnail; ?>)"></div>
	                <div class="right">
		                <p class="date"><?php echo $date; ?><?php if ($page->getAttribute('news_type')){ ?>: <span><?php echo $page->getAttribute('news_type'); ?></span><?php } ?></p>
		                <a href="<?php echo $url; ?>" target="<?php echo $target; ?>"><?php echo $title; ?></a>
	                </div>

                
                </div>

                <?php
            } } ?>
        </div><!-- end .ccm-block-page-list-pages -->

        <?php if (count($pages) == 0) { ?>
            <div class="ccm-block-page-list-no-pages"><?php echo h($noResultsMessage) ?></div>
        <?php } ?>

    </div><!-- end .ccm-block-page-list-wrapper -->


    <?php if ($showPagination) { ?>
        <?php echo $pagination; ?>
    <?php } ?>

    <?php

} ?>
