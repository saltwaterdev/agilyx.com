<?php  defined("C5_EXECUTE") or die("Access Denied."); $ps = Loader::helper('form/page_selector'); ?>

<style>
	.form-group label{
		display: block;
	}
	.form-group input[type="text"]{
		width: 100%;
	}
	.item_holder{
		overflow: hidden;
	}
	.option{
		width: 50px;
		height: 50px;
		float: left;
		margin-right: 15px;
	}
	.option:last-child{
		margin-right: 0;
	}
	.option.selected{
		border: 2px solid red;
	}
	.option1{
		background-color: #1a4f28;
	}
	.option2{
		background-color: #81be36;
	}
	.option3{
		background-color: #104f4f;
	}
	.option4{
		background-color: #199c9c;
	}
	.option5{
		background-color: #0e4385;
	}
	.option6{
		background-color: #2e3546;
	}
	.option7{
		background-color: #363b45;
	}
</style>

<div class="form-group">
	<?php 
    if (isset($picture) && $picture > 0) {
        $picture_o = File::getByID($picture);
        if (!is_object($picture_o)) {
            unset($picture_o);
        }
    } ?>
    <?php  echo $form->label('picture', t("Optional Image")); ?>
    <?php  echo isset($btFieldsRequired) && in_array('picture', $btFieldsRequired) ? '<small class="required">' . t('Required') . '</small>' : null; ?>
    <?php  echo Core::make("helper/concrete/asset_library")->image('ccm-b-sliding_copy-picture-' . Core::make('helper/validation/identifier')->getString(18), $view->field('picture'), t("Choose Image"), $picture_o); ?>
</div>

<div class="form-group">
    <label class="control-label">Copy</label>
    <?php
    $editor = Core::make('editor');
    echo $editor->outputStandardEditor('copy', $copy);
    ?>
</div>

<div class="form-group">
	<label class="control-label">Choose Background Color</label>
	<div class="item_holder">
		<div class="option option1 <?php if ($color == 1){echo "selected";} ?>" data-option="1"></div>
		<div class="option option2 <?php if ($color == 2){echo "selected";} ?>" data-option="2"></div>
		<div class="option option3 <?php if ($color == 3){echo "selected";} ?>" data-option="3"></div>
		<div class="option option4 <?php if ($color == 4){echo "selected";} ?>" data-option="4"></div>
		<div class="option option5 <?php if ($color == 5){echo "selected";} ?>" data-option="5"></div>
		<div class="option option6 <?php if ($color == 6){echo "selected";} ?>" data-option="6"></div>
		<div class="option option7 <?php if ($color == 7){echo "selected";} ?>" data-option="7"></div>
	</div>
</div>

<div class="form-group">
    <label class="control-label">If entire block is clickable, enter URL.</label>
    <input type="text" name="full_link" id="full_link" value="<?php echo $full_link; ?>">
</div>

<div class="form-group">
    <label class="control-label">Is the link or CTA a video?</label>
    <input type="radio" name="link_type" value="1" <?php if ($link_type == 1){ echo "checked"; } ?>>Yes<br>
    <input type="radio" name="link_type" value="2" <?php if ($link_type == 2){ echo "checked"; } ?>>No
</div>

<input type="hidden" name="color" id="box_color" value="<?php echo $color; ?>">

<script>
	$(document).ready(function() {
		$("body").on("click",".item_holder .option",function(e) {
			e.preventDefault();
			$(".item_holder .option").removeClass("selected");
			$(this).addClass("selected");
			dataID = $(this).attr("data-option");
			$("#box_color").val(dataID).text(dataID);
		});
	});
</script>