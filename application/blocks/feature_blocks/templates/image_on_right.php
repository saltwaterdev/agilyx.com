<?php  defined("C5_EXECUTE") or die("Access Denied."); 
	if ($link_type == 1){
		$fancybox = " data-fancybox ";
	} else {
		$fancybox = "";
	}
	
		if ($color == 1){
		$bgcolor = "#1a4f28";
	} else if ($color == 2){
		$bgcolor = "#81be36";
	} else if ($color == 3){
		$bgcolor = "#104f4f";
	} else if ($color == 4){
		$bgcolor = "#199c9c";
	} else if ($color == 5){
		$bgcolor = "#0e4385";
	} else if ($color == 6){
		$bgcolor = "#2e3546";
	} else {
		$bgcolor = "#363b45";
	}
	if ($link_type == 1){
		$fancybox = " data-fancybox ";
	} else {
		$fancybox = "";
	}
	if ($picture){
		$ih = Loader::helper('image');
		$block1thumbnail = File::getByID($pictureid);
		if ($block1thumbnail) {
			$block1thumb = $ih->getThumbnail($block1thumbnail, 800, 800, true);
		}
	}
	if (strpos($full_link, 'agilyx.') !== false || strpos($full_link, 'agx.') !== false) {
	    $target = "_self";
	} else {
		$target = "_blank";
	}
?>

<div class="feature_block right">
	<?php if ($full_link){ echo '<a href="'.$full_link.'" '.$fancybox.' class="link" target="'.$target.'"></a>'; }?>
	<?php if ($link_type == 1){echo "<div class='play_btn'></div>"; } ?>
	<div class="img" style="background-image: url(<?php if($picture){echo $block1thumb->src;} ?>)"></div>
	<div class="copy" style="background-color: <?php echo $bgcolor; ?>">
		<div class="container">
			<?php echo $copy; ?>
		</div>
	</div>
</div>