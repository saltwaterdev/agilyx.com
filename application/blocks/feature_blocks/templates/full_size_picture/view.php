<?php  defined("C5_EXECUTE") or die("Access Denied."); 
	if ($picture){
		$ih = Loader::helper('image');
		$block1thumbnail = File::getByID($pictureid);
		if ($block1thumbnail) {
			$block1thumb = $ih->getThumbnail($block1thumbnail, 800, 800, true);
		}
	}
?>

<div class="feature_block full" style="background-image: url(<?php if($picture){echo $block1thumb->src;} ?>)">
	<a href="<?php echo $full_link; ?>"></a>
	<?php echo $copy; ?>
</div>