$(document).ready(function(){
	$( ".feature_block" ).each(function() {
		if ($(this).find(".btn")[0]){
			$(this).find(".btn").parent("a").parent("p").css("text-align","center");
		}
		if ($(this).parent().hasClass("col-sm-10")){
			equalHeights();
		}
	});
	$( window ).resize(function() {
		equalHeights();
	});
	function equalHeights(){
		$(this).find(".feature_block.top").css("height","initial");
		if ($(window).width() >= 960) {
			maxHeight = 0;
			$( ".feature_block.top" ).each(function() {
				thisHeight = $(this).outerHeight();
				if (maxHeight < thisHeight){
					maxHeight = thisHeight;
				}
			});
			$(".feature_block.top").css("height",maxHeight);	
		}
	}
});