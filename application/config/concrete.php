<?php

return [
    'icons' => [
        'file_manager_listing' => [
            'handle' => 'file_manager_listing',
            'width' => 120,
            'height' => 120,
        ],
    ],

    'file_manager' => [
        'results' => 100,
    ],
];