<?php 
namespace Concrete\Package\Barebones\Src;

use Concrete\Core\Page\Theme\GridFramework\GridFramework;

defined('C5_EXECUTE') or die(_("Access Denied."));

class BarebonesGridFramework extends GridFramework
{
	
	public function supportsNesting()
    {
        return true;
    }

    public function getPageThemeGridFrameworkName()
    {
        return t('Barebones');
    }

    public function getPageThemeGridFrameworkRowStartHTML()
    {
        return '<div class="">';
    }

    public function getPageThemeGridFrameworkRowEndHTML()
    {
        return '</div>';
    }

    public function getPageThemeGridFrameworkContainerStartHTML()
    {
         return '<div class="row">';
    }

    public function getPageThemeGridFrameworkContainerEndHTML()
    {
         return '</div>';
    }

    public function getPageThemeGridFrameworkColumnClasses()
    {
        $columns = array(
            'col-sm-1 ',
            'col-sm-2 ',
            'col-sm-3',
            'col-sm-4',
            'col-sm-5',
            'col-sm-6',
            'col-sm-7',
            'col-sm-8',
            'col-sm-9',
            'col-sm-10',
            'col-sm-11',
            'col-sm-12',
            'col-sm-13',
            'col-sm-14',
            'col-sm-15',
            'col-sm-16',
            'col-sm-17',
            'col-sm-18',
            'col-sm-19',
            'col-sm-20',
            'col-sm-21',
            'col-sm-22',
            'col-sm-23',
            'col-sm-24',
            'col-sm-25',
            'col-sm-26',
            'col-sm-27',
            'col-sm-28',
            'col-sm-29',
            'col-sm-30',
        );
        return $columns;
    }

    public function getPageThemeGridFrameworkColumnOffsetClasses()
    {
        $offsets = array(
            'col-sm-offset-1',
            'col-sm-offset-2',
            'col-sm-offset-3',
            'col-sm-offset-4',
            'col-sm-offset-5',
            'col-sm-offset-6',
            'col-sm-offset-7',
            'col-sm-offset-8',
            'col-sm-offset-9',
            'col-sm-offset-10',
            'col-sm-offset-11',
            'col-sm-offset-12',
            'col-sm-offset-13',
            'col-sm-offset-14',
            'col-sm-offset-15',
            'col-sm-offset-16',
            'col-sm-offset-17',
            'col-sm-offset-18',
            'col-sm-offset-19',
            'col-sm-offset-20',
            'col-sm-offset-21',
            'col-sm-offset-22',
            'col-sm-offset-23',
            'col-sm-offset-24',
            'col-sm-offset-25',
            'col-sm-offset-26',
            'col-sm-offset-27',
            'col-sm-offset-28',
            'col-sm-offset-29',
            'col-sm-offset-30',
        );
        return $offsets;
    }

    public function getPageThemeGridFrameworkColumnAdditionalClasses()
    {
        return 'columns';
    }

    public function getPageThemeGridFrameworkColumnOffsetAdditionalClasses()
    {
        return 'columns';
    }

}