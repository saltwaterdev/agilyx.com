<?php       

namespace Concrete\Package\Barebones;
use Package;
use BlockType;
use SinglePage;
use PageTheme;
use BlockTypeSet;
use CollectionAttributeKey;
use Concrete\Core\Attribute\Type as AttributeType;
use Concrete\Package\Barebones\Src\BarebonesGridFramework;
use Core;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package
{

	protected $pkgHandle = 'barebones';
	protected $appVersionRequired = '5.7.1';
	protected $pkgVersion = '1.0';
	
	
	
	public function getPackageDescription()
	{
		return t("Barebone Theme to Place Content and Arrange IA");
	}

	public function getPackageName()
	{
		return t("Barebones");
	}
	
	public function on_start()
	{
	    $manager = Core::make('manager/grid_framework');
	    $manager->extend('barebones', function($app) {
	        return new BarebonesGridFramework();
	    });
	}
	
	public function install()
	{
		$pkg = parent::install();
		BlockTypeSet::add("barebones","Barebones", $pkg);
		PageTheme::add('barebones', $pkg);				
		
		$imgAttr = AttributeType::getByHandle('image_file'); 
  		$thumb = CollectionAttributeKey::getByHandle('thumbnail'); 
		if(!is_object($thumb)) {
	     	CollectionAttributeKey::add($imgAttr, 
	     	array(
	     		'akHandle' => 'thumbnail', 
	     		'akName' => t('Thumbnail Image'), 
	     	),$pkg);
	  	}
	}
}
?>