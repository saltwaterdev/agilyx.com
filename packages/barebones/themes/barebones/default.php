<?php $this->inc('elements/header.php'); $page = Page::getCurrentPage(); ?>
	
<div class="container">
	<div class="site_tree">
		<?php
			
			$nav = BlockType::getByHandle('autonav');
    		$nav->controller->orderBy = 'display_asc';
			$nav->controller->displayPages = 'top';
			$nav->controller->displaySubPages = 'all';
			$nav->controller->displaySubPageLevels = 'all';
			$nav->render('view');
			
		?>
	</div>
	<div class="content_area">
		<div class="container">
			<?php $a = new Area('Main'); $a->enableGridContainer(); $a->display($c); ?>
		</div>
	</div>
</div>

<?php $this->inc('elements/footer.php'); ?>