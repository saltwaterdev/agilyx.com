<!DOCTYPE HTML>
<html lang="en">
	
	<head>
		
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $view->getThemePath(); ?>/css/styles.css" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0" />
		<?php Loader::element('header_required'); ?>
		<?php 
			global $u;
			$page = Page::getCurrentPage();
			global $page;
			$isLoggedIn = User::isLoggedIn();
			
		?>
	</head>

<body id="barebones" class="<?php if ($isLoggedIn) { echo "loggedin"; } ?>">
	
	<header>
	
	</header>