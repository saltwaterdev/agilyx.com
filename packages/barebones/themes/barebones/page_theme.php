<?php 
namespace Concrete\Package\Barebones\Theme\Barebones;
use Concrete\Core\Page\Theme\Theme;
use Concrete\Core\Area\Layout\Preset\Provider\ThemeProviderInterface;
class PageTheme extends Theme implements ThemeProviderInterface {

	public function registerAssets() {

        $this->requireAsset('css', 'font-awesome');
       	$this->requireAsset('javascript', 'jquery');
        //$this->requireAsset('javascript', 'bootstrap/*');
	}

   	protected $pThemeGridFrameworkHandle = 'barebones';

    public function getThemeBlockClasses(){
    		
    	// this adds custom classes as a dropdown option on blocks.
		return array(
            'social_links' => array("singleColor","flatFullColor","roundedGradient","roundedFlat"),
            'content' => array('blue-background')
        );
    }

    public function getThemeAreaClasses()
    {
       	// this adds custom classes to be added to an area.
	    /*
		return array(
            'Main' => array('area-content-accent')
        );
		*/
    }

    public function getThemeEditorClasses()
    {
        // classes available in WYSIWYG	        
        return array(
            array('title' => t('Site Title'), 'menuClass' => 'site-title', 'spanClass' => 'site-title')
        );
		 
		 
    }
    
    public function getThemeAreaLayoutPresets()
	{
	    $presets = array(
	        array(
	            'handle' => 'wide_container',
	            'name' => 'Wide Container',
	            'container' => '<div class="wide_row"></div>',
	            'columns' => array(
	                '<div class="col-sm-12"></div>'
	            ),
	        ),
	    );
	    return $presets;
	}

}
