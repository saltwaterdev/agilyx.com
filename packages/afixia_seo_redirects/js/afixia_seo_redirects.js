"use strict";

var AfxImportExport = function (settings, validation) {

    function ClearErrorMessage() {
        $("#afx-import-export-validation").html('');
    }

    function SetErrorMessage(message) {
        var template = _.template($("script.validation-template").html());
        var error = {"success":0,"validationList":[message]};
        $("#afx-import-export-validation").html(template(error));
    }

    // Validation
    if (validation != null) {
        var template = _.template($("script.validation-template").html());
        $("#afx-import-export-validation").html(template(validation));
    }

    $("#import-file-selector").on("change", function(event) {
        ClearErrorMessage();
        var filename = $(this).val().replace(/.*(\/|\\)/, '');
        $('#import-file-info').html(filename);
    });

    $("#restore-file-selector").on("change", function(event) {
        ClearErrorMessage();
        var filename = $(this).val().replace(/.*(\/|\\)/, '');
        $('#restore-file-info').html(filename);
    });

    $("#afx-import-form").submit(function(event) {
        ClearErrorMessage();
        var filename = $('#import-file-info').html();
        if(!filename.length) {
            event.preventDefault();
            SetErrorMessage(settings.import_file_error);
        } else {
            if (!confirm(settings.import_warning))
                event.preventDefault();
        }
    });

    $("#afx-restore-form").submit(function(event) {
        ClearErrorMessage();
        var filename = $('#restore-file-info').html();
        if(!filename.length) {
            event.preventDefault();
            SetErrorMessage(settings.restore_file_error);
        } else {
            if (!confirm(settings.restore_warning))
                event.preventDefault();
        }
    });
};

var AfxUrlsChangedList = function (settings) {
    var query, page_num, page_size, page_sort, page_direction;

    // Init
    page_num = settings.page_num;
    page_size = settings.page_size;
    page_sort = settings.page_sort;
    page_direction = settings.page_direction;
    query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;

    // Wire up Grid

    // Page Sorting
    $('.afx-results-table thead a').on('click', function(e) {
        e.preventDefault();

        // Direction
        page_direction = "asc";

        if ($(this).parent().hasClass('ccm-results-list-active-sort-desc')) {
            $('.afx-results-table thead th').each(function () {
                $(this).attr('class', '');
            });
            $(this).parent().attr('class', 'ccm-results-list-active-sort-asc');
            page_direction = "asc";
        } else if ($(this).parent().hasClass('ccm-results-list-active-sort-asc')) {
            $('.afx-results-table thead th').each(function () {
                $(this).attr('class', '');
            });
            $(this).parent().attr('class', 'ccm-results-list-active-sort-desc');
            page_direction = "desc";
        } else {
            $('.afx-results-table thead th').each(function () {
                $(this).attr('class', '');
            });
            $(this).parent().attr('class', 'ccm-results-list-active-sort-asc');
            page_direction = "asc";
        }

        // Sort Order
        page_sort = $(this).attr('data-sort');

        query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;
        bindGrid();
    });

    // Page Size
    $("#entryselect").change(function () {
        $(this).find("option:selected").each(function () {
            page_size = $(this).val();
            query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;
            bindGrid();
        });
    });

    $("#bulkaction").change(function () {
        $(this).find("option:selected").each(function () {
            var cmd = $(this).val();
            var txt = $(this).text();
            var rIDs = '';
            $('.afx-results-table .afx-grid-cell').each(function (i) {
                if ($(this).find('input[data-search-checkbox=individual]').prop('checked')) {
                    rIDs += $(this).attr('data-id') + ",";
                }
            });
            if (cmd == 'delete_all') rIDs = '0';
            rIDs = rIDs.replace(/,\s*$/, ""); // del trailing
            if (cmd.length && rIDs.length) {
                if (confirm(settings.confirm_txt.format(txt))) {
                    $.ajax({
                        url: settings.bulkaction_url,
                        type: 'post',
                        data: {
                            "bulkaction": $(this).val(),
                            "rIDs": rIDs
                        }
                    }).done(function () {
                        bindGrid();
                    });

                    $('input[data-search-checkbox=select-all]').prop('checked', false).trigger('change');
                    $('input[data-search-checkbox=individual]').prop('checked', false).trigger('change');
                    $(this).parent().val(''); // reset
                }
            } else {
                alert(settings.alert_txt);
                $('input[data-search-checkbox=select-all]').prop('checked', false).trigger('change');
                $(this).parent().val(''); // reset
            }
        });
    });

    bindGrid();

    function bindGrid() {
        // Load Template
        var template = _.template(
            $("script.grid-template").html()
        );

        // AJAX Data
        showLoader();
        $.getJSON(settings.getlist_url + query, function (data) {
            $(".afx-results-table tbody").html(
                template(data) // Populate Template
            );

            // Wire Pagination
            $(".afx-ctrls-pagination").html(data.pagination).find("a").click(function (e) {
                e.preventDefault();
                showLoader();
                var url = $(this).attr("href");
                page_num = getParameterByName("ccm_paging_p", url);
                page_size = getParameterByName("page_size", url);
                query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;
                bindGrid();
            });

            // Wire Grid Commands
            $(".afx-results-table .grid-command").click(function (e) {
                e.preventDefault();
                if($(this).hasClass('afx-delete')) {
                    if(!confirm(settings.delete_txt)) return;
                }
                if($(this).hasClass('afx-create')) {
                    var item = $(this).attr('data-item');
                    $.fn.dialog.open({
                        href: settings.add_url,
                        title: settings.edit_txt,
                        width: '660',
                        height: '550',
                        data: item,
                        modal: true,
                        close: function() {
                            bindGrid();
                            $(this).dialog('destroy');
                        }
                    });
                    return;
                }
                showLoader();
                var cmd = $(this).attr("href");
                $.ajax({
                    url: cmd
                }).done(function () {
                    bindGrid();
                });
            });

            // Wire checkbox
            $('input[data-search-checkbox=select-all]').click(function (e) {
                $('input[data-search-checkbox=individual]').prop('checked', $(this).is(':checked')).trigger('change');
            });

            // Populate "Showing..."
            $("#entrydisplay").html(data.entrydisplay);

            hideLoader();
        });
    }
};

var AfxUrlsNotFoundList = function (settings) {
    var query, page_num, page_size, page_sort, page_direction;

    // Init
    page_num = settings.page_num;
    page_size = settings.page_size;
    page_sort = settings.page_sort;
    page_direction = settings.page_direction;
    query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;

    // Wire up Grid

    // Page Sorting
    $('.afx-results-table thead a').on('click', function(e) {
        e.preventDefault();

        // Direction
        page_direction = "asc";

        if ($(this).parent().hasClass('ccm-results-list-active-sort-desc')) {
            $('.afx-results-table thead th').each(function () {
                $(this).attr('class', '');
            });
            $(this).parent().attr('class', 'ccm-results-list-active-sort-asc');
            page_direction = "asc";
        } else if ($(this).parent().hasClass('ccm-results-list-active-sort-asc')) {
            $('.afx-results-table thead th').each(function () {
                $(this).attr('class', '');
            });
            $(this).parent().attr('class', 'ccm-results-list-active-sort-desc');
            page_direction = "desc";
        } else {
            $('.afx-results-table thead th').each(function () {
                $(this).attr('class', '');
            });
            $(this).parent().attr('class', 'ccm-results-list-active-sort-asc');
            page_direction = "asc";
        }

        // Sort Order
        page_sort = $(this).attr('data-sort');

        query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;
        bindGrid();
    });

    // Page Size
    $("#entryselect").change(function () {
        $(this).find("option:selected").each(function () {
            page_size = $(this).val();
            query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;
            bindGrid();
        });
    });

    $("#bulkaction").change(function () {
        $(this).find("option:selected").each(function () {
            var cmd = $(this).val();
            var txt = $(this).text();
            var rIDs = '';
            $('.afx-results-table .afx-grid-cell').each(function (i) {
                if ($(this).find('input[data-search-checkbox=individual]').prop('checked')) {
                    rIDs += $(this).attr('data-id') + ",";
                }
            });
            if (cmd == 'delete_all') rIDs = '0';
            rIDs = rIDs.replace(/,\s*$/, ""); // del trailing
            if (cmd.length && rIDs.length) {
                if (confirm(settings.confirm_txt.format(txt))) {
                    $.ajax({
                        url: settings.bulkaction_url,
                        type: 'post',
                        data: {
                            "bulkaction": $(this).val(),
                            "rIDs": rIDs
                        }
                    }).done(function () {
                        bindGrid();
                    });

                    $('input[data-search-checkbox=select-all]').prop('checked', false).trigger('change');
                    $('input[data-search-checkbox=individual]').prop('checked', false).trigger('change');
                    $(this).parent().val(''); // reset
                }
            } else {
                alert(settings.alert_txt);
                $('input[data-search-checkbox=select-all]').prop('checked', false).trigger('change');
                $(this).parent().val(''); // reset
            }
        });
    });

    bindGrid();

    function bindGrid() {
        // Load Template
        var template = _.template(
            $("script.grid-template").html()
        );

        // AJAX Data
        showLoader();
        $.getJSON(settings.getlist_url + query, function (data) {
            $(".afx-results-table tbody").html(
                template(data) // Populate Template
            );

            // Wire Pagination
            $(".afx-ctrls-pagination").html(data.pagination).find("a").click(function (e) {
                e.preventDefault();
                showLoader();
                var url = $(this).attr("href");
                page_num = getParameterByName("ccm_paging_p", url);
                page_size = getParameterByName("page_size", url);
                query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;
                bindGrid();
            });

            // Wire Grid Commands
            $(".afx-results-table .grid-command").click(function (e) {
                e.preventDefault();
                if($(this).hasClass('afx-delete')) {
                    if(!confirm(settings.delete_txt)) return;
                }
                if($(this).hasClass('afx-create')) {
                    var item = $(this).attr('data-item');
                    $.fn.dialog.open({
                        href: settings.add_url,
                        title: settings.edit_txt,
                        width: '660',
                        height: '550',
                        data: item,
                        modal: true,
                        close: function() {
                            bindGrid();
                            $(this).dialog('destroy');
                        }
                    });
                    return;
                }
                showLoader();
                var cmd = $(this).attr("href");
                $.ajax({
                    url: cmd
                }).done(function () {
                    bindGrid();
                });
            });

            // Wire checkbox
            $('input[data-search-checkbox=select-all]').click(function (e) {
                $('input[data-search-checkbox=individual]').prop('checked', $(this).is(':checked')).trigger('change');
            });

            // Populate "Showing..."
            $("#entrydisplay").html(data.entrydisplay);

            hideLoader();
        });
    }
};

var AfxRedirectRuleEditor = function () {

    // Wire Help Buttons
    $("#btn-show-help").on("click", function () {
        $("#btn-show-help").toggle();
        $("#btn-hide-help").toggle();
        $("#afx-editor-help").slideToggle();
    });
    $("#btn-hide-help").on("click", function () {
        $("#btn-show-help").toggle();
        $("#btn-hide-help").toggle();
        $("#afx-editor-help").slideToggle();
    });

    // Wire Radio Buttons
    var tog = "rIsRegEx";
    var sel = $('#rIsRegEx').val();
    updateToggle(tog, sel);

    $('div.radioBtn a').on('click', function () {
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        updateToggle(tog, sel);
    });

    // Wire Response Code
    $('#rResponseCode').on('change', function () {
        updateView();
    });

    // Wire Redirect Type
    $('#rule-editor #redirectToType').on('change', function () {
        $('div.redirect-to-type').hide('fast');
        $('div.redirect-to-type-' + $(this).val()).show('fast');
    });

    function updateToggle(tog, sel) {
        $('#' + tog).prop('value', sel);
        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
        updateView();
    }

    function updateView() {
        var sel = $('#rIsRegEx').val();
        var code = $('#rResponseCode').val();
        var roldurl = true;
        var rregex = true;
        var rtype = true;
        var rtoregex = true;
        var rblock = true;
        var rblockdestination = true;
        var rregexdestination = true;
        if (sel == 0) {
            roldurl = true;
            rregex = false;
            rtype = true;
            rtoregex = false;
            rblock = true;
            rblockdestination = true;
            rregexdestination = false;
        }
        if (sel == 1) {
            roldurl = false;
            rregex = true;
            rtype = false;
            rtoregex = true;
            rblock = false;
            rblockdestination = false;
            rregexdestination = true;
        }
        if(code == 410 || code == 451) {
            rblockdestination = false;
            rregexdestination = false;
        }
        roldurl ? $('.redirect-oldurl').show() : $('.redirect-oldurl').hide();
        rregex ? $('.redirect-regex').show() : $('.redirect-regex').hide();
        rtype ? $('.redirect-type').show() : $('.redirect-type').hide();
        rtoregex ? $('.redirect-toregex').show() : $('.redirect-toregex').hide();
        rblock ? $('.redirect-to-type-block').show() : $('.redirect-to-type-block').hide();
        rblockdestination ? $('.redirect-destination').show() : $('.redirect-destination').hide();
        rregexdestination ? $('.redirect-toregex-destination').show() : $('.redirect-toregex-destination').hide();
    }
};

var AfxRedirectRulesList = function (settings) {
    var query, page_num, page_size, page_sort, page_direction, sortOrderStart, sortOrderEnd, is_grid_locked;

    // Init
    page_num = settings.page_num;
    page_size = settings.page_size;
    page_sort = settings.page_sort;
    page_direction = settings.page_direction;
    is_grid_locked = false;
    sortOrderStart = '';
    sortOrderEnd = '';
    query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;

    // Wire up Add SEO Redirect Button
    $("#afx-add-redirect").on("click", function () {
        $.fn.dialog.open({
            href: settings.add_url,
            title: settings.edit_txt,
            width: '660',
            height: '550',
            data: '',
            modal: true,
            close: function() {
                bindGrid();
                $(this).dialog('destroy');
            }
        });
    });

    // Wire up Quick 301 Add
    $('a[data-selector-link=choose-file]').html(settings.file_txt).on('click', function(e) {
        e.preventDefault();
        ConcreteFileManager.launchDialog(function (data) {
			$("#rToFID").val(data.fID);
            ConcreteFileManager.getFileDetails(data.fID, function(r) {
                jQuery.fn.dialog.hideLoader();
                var file = r.files[0];				                
				$('a[data-selector-link=choose-file]').html(file.fileName);
			});
		});     
	}); 

    $('a[data-selector-link=choose-page]').html(settings.page_txt).on('click', function(e) {
        e.preventDefault();
        ConcretePageAjaxSearch.launchDialog(function(data) {
			$("#rToCID").val(data.cID);
			$('a[data-selector-link=choose-page]').html(data.title);
		});     
	}); 

    var $s = $('#afx-quick-add-container #redirectToType');
    $s.on('change', function () {
        $('div.afx-r-type').hide();
        $('div.afx-r-type-' + $s.val()).show();
    });

    $("#afx-quick-add").submit(function (event) {
        event.preventDefault();
        $.ajax({
            url: settings.quickadd_url,
            dataType: 'json',
            type: 'post',
            data: $('#afx-quick-add').serialize(),
            beforeSend: function () {
                $("#afx-quick-add-validation").html('');
                showLoader();
            }
        }).done(function (data) {
            if (data.success) {
                $("#rFrom").val('');
                $("#rToCID").val(0);
                $("#rToFID").val(0);
                $("#rToURL").val('');
				$('a[data-selector-link=choose-page]').html(settings.page_txt);
				$('a[data-selector-link=choose-file]').html(settings.file_txt);
                bindGrid('');
            } else {
                var template = _.template($("script.validation-template").html());
                $("#afx-quick-add-validation").html(template(data));
                hideLoader();
            }
        });
    });

    // Wire up Rules Grid

    // Page Sorting
    $('.afx-results-table thead a').on('click', function(e) {
        e.preventDefault();

        // Direction
        page_direction = "asc";

        if ($(this).parent().hasClass('ccm-results-list-active-sort-desc')) {
            $('.afx-results-table thead th').each(function () {
                $(this).attr('class', '');
            });
            $(this).parent().attr('class', 'ccm-results-list-active-sort-asc');
            page_direction = "asc";
        } else if ($(this).parent().hasClass('ccm-results-list-active-sort-asc')) {
            $('.afx-results-table thead th').each(function () {
                $(this).attr('class', '');
            });
            $(this).parent().attr('class', 'ccm-results-list-active-sort-desc');
            page_direction = "desc";
        } else {
            $('.afx-results-table thead th').each(function () {
                $(this).attr('class', '');
            });
            $(this).parent().attr('class', 'ccm-results-list-active-sort-asc');
            page_direction = "asc";
        }

        // Sort Order
        page_sort = $(this).attr('data-sort');

        // Grid Locking (disable DnD)
        is_grid_locked = !(page_sort == "rSort" && page_direction == "asc");

        query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;
        bindGrid();
    });

    // page size
    $("#entryselect").change(function () {
        $(this).find("option:selected").each(function () {
            page_size = $(this).val();
            query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;
            bindGrid();
        });
    });

    // bulk actions
    $("#bulkaction").change(function () {
        $(this).find("option:selected").each(function () {
            var cmd = $(this).val();
            var txt = $(this).text();
            var rIDs = '';
            $('.afx-results-table .afx-grid-cell').each(function (i) {
                if ($(this).find('input[data-search-checkbox=individual]').prop('checked')) {
                    rIDs += $(this).attr('data-id') + ",";
                }
            });
            if (cmd == 'delete_all') rIDs = '0';
            rIDs = rIDs.replace(/,\s*$/, ""); // del trailing
            if (cmd.length && rIDs.length) {
                if (confirm(settings.confirm_txt.format(txt))) {
                    $.ajax({
                        url: settings.bulkaction_url,
                        type: 'post',
                        data: {
                            "bulkaction": $(this).val(),
                            "rIDs": rIDs
                        }
                    }).done(function () {
                        bindGrid();
                    });

                    $('input[data-search-checkbox=select-all]').prop('checked', false).trigger('change');
                    $('input[data-search-checkbox=individual]').prop('checked', false).trigger('change');
                    $(this).parent().val(''); // reset
                }
            } else {
                alert(settings.alert_txt);
                $('input[data-search-checkbox=select-all]').prop('checked', false).trigger('change');
                $(this).parent().val(''); // reset
            }
        });
    });

    bindGrid();

    function bindGrid() {
        // Load Template
        var template = _.template(
            $("script.grid-template").html()
        );

        // AJAX Data
        showLoader();
        $.getJSON(settings.getlist_url + query, function (data) {
            $(".afx-results-table tbody").html(
                template(data) // Populate Template
            ).mousedown(function () {
                sortOrderStart = ''; // Get starting Sort
                $(this).find("tr").each(function (i) {
                    sortOrderStart += $(this).find('td:eq(0)').attr('data-id') + ",";
                });
            }).sortable({
                disabled: is_grid_locked,
                placeholder: "ui-state-highlight",
                handle: ".ui-dnd-handle",
                cancel: ".ui-dnd-cancel",
                delay: 100,
                distance: 4,
                forcePlaceholderSize: true,
                helper: fixWidthHelper,
                update: function () {
                    showLoader();
                    sortOrderEnd = ''; // Get resulting sort after DnD
                    $(this).find("tr").each(function (i) {
                        var td = $(this).find('td:eq(0)');
                        sortOrderEnd += $(td).attr('data-id') + ",";
                    });
                    var post_data = {}; // ajax call
                    post_data['sortOrderStart'] = sortOrderStart.replace(/,\s*$/, ""); // del trailing
                    post_data['sortOrderEnd'] = sortOrderEnd.replace(/,\s*$/, ""); // del trailing
                    $.ajax({
                        url: settings.sort_url,
                        dataType: 'html',
                        type: 'post',
                        data: post_data
                    }).done(function () {
                        bindGrid('');
                    });
                }
            }).disableSelection();

            // Wire Pagination
            $(".afx-ctrls-pagination").html(data.pagination).find("a").click(function (e) {
                e.preventDefault();
                showLoader();
                var url = $(this).attr("href");
                page_num = getParameterByName("ccm_paging_p", url);
                page_size = getParameterByName("page_size", url);
                query = "&ccm_paging_p=" + page_num + "&page_size=" + page_size + "&page_sort=" + page_sort + "&page_direction=" + page_direction;
                bindGrid();
            });

            // Wire Grid Commands
            $(".afx-results-table .grid-command").click(function (e) {
                e.preventDefault();
                if($(this).hasClass('afx-delete')) {
                    if(!confirm(settings.delete_txt)) return;
                }
                if($(this).hasClass('afx-edit')) {
                    var itemid = $(this).attr('data-itemid');
                    $.fn.dialog.open({
                        href: settings.edit_url,
                        title: settings.edit_txt,
                        width: '660',
                        height: '550',
                        data: itemid,
                        modal: true,
                        close: function() {
                            bindGrid();
                            $(this).dialog('destroy');
                        }
                    });
                    return;
                }
                showLoader();
                var cmd = $(this).attr("href");
                $.ajax({
                    url: cmd
                }).done(function () {
                    bindGrid();
                });
            });

            // Wire checkbox
            $('input[data-search-checkbox=select-all]').click(function (e) {
                $('input[data-search-checkbox=individual]').prop('checked', $(this).is(':checked')).trigger('change');
            });

            // Disable sorting
            if(is_grid_locked) {
                $('.ui-dnd-handle a').each(function () {
                    $(this).attr('href', '').addClass('disabled');
                });
                $('.ui-dnd-handle div').addClass('disabled');
            }

            // Populate "Showing..."
            $("#entrydisplay").html(data.entrydisplay);

            hideLoader();
        });
    }
};

var AfxSettingsEditor = function (settings, validation) {

    // Validation
    if (validation != null) {
        var template = _.template($("script.validation-template").html());
        $("#afx-settings-editor-validation").html(template(validation));
    }

    update("sProcessRedirects", $('#sProcessRedirects').val());
    update("sOnly404s", $('#sOnly404s').val());
    update("sRedirectSuperAdmin", $('#sRedirectSuperAdmin').val());
    update("sLogURLsNotFound", $('#sLogURLsNotFound').val());

    $('div.radioBtn a').on('click', function () {
        var sel = $(this).data('title');
        var tog = $(this).data('toggle');
        update(tog, sel);
    });

    $('.afx-add-ignore').on('click', function(e) {
        e.preventDefault();
        var list = JSON.parse($('#sIgnoreList').val());
        var list_item = $('.afx-ignore-item').val().trim();
        if (list_item.charAt(0) == "/") list_item = list_item.substr(1);
        if (list_item.length > 0) {
            list_item = "/" + list_item;
            list.ignore_list.push({"id": list_item.hashcode(), "url": list_item});
            $('#sIgnoreList').val(JSON.stringify(list));
        }
        bindGrid();
        $('.afx-ignore-item').val('');
    });

    bindGrid();
    $('#redirect-settings').show();

    function bindGrid() {
        // load
        var list = JSON.parse($('#sIgnoreList').val());
        if (list != null) {
            var template = _.template($("script.grid-template").html());
            $(".afx-ignore-list tbody").html(template(list));
        }
        // wire delete
        $(".afx-ignore-list .afx-delete").click(function (e) {
            e.preventDefault();
            var itemid = $(this).attr('data-id');
            if (confirm(settings.delete_txt)) {
                var list = JSON.parse($('#sIgnoreList').val());
                remove_item(list.ignore_list, itemid);
                $('#sIgnoreList').val(JSON.stringify(list));
                bindGrid();
            }
        });
    }

    function remove_item(arr, id) {
        for (var i = 0; i < arr.length; i++) {
            var cur = arr[i];
            if (cur.id == id) {
                arr.splice(i, 1);
                break;
            }
        }
    }

    function update(tog, sel) {
        $('#' + tog).prop('value', sel);
        $('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]')
            .removeClass('active').addClass('notActive');
        $('a[data-toggle="' + tog + '"][data-title="' + sel + '"]')
            .removeClass('notActive').addClass('active');
    }
};

// sprintf
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ?
                args[number] :
                match;
        });
    };
}

// string hash
if (!String.prototype.hashCode) {
    String.prototype.hashCode = function() {
        var hash = 0, i = 0, len = this.length, chr;
        while ( i < len ) {
            hash  = ((hash << 5) - hash + this.charCodeAt(i++)) << 0;
        }
        return hash;
    };
}
if (!String.prototype.hashcode) {
    String.prototype.hashcode = function () {
        return (this.hashCode() + 2147483647) + 1;
    };
}

function fixWidthHelper(e, ui) {
    ui.children().each(function () {
        $(this).width($(this).width());
    });
    return ui;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function showLoader() {
    if (typeof NProgress !== "undefined") NProgress.start();
    else $('body').addClass('ccm-loading');
}

function hideLoader() {
    if (typeof NProgress !== "undefined") NProgress.done();
    else $('body').removeClass('ccm-loading');
}