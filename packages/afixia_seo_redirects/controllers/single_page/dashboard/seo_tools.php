<?php

namespace Concrete\Package\AfixiaSeoRedirects\Controller\SinglePage\Dashboard;

defined('C5_EXECUTE') or die("Access Denied.");

use Page;
use Permissions;
use Concrete\Core\Page\Controller\DashboardPageController;

class SeoTools extends DashboardPageController
{
    public function view()
    {
        $this->enableNativeMobile();
        $categories = array();
        $c = Page::getByPath('/dashboard/system/seo');
        $children = $c->getCollectionChildrenArray(true);
        foreach ($children as $cID) {
            $nc = Page::getByID($cID, 'ACTIVE');
            $ncp = new Permissions($nc);
            if ($ncp->canRead()) {
                $categories[] = $nc;
            }
        }
        $this->set('categories', $categories);

        return;
    }
}
