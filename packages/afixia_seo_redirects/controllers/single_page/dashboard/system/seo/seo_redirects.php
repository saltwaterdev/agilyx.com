<?php

namespace Concrete\Package\AfixiaSeoRedirects\Controller\SinglePage\Dashboard\System\Seo;

defined('C5_EXECUTE') or die("Access Denied.");

use Concrete\Core\Page\Controller\DashboardPageController;

class SeoRedirects extends DashboardPageController
{
    public function view()
    {
        $this->redirect('/dashboard/system/seo/seo_redirects/redirect_rules');
        return;
    }
}
