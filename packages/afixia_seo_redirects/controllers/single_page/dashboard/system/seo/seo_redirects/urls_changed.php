<?php

namespace Concrete\Package\AfixiaSeoRedirects\Controller\SinglePage\Dashboard\System\Seo\SeoRedirects;

defined('C5_EXECUTE') or die("Access Denied.");

use AssetList;
use Concrete\Core\Page\Controller\DashboardPageController;

class UrlsChanged extends DashboardPageController
{
    public function view()
    {
	    $this->requireAsset('afixia_seo_redirects');
        return;
    }
}
