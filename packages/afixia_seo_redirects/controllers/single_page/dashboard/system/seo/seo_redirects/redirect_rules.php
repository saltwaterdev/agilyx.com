<?php

namespace Concrete\Package\AfixiaSeoRedirects\Controller\SinglePage\Dashboard\System\Seo\SeoRedirects;

defined('C5_EXECUTE') or die("Access Denied.");

use Config;
use Page;
use Permissions;
use AssetList;
use Concrete\Core\Page\Controller\DashboardPageController;

class RedirectRules extends DashboardPageController
{
    public function view()
    {
	    $this->requireAsset('afixia_seo_redirects');
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $this->set('subfolder_install', Config::get('afixia_seo_redirects.subfolder_install'));
        $this->set('prettyUrlArray', $this->prettyUrlMessage());
        $this->set('subfolderUrlArray', $this->subfolderUrlMessage());
        $this->set('rRedirectToTypeOptions', $this->getRedirectToTypeOptions());
        return;
    }

    public function getResponseCodeOptions()
    {
        $options = array();
        $options['0'] = t('Please select');
        $options['301'] = t('301 - Moved Permanently');
        $options['302'] = t('302 - Found');
        $options['307'] = t('307 - Temporary Redirect');
        $options['410'] = t('410 - Gone (Content Deleted)');
        $options['451'] = t('451 - Unavailable For Legal Reasons');
        return $options;
    }

    public function getRedirectToTypeOptions()
    {
        $options = array();
        $options['cid'] = t('Page');
        $options['fid'] = t('File');
        $options['url'] = t('URL');
        return $options;
    }

    public function prettyUrlMessage()
    {
        $result = false;

        if (!Config::get('concrete.seo.url_rewriting')) { //if Pretty URLs is not enabled
            $pretty_urls_page = Page::getByPath('/dashboard/system/seo/urls', 'ACTIVE');
            if (is_object($pretty_urls_page)) { //if the Pretty URLs page is an object / exists
                $pretty_permissions = new Permissions($pretty_urls_page);
                if ($pretty_permissions->canRead()) { //if this user can read the Pretty URL page
                    $result = array('pretty_urls_link_start' => '<a href="' . $pretty_urls_page->getCollectionLink() . '">', 'pretty_urls_link_end' => '</a>');
                }
            }
        }

        return $result;
    }

    public function subfolder()
    {
        $subfolder = false;
        $pos = strpos($_SERVER['PHP_SELF'], '/index.php');
        if ($pos !== false) {
            $subfolder = rtrim(ltrim(substr($_SERVER['PHP_SELF'], 0, $pos), '/'), '/');
        }

        if (strlen($subfolder) > 0) {
            return $subfolder;
        } else {
            return false;
        }
    }

    public function subfolderUrlMessage()
    {
        $result = false;

        if (empty(Config::get('afixia_seo_redirects.subfolder_install'))) { //if nothing defined for a subfolder
            $subfolder = $this->subfolder();
            if ($subfolder) { //if we detect a subfolder
                $redirect_settings_page = Page::getByPath('/dashboard/system/seo/seo_redirects/redirect_settings', 'ACTIVE');
                if (is_object($redirect_settings_page)) { //if the Redirect Settings page is an object / exists
                    $redirect_settings_permissions = new Permissions($redirect_settings_page);
                    if ($redirect_settings_permissions->canRead()) { //if this user can read the Redirect Settings page
                        $result = array('redirect_settings_link_start' => '<a href="' . $redirect_settings_page->getCollectionLink() . '">', 'redirect_settings_link_end' => '</a>', 'subfolder' => $subfolder);
                    }
                }
            }
        }

        return $result;
    }
}

