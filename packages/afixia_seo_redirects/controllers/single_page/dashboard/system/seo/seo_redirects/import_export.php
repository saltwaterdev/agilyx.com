<?php

namespace Concrete\Package\AfixiaSeoRedirects\Controller\SinglePage\Dashboard\System\Seo\SeoRedirects;

defined('C5_EXECUTE') or die("Access Denied.");

use AssetList;
use Core;
use Exception;
use Database;
use DateTime;
use ORM;
use Page;
use File;
use Concrete\Package\AfixiaSeoRedirects\Src\RedirectRule;
use Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectRuleEntity;
use Concrete\Core\Page\Controller\DashboardPageController;

class ImportExport extends DashboardPageController
{
    public function view()
    {
        $this->requireAfixiaSeoAssets();
    }

    public function import($token = '')
    {
        $this->requireAfixiaSeoAssets();
        $valt = Core::make('helper/validation/token');

        if (!$valt->validate('', $token)) {
            $this->redirect('/dashboard/system/seo/seo_redirects/import_export');
        } else {
            // validation round #1
            $validationList = $this->validate_csv_upload($_FILES, 'import-file-selector');
            $em = ORM::entityManager();

            if (count($validationList) == 0) {
                // validation round #2
                $validationList = array(); // next round of validation

                $filename = $_FILES['import-file-selector']['tmp_name'];
                $row_count = 0;
                $delimiter = ',';
                $enclosure = '"';

                // Normalize Sort
                $rep = RedirectRule::getRepository();
                $rules = $rep->createQueryBuilder('r')->orderBy('r.rSort', 'ASC')->getQuery()->getResult();
                $sort_order = 1;
                foreach ($rules as $rule) {
                    $rule->rSort = $sort_order++;
                    $em->persist($rule);
                }
                $em->flush();
                $em->beginTransaction();

                //Read CSV
                if (file_exists($filename) && is_readable($filename)) {
                    if (($handle = fopen($filename, 'r')) !== FALSE) {
                        while (($row = fgetcsv($handle, 1000, $delimiter, $enclosure)) !== FALSE) {
                            $row_count++;

                            // Validate Header
                            if ($row_count == 1) {
                                $valid_header = "From_URL,To_URL,To_PageID,To_FileID,Response_Code";
                                $test_header = implode(',', $row);

                                if ($test_header != $valid_header) {
                                    $validationList[] .= t('Invalid column names in CSV');
                                    $validationList[] .= t('Correct Columns: ') . $valid_header;
                                    $validationList[] .= t('Uploaded Columns: ') . $test_header;
                                    break;
                                }

                                continue;
                            }

                            // Import Data
                            echo $row->count;
                            if (count($row) == 5) {
                                try {
                                    // from URL
                                    $parsed_from_url = parse_url($row[0]);
                                    $from_path = $parsed_from_url["path"];
                                    $from_query = $parsed_from_url["query"];
                                    if(strlen($from_query) > 0) {
                                        $from_path .= '?' . $from_query;
                                    }

                                    // to URL
                                    $rToURL = $row[1];
//                                    $parsed_to_url = parse_url($row[1]);
//                                    $to_path = $parsed_to_url["path"];
//                                    $to_query = $parsed_to_url["query"];
//                                    $rToURL = $to_path;
//                                    if(strlen($to_query) > 0) {
//                                        $rToURL .= '?' . $to_query;
//                                    }

                                    $rToCID = 0;
                                    if (is_numeric($row[2])) {
                                        $rToCID = intval($row[2]);
                                    }

                                    // validate page
                                    if($rToCID > 0) {
                                        $page = Page::getByID($rToCID);
                                        $rToURL = ''; //Use the Page ID
                                        if (!is_object($page)) {
                                            $rToCID = 0;
                                            $validationList[] .= t('Error: Page ID does not exist. To_PageID ') . $row[2] . t(' in CSV row ') . $row_count;
                                        }
                                    }

                                    $rToFID = 0;
                                    if (is_numeric($row[3])) {
                                        $rToFID = intval($row[3]);
                                    }

                                    // validate file
                                    if($rToFID > 0) {
                                        $file = File::getByID($rToFID);
                                        $rToURL = ''; //Use the File ID
                                        if (!is_object($file)) {
                                            $rToFID = 0;
                                            $validationList[] .= t('Error: File ID does not exist. To_FileID ') . $row[3] . t(' in CSV row ') . $row_count;
                                        }
                                    }

                                    $response_code = 0;
                                    if (is_numeric($row[4])) {
                                        $temp = intval($row[4]);
                                        if ($temp == 301 ||
                                            $temp == 302 ||
                                            $temp == 307 ||
                                            $temp == 410 ||
                                            $temp == 451
                                        ) {
                                            $response_code = $temp;
                                        } else {
                                            $validationList[] .= t('Invalid Response_Code ') . $row[4] . t(' in CSV row ') . $row_count;
                                        }
                                    }

                                    $rdr = new RedirectRuleEntity();
                                    $rdr->rActive = 1;
                                    $rdr->rIsWildCard = 0;
                                    $rdr->rIsRegEx = 0;
                                    $rdr->rFrom = urldecode($from_path);
                                    $rdr->rToCID = $rToCID;
                                    $rdr->rToFID = $rToFID;
                                    $rdr->rToURL = $rToURL;
                                    $rdr->rResponseCode = $response_code;
                                    $rdr->rSort = $sort_order++;
                                    $rdr->rCount = 0;
                                    $rdr->rLastUsed =  new DateTime();
                                    $em->persist($rdr);
                                    $em->flush();
                                } catch (Exception $e) {
                                    $validationList[] .= $e->getMessage() . t(' in CSV row ') . $row_count;
                                }
                            } else {
                                $validationList[] .= t('Missing fields in CSV row number ') . $row_count;
                            }
                        }
                    } else {
                        $validationList[] .= t('File unreadable');
                    }
                } else {
                    $validationList[] .= t('File unreadable');
                }

                if (count($validationList) == 0) {
                    $em->commit();

                    // Normalize Sort
                    $rep = RedirectRule::getRepository();
                    $rules = $rep->createQueryBuilder('r')->orderBy('r.rSort', 'ASC')->getQuery()->getResult();
                    $sort_order = 1;
                    foreach ($rules as $rule) {
                        $rule->rSort = $sort_order++;
                        $em->persist($rule);
                    }
                    $em->flush();

                    $this->set('message', t('Import Successful!'));
                    $this->view();
                } else {
                    // validation round #2
                    $em->rollback();
                    $validationList[] .= t('No records imported. Fix CSV errors and try again.');
                    $this->set('validation', json_encode(array('success' => count($validationList) ? 0 : 1,
                        'validationList' => $validationList)));
                    return;
                }
            } else {
                // validation round #1
                $validationList[] .= t('No records imported. Fix CSV errors and try again.');
                $this->set('validation', json_encode(array('success' => count($validationList) ? 0 : 1,
                    'validationList' => $validationList)));
                return;
            }
        }
    }

    public function restore($token = '')
    {
        $this->requireAfixiaSeoAssets();
        $valt = Core::make('helper/validation/token');

        if (!$valt->validate('', $token)) {
            $this->redirect('/dashboard/system/seo/seo_redirects/import_export');
        } else {
            // validation round #1
            $validationList = $this->validate_csv_upload($_FILES, 'restore-file-selector');

            if (count($validationList) == 0) {
                // validation round #2
                $validationList = array(); // next round of validation

                $filename = $_FILES['restore-file-selector']['tmp_name'];
                $row_count = 0;
                $delimiter = ',';
                $enclosure = '"';

                $em = ORM::entityManager();
                $em->beginTransaction();

                //Read CSV
                if (file_exists($filename) && is_readable($filename)) {
                    if (($handle = fopen($filename, 'r')) !== FALSE) {
                        while (($row = fgetcsv($handle, 1000, $delimiter, $enclosure)) !== FALSE) {
                            $row_count++;

                            // Validate Header
                            if ($row_count == 1) {
                                $valid_header = "rID,rActive,rIsWildCard,rIsRegEx,rFrom,rToCID,rToFID,rToURL,rResponseCode,rSort,rCount,rLastUsed";
                                $test_header = implode(',', $row);

                                if ($test_header != $valid_header) {
                                    $validationList[] .= t('Invalid column names in CSV');
                                    $validationList[] .= t('Correct Columns: ') . $valid_header;
                                    $validationList[] .= t('Uploaded Columns: ') . $test_header;
                                    break;
                                }

                                // Valid Header, Clear Table
                                $db = Database::connection();
                                $db->query('DELETE FROM afxRedirectRules');
                                continue;
                            }

                            // Import Data
                            echo $row->count;
                            if (count($row) == 12) {
                                try {
                                    $rdr = new RedirectRuleEntity();
                                    $rdr->rActive = $row[1];
                                    $rdr->rIsWildCard = $row[2];
                                    $rdr->rIsRegEx = $row[3];
                                    $rdr->rFrom = $row[4];
                                    $rdr->rToCID = $row[5];
                                    $rdr->rToFID = $row[6];
                                    $rdr->rToURL = $row[7];
                                    $rdr->rResponseCode = $row[8];
                                    $rdr->rSort = $row[9];
                                    $rdr->rCount = $row[10];
                                    $rdr->rLastUsed =  new DateTime($row[11]);
                                    $em->persist($rdr);
                                    $em->flush();
                                } catch (Exception $e) {
                                    $validationList[] .= $e->getMessage() . t(' in CSV row ') . $row_count;
                                }
                            } else {
                                $validationList[] .= t('Missing fields in CSV row number ') . $row_count;
                            }
                        }
                    } else {
                        $validationList[] .= t('File unreadable');
                    }
                } else {
                    $validationList[] .= t('File unreadable');
                }

                if (count($validationList) == 0) {
                    $em->commit();
                    $this->set('message', t('Restore Successful!'));
                    $this->view();
                } else {
                    // validation round #2
                    $em->rollback();
                    $validationList[] .= t('No records imported. Fix CSV errors and try again.');
                    $this->set('validation', json_encode(array('success' => count($validationList) ? 0 : 1,
                        'validationList' => $validationList)));
                    return;
                }
            } else {
                // validation round #1
                $validationList[] .= t('No records imported. Fix CSV errors and try again.');
                $this->set('validation', json_encode(array('success' => count($validationList) ? 0 : 1,
                    'validationList' => $validationList)));
                return;
            }
        }
    }

    public function validate_csv_upload($files, $file_input_name)
    {
        $validationList = array();

        if (isset($_FILES[$file_input_name])) {
            $allowedExts = array("csv");
            $temp = explode(".", $_FILES[$file_input_name]["name"]);
            $extension = end($temp);

            if ($_FILES[$file_input_name]["error"] > 0) {
                $validationList[] .= t('Error opening the file');
            }
            // if ($_FILES[$file_input_name]["type"] != "text/csv") {
            //     $validationList[] .= t('Mime type not allowed');
            // }
            if (!in_array($extension, $allowedExts)) {
                $validationList[] .= t('Extension not allowed');
            }
            if ($_FILES[$file_input_name]["size"] <= 0) {
                $validationList[] .= t('File size should be less than 0 kB');
            }
        } else {
            $validationList[] .= t('File not received');
        }

        return $validationList;
    }

    public function backup($token = '')
    {
        $this->requireAfixiaSeoAssets();

        $valt = Core::make('helper/validation/token');
        if (!$valt->validate('', $token)) {
            $this->redirect('/dashboard/system/seo/seo_redirects/import_export');
        } else {
            header("Content-Type: text/csv");
            header("Cache-control: private");
            header("Pragma: public");

            $fileName = "afixia-seo-redirects-backup-rules";
            $date = date('Y-m-d');
            header("Content-Disposition: attachment; filename=" . $fileName . "-{$date}.csv");

            // open output stream
            $fp = fopen('php://output', 'w');
            $delimiter = ',';
            $enclosure = '"';

            // write column names
            $em = ORM::entityManager();
            $field_names = $em->getClassMetadata('Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectRuleEntity')->getFieldNames();
            fputcsv($fp, $field_names, $delimiter, $enclosure);

            // get data rows
            $conn = $em->getConnection();
            $statement = $conn->executeQuery('SELECT * FROM afxRedirectRules ORDER BY rSort ASC');
            $rules = $statement->fetchAll();

            // write data rows
            foreach ($rules as $rule) {
                fputcsv($fp, $rule, $delimiter, $enclosure);
            }

            // close output stream
            fclose($fp);
            die;
        }
    }

    public function requireAfixiaSeoAssets()
    {
        $this->requireAsset('afixia_seo_redirects');
        $this->set('validation', 'null');
    }

    public function import_sample_csv($token = '')
    {
        $valt = Core::make('helper/validation/token');
        if (!$valt->validate('', $token)) {
            $this->redirect('/dashboard/system/seo/seo_redirects/import_export');
        } else {
            // output headers so that the file is downloaded rather than displayed
            header("Content-Type: text/csv");
            header("Cache-control: private");
            header("Pragma: public");
            header("Content-Disposition: attachment; filename=afixia-seo-redirects-import-sample.csv");

            // open output stream
            $fp = fopen('php://output', 'w');

            // output the column headings
            fputcsv($fp, array('From_URL', 'To_URL', 'To_PageID', 'To_FileID', 'Response_Code'));
            // output records
            fputcsv($fp, array('https://www.urlimportexample.com/news/news/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('http://urlimportexample.com/news/ceo-column/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('https://urlimportexample.com/software/asset-management/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('http://urlimportexample.com/privacy-policy/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('https://urlimportexample.com/hardware/repair-maintanence/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('http://urlimportexample.com/news/blog/2015/05/some-blog-name/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('http://www.urlimportexample.com/index.php/contact/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('http://urlimportexample.com/hardware/computers/gaming/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('https://urlimportexample.com/replacement-program/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('http://urlimportexample.com/index.php/news/ceo-column/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('http://urlimportexample.com/mobile/index.php/download_file/-/view/187/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('http://urlimportexample.com/mobile/news/blog/2014/10/seo-redirects/', '/portfolio', '0', '0', '301'));
            fputcsv($fp, array('http://urlimportexample.com/about/', '/portfolio', '0', '0', '301'));
            //fputcsv($fp, array('______', '/portfolio', '0', '0', '301'));

            // close output stream
            fclose($fp);
            die;
        }
    }
}
