<?php

namespace Concrete\Package\AfixiaSeoRedirects\Controller\SinglePage\Dashboard\System\Seo\SeoRedirects;

defined('C5_EXECUTE') or die("Access Denied.");

use Config;
use AssetList;
use Concrete\Core\Page\Controller\DashboardPageController;

class RedirectSettings extends DashboardPageController
{
    public function view()
    {
	    $this->requireAsset('afixia_seo_redirects');
        $this->set('process_redirects', Config::get('afixia_seo_redirects.process_redirects'));
        $this->set('only_404s', Config::get('afixia_seo_redirects.only_404s'));
        $this->set('redirect_super_admin', Config::get('afixia_seo_redirects.redirect_super_admin'));
        $this->set('log_urls_not_found', Config::get('afixia_seo_redirects.log_urls_not_found'));
        $this->set('not_found_limit', Config::get('afixia_seo_redirects.not_found_limit'));
        $this->set('subfolder_install', Config::get('afixia_seo_redirects.subfolder_install'));
        $ignore_list = urldecode(Config::get('afixia_seo_redirects.ignore_list'));
        if(strlen($ignore_list) == 0) {$ignore_list = '{"ignore_list":[]}'; }
        $this->set('ignore_list', $ignore_list);
        $this->set('validation', 'null');
        return;
    }

    public function save()
    {
        $validationList = array();
        $this->set('validation', 'null');

        $sProcessRedirects = $this->post('sProcessRedirects') ? true : false;
        $sOnly404s = $this->post('sOnly404s') ? true : false;
        $sRedirectSuperAdmin = $this->post('sRedirectSuperAdmin') ? true : false;
        $sLogURLsNotFound = $this->post('sLogURLsNotFound') ? true : false;
        $sNotFoundLimit = 1000;

        $temp = $this->post('sNotFoundLimit');
        if (is_numeric($temp) && $temp >= 50) {
            $sNotFoundLimit = $temp;
        } else {
            $validationList[] .= t('URLs Not Found Limit must contain a value greater than or equal to 50.');
        }

        $subfolder_install_cleaned = rtrim(ltrim($this->post('sSubfolderInstall'), '/'), '/');

        $ignore_list = json_decode($this->post('sIgnoreList'));
        if($ignore_list === null) {
            $ignore_list = json_decode('{"ignore_list":[]}');
        }
        $ignore_list = urlencode(json_encode($ignore_list));

        // Save
        if (count($validationList) == 0) {
            Config::save('afixia_seo_redirects.process_redirects', $sProcessRedirects);
            Config::save('afixia_seo_redirects.only_404s', $sOnly404s);
            Config::save('afixia_seo_redirects.redirect_super_admin', $sRedirectSuperAdmin);
            Config::save('afixia_seo_redirects.log_urls_not_found', $sLogURLsNotFound);
            if($sNotFoundLimit) {
                Config::save('afixia_seo_redirects.not_found_limit', $sNotFoundLimit);
            }
            Config::save('afixia_seo_redirects.subfolder_install', $subfolder_install_cleaned);
            Config::save('afixia_seo_redirects.ignore_list', $ignore_list);
            $this->redirect('/dashboard/system/seo/seo_redirects/redirect_settings', 'saved');
            return;
        } else {
            $this->set('validation', json_encode(array('success' => count($validationList) ? 0 : 1,
                'validationList' => $validationList)));
            $this->view();
            return;
        }
    }

    public function saved() {
        $this->set('message', t('Settings Saved.'));
        $this->view();
    }
}