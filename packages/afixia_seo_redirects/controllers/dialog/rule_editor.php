<?php

namespace Concrete\Package\AfixiaSeoRedirects\Controller\Dialog;

defined('C5_EXECUTE') or die("Access Denied.");

use Core;
use Config;
use ORM;
use Page;
use Permissions;
use DateTime;
use Concrete\Core\Application\EditResponse;
use Concrete\Controller\Backend\UserInterface as BackendInterfaceController;
use Concrete\Package\AfixiaSeoRedirects\Src\RedirectRule;
use Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectRuleEntity;

class RuleEditor extends BackendInterfaceController
{
    protected $viewPath = 'dialogs/rule_editor';
    protected $canEdit = false;

    public function view()
    {
        return;
    }

    protected function canAccess()
    {
        $redirect_rules_page = Page::getByPath('/dashboard/system/seo/seo_redirects/redirect_rules', 'ACTIVE');
        if (is_object($redirect_rules_page)) {
            $redirect_settings_permissions = new Permissions($redirect_rules_page);
            if ($redirect_settings_permissions->canRead()) {
                $this->canEdit = true;
            }
        }

        return $this->canEdit;
    }

    public function add()
    {
        $fromURL = urldecode($_SERVER["QUERY_STRING"]);
        $dh = Core::make('helper/date');
        $now = new DateTime('now', $dh->getTimezone('user'));
        $this->set('rResponseCodeOptions', $this->getResponseCodeOptions());
        $this->set('rRedirectToTypeOptions', $this->getRedirectToTypeOptions());
        $rdr = new RedirectRule();
        $rdr->set('rActive', 1);
        $rdr->set('rIsWildCard', 0);
        $rdr->set('rIsRegEx', 0);
        $rdr->set('rFrom', $fromURL);
        $rdr->set('rToCID', 0);
        $rdr->set('rToFID', 0);
        $rdr->set('rToURL', '');
        $rdr->set('rResponseCode', 0);
        $rdr->set('rSort', 0);
        $rdr->set('rCount', 0);
        $rdr->set('rLastUsed', $now);
        $this->set('rule', $rdr);
        return;
    }

    public function edit()
    {
        $rID = urldecode($_SERVER["QUERY_STRING"]);
        $this->set('rResponseCodeOptions', $this->getResponseCodeOptions());
        $this->set('rRedirectToTypeOptions', $this->getRedirectToTypeOptions());

        if ($rID !== null) {
            $rdr = RedirectRule::getByID($rID);
            if (is_object($rdr)) {
                $this->set('rule', $rdr);
                return;
            }
        }

        return;
    }

    public function submit()
    {
        $response = new EditResponse();
        if (!$this->validateAction()) {
            $err = Core::make('error');
            $err->add(t("Invalidate Action"));
            $response->setError($err);
            $response->outputJSON();
            return;
        }

        $em = ORM::entityManager();
        $dh = Core::make('helper/date');
        $now = new DateTime('now', $dh->getTimezone('user'));

        $validationList = array();
        $rep = $this->getRedirectRulesRepository();
        $rdr = null;
        $rID = $this->post('rID');

        if ($rID > 0) {
            $rdr = $rep->find($rID);
            if (!is_object($rdr)) {
                $this->redirect('/dashboard/system/seo/seo_redirects/redirect_rules');
            }
        } else {
            $rdr = new RedirectRuleEntity();
            $rdr->set('rActive', 1);
            $rdr->set('rIsWildCard', 0);
            $rdr->set('rIsRegEx', 0);
            $rdr->set('rFrom', '');
            $rdr->set('rToCID', 0);
            $rdr->set('rToFID', 0);
            $rdr->set('rToURL', '');
            $rdr->set('rResponseCode', 0);
            $rdr->set('rSort', 0);
            $rdr->set('rCount', 0);
            $rdr->set('rLastUsed', $now);
        }

        // IsRegEX
        $rIsRegEx = $this->post('rIsRegEx') ? true : false;
        $rdr->rIsRegEx = $rIsRegEx;

        // From
        $rFrom = trim($this->post('rFrom'));
        if (strlen($rFrom) > 0 && !$rIsRegEx) {
            //Make any non relative URL in to a relative URL
            $pUrl = parse_url($rFrom);
            if (!empty($pUrl['scheme']) || !empty($pUrl['host']) || !empty($pUrl['fragment'])) {
                $rFrom = $pUrl['path'];
                if (!empty($pUrl['query'])) {
                    $rFrom = $rFrom . '?' . $pUrl['query'];
                }
            }

            if (!empty(Config::get('afixia_seo_redirects.subfolder_install'))) {
                $replace = '"' . Config::get('afixia_seo_redirects.subfolder_install') . '"';
                $rFrom = preg_replace($replace, '', $rFrom, 1);
            }

            $rFrom = ltrim($rFrom, "/");
            $rdr->rFrom = "/" . urldecode($rFrom);
        }

        if (strlen($rFrom) === 0 && !$rIsRegEx) {
            $validationList[] .= t('From URL must not be blank');
        }

        // FromRegEx
        $rFromRegEx = trim($this->post('rFromRegEx'));
        if ($rIsRegEx) {
            $rFromRegEx = ltrim($rFromRegEx, "/");
            $rFromRegEx = rtrim($rFromRegEx, "/");
            $rdr->rFrom = "/" . $rFromRegEx . "/";
            if (!$this->validateRegex($rdr->rFrom)) {
                $validationList[] .=
                    t("From URL: Invalid regular expression. Try escaping internal / characters as \/.\n") .
                    t("The search pattern requires a single leading and trailing / character.\n") .
                    t("Example: /catalog\/item([0-9]+).html/");
            }
        }

        // IsWildCard
        $rIsWildCard = false;
        if (strpos($rFrom, '*') !== false) {
            $rIsWildCard = true;
        }
        $rdr->rIsWildCard = $rIsWildCard;

        // Page, File or URL
        $rToCID = 0;
        if ($this->post('rToCID')) {
            $rToCID = (int)$this->post('rToCID');
        }

        $rToFID = 0;
        if ($this->post('rToFID')) {
            $rToFID = (int)$this->post('rToFID');
        }

        $rToURL = '';
        if ($this->post('rToURL')) {
            $rToURL = trim($this->post('rToURL'));
        }

        if ($this->post('redirectToType') == 'cid') {
            $rToURL = '';
            $rToFID = 0;
            if ($rToCID == 0) {
                $validationList[] .= t('You must choose a page');
            }
        }
        if ($this->post('redirectToType') == 'fid') {
            $rToURL = '';
            $rToCID = 0;
            if ($rToFID == 0) {
                $validationList[] .= t('You must choose a file');
            }
        }
        if ($this->post('redirectToType') == 'url') {
            $rToCID = 0;
            $rToFID = 0;
            if (strlen($rToURL) === 0) {
                $validationList[] .= t('To URL must not be blank');
            }
        }

        $rdr->rToCID = $rToCID;
        $rdr->rToFID = $rToFID;
        $rdr->rToURL = $rToURL;

        // To RegEx
        $rtoRegEx = trim($this->post('rtoRegEx'));
        if ($rIsRegEx) {
            $rdr->rToCID = 0;
            $rdr->rToFID = 0;
            $rdr->rToURL = $rtoRegEx;
            if (strlen($rtoRegEx) === 0) {
                $validationList[] .= t('To URL must not be blank');
            }
        }

        // HTTP Response Code
        $rResponseCode = $this->post('rResponseCode');
        $rdr->rResponseCode = $rResponseCode;
        if ($rResponseCode == 0) {
            $validationList[] .= t('You must choose a HTTP Response Code');
        }
        if ($rResponseCode == 410) {
            $rdr->rToCID = 0;
            $rdr->rToFID = 0;
            $rdr->rToURL = t('Gone');
        }
        if ($rResponseCode == 451) {
            $rdr->rToCID = 0;
            $rdr->rToFID = 0;
            $rdr->rToURL = t('Unavailable');
        }

        // Save
        if (count($validationList) == 0) {
            $em->persist($rdr);
            $em->flush();

            // Normalize Sort
            $rules = $rep->createQueryBuilder('r')->orderBy('r.rSort', 'ASC')->getQuery()->getResult();
            $order = 1;
            foreach ($rules as $rule) {
                $rule->rSort = $order++;
                $em->persist($rule);
            }
            $em->flush();

            // List View
            $response->setMessage(t("Saved Successfully"));
            $response->outputJSON();
            return;
        } else {
            $err = Core::make('error');
            foreach ($validationList as $item) {
                $err->add($item);
            }
            $response->setError($err);
            $response->outputJSON();
            return;
        }
    }

    function validateRegex($regex)
    {
        $ret = false;
        $val = @preg_match($regex, '');
        if ($val === 0) $ret = true;
        if ($val === 1) $ret = false;
        if ($val === false) $ret = false;
        return ($ret);
    }

    protected function getRedirectRulesRepository()
    {
        return ORM::entityManager()->getRepository('Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectRuleEntity');
    }

    public function getResponseCodeOptions()
    {
        $options = array();
        $options['0'] = t('Please select');
        $options['301'] = t('301 - Moved Permanently');
        $options['302'] = t('302 - Found');
        $options['307'] = t('307 - Temporary Redirect');
        $options['410'] = t('410 - Gone (Content Deleted)');
        $options['451'] = t('451 - Unavailable For Legal Reasons');
        return $options;
    }

    public function getRedirectToTypeOptions()
    {
        $options = array();
        $options['cid'] = t('Page');
        $options['fid'] = t('File');
        $options['url'] = t('URL');
        return $options;
    }
}
