<?php

namespace Concrete\Package\AfixiaSeoRedirects\Controller\Ajax;

defined('C5_EXECUTE') or die("Access Denied.");

use Controller;
use Database;
use ORM;
use Concrete\Core\Http\Request;
use Concrete\Package\AfixiaSeoRedirects\Src\RedirectUrlsChanged;
use Concrete\Package\AfixiaSeoRedirects\Src\RedirectUrlsChangedList;

class UrlsChangedAjax extends Controller
{
    public function getlist(Request $request)
    {
        $page_size = $request->get("page_size");
        $page_sort = $request->get("page_sort");
        $page_direction = $request->get("page_direction");

        $per_page = (int)$page_size;
        $changedList = new RedirectUrlsChangedList();
        $changedList->setItemsPerPage($per_page);
        $changedList->sortBy($page_sort,$page_direction);
        $paginator = $changedList->getPagination();
        $pagination = $paginator->renderDefaultView();
        $pages = $paginator->getCurrentPageResults();

        $page_current = $paginator->getCurrentPage();
        $page_count = count($pages);
        $items_total = $changedList->getTotalResults();
        $items_first = ($page_current - 1) * $per_page + 1;
        $items_last = $items_first + $page_count - 1;

        if ($items_total == 0) $items_first = 0;
        $entrydisplay = t("Showing %s to %s of %s entries", $items_first, $items_last, $items_total);

        $data = array();
        foreach ($pages as $c) {
            $obj = new \stdClass;
            $obj->rID = $c->rID;
            if (strlen($c->rFrom) > 0) $obj->rFrom = $c->rFrom;
            else $obj->rFrom = "[INVALID]";
            $obj->rFromEncoded = urlencode($obj->rFrom);
            $obj->rReason = $c->rReason;
            $obj->rChanged = $c->rChanged->format('m/d/y h:i a');
            $data[] = $obj;
        }

        $results = array('urlschangedlist' => $data,
            'pagination' => $pagination,
            'entrydisplay' => $entrydisplay);

        echo json_encode($results);

        return;
    }

    public function bulkaction(Request $request)
    {
        $em = ORM::entityManager();
        $bulk_action = $request->get('bulkaction');
        $rIDs = explode(",", $request->get('rIDs'));

        if ($bulk_action == 'delete_all') {
            $db = Database::connection();
            $db->query('delete from afxRedirectUrlsChanged');
            return;
        }

        foreach ($rIDs as $rID) {
            $changed = RedirectUrlsChanged::getByID((int)$rID);
            if (is_object($changed)) {
                switch ($bulk_action) {
                    case 'delete_selected':
                        $em->remove($changed);
                        break;
                }
            }
        }
        $em->flush();

        return;
    }

    public function delete(Request $request)
    {
        $rID = $request->get("rID");
        $em = ORM::entityManager();
        $rep = RedirectUrlsChanged::getRepository();

        if ($rID !== null) {
            $changed = $rep->find($rID);
            if (is_object($changed)) {
                $em->remove($changed);
                $em->flush();
            }
        }

        return;
    }
}