<?php

namespace Concrete\Package\AfixiaSeoRedirects\Controller\Ajax;

defined('C5_EXECUTE') or die("Access Denied.");

use Core;
use Controller;
use Database;
use File;
use Page;
use ORM;
use Config;
use DateTime;
use Concrete\Core\Http\Request;
use Concrete\Package\AfixiaSeoRedirects\Src\RedirectRule;
use Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectRuleEntity;
use Concrete\Package\AfixiaSeoRedirects\Src\RedirectRulesList;

class RedirectRulesAjax extends Controller
{
    public function quickadd(Request $request)
    {
        $dh = Core::make('helper/date');
        $now = new DateTime('now', $dh->getTimezone('user'));

        $em = ORM::entityManager();
        $rep = RedirectRule::getRepository();
        $validationList = array();

        $rdr = new RedirectRuleEntity();
        $rdr->set('rActive', 1);
        $rdr->set('rIsWildCard', 0);
        $rdr->set('rIsRegEx', 0);
        $rdr->set('rFrom', '');
        $rdr->set('rToCID', 0);
        $rdr->set('rToFID', 0);
        $rdr->set('rToURL', '');
        $rdr->set('rResponseCode', 0);
        $rdr->set('rSort', 0);
        $rdr->set('rCount', 0);
        $rdr->set('rLastUsed', $now);

        // From
        $rFrom = trim($request->get('rFrom'));
        if (strlen($rFrom) === 0) {
            $validationList[] .= t('From URL must not be blank');
        }

        //Make any non relative URL in to a relative URL
        $pUrl = parse_url($rFrom);
        if(!empty($pUrl['scheme']) || !empty($pUrl['host']) || !empty($pUrl['fragment'])) {
            $rFrom = $pUrl['path'];
            if(!empty($pUrl['query'])) {
                $rFrom = $rFrom . '?' . $pUrl['query'];
            }
        }

        if(!empty(Config::get('afixia_seo_redirects.subfolder_install'))) {
            $replace = '"'. Config::get('afixia_seo_redirects.subfolder_install') . '"';
            $rFrom = preg_replace($replace, '', $rFrom, 1);
        }

        $rFrom = ltrim($rFrom, "/");
        $rdr->rFrom = "/" . $rFrom;

        // IsWildCard
        $rIsWildCard = false;
        if (strpos($rFrom, '*') !== false) {
            $rIsWildCard = true;
        }
        $rdr->rIsWildCard = $rIsWildCard;

        // Page, File or URL
        $rToCID = 0;
        if ($request->get('rToCID')) {
            $rToCID = (int)$this->post('rToCID');
        }

        $rToFID = 0;
        if ($request->get('rToFID')) {
            $rToFID = (int)$this->post('rToFID');
        }

        $rToURL = '';
        if ($request->get('rToURL')) {
            $rToURL = trim($this->post('rToURL'));
        }

        if ($request->get('redirectToType') == 'cid') {
            $rToURL = '';
            $rToFID = 0;
            if ($rToCID == 0) {
                $validationList[] .= t('You must choose a page');
            }
        }
        if ($request->get('redirectToType') == 'fid') {
            $rToURL = '';
            $rToCID = 0;
            if ($rToFID == 0) {
                $validationList[] .= t('You must choose a file');
            }
        }
        if ($request->get('redirectToType') == 'url') {
            $rToCID = 0;
            $rToFID = 0;
            if (strlen($rToURL) === 0) {
                $validationList[] .= t('URL must not be blank');
            }
        }

        $rdr->rToCID = $rToCID;
        $rdr->rToFID = $rToFID;
        $rdr->rToURL = $rToURL;

        // HTTP Response Code
        $rdr->rResponseCode = 301;

        // Save
        if (count($validationList) == 0) {
            $em->persist($rdr);
            $em->flush();
        }

        // Normalize Sort
        $rules = $rep->createQueryBuilder('r')->orderBy('r.rSort', 'ASC')->getQuery()->getResult();
        $order = 1;
        foreach ($rules as $rule) {
            $rule->rSort = $order++;
            $em->persist($rule);
        }
        $em->flush();

        echo json_encode(array('success' => count($validationList) ? 0 : 1,
            'validationList' => $validationList));

        return;
    }

    public function getlist(Request $request)
    {
        /* $nh = Core::make('helper/navigation'); */
        $page_size = $request->get("page_size");
        $page_sort = $request->get("page_sort");
        $page_direction = $request->get("page_direction");

        $per_page = (int)$page_size;
        $rulesList = new RedirectRulesList();
        $rulesList->setItemsPerPage($per_page);
        $rulesList->sortBy($page_sort, $page_direction);
        $paginator = $rulesList->getPagination();
        $pagination = $paginator->renderDefaultView();
        $pages = $paginator->getCurrentPageResults();

        $page_current = $paginator->getCurrentPage();
        $page_count = count($pages);
        $items_total = $rulesList->getTotalResults();
        $items_first = ($page_current - 1) * $per_page + 1;
        $items_last = $items_first + $page_count - 1;

        if ($items_total == 0) $items_first = 0;
        $entrydisplay = t("Showing %s to %s of %s entries", $items_first, $items_last, $items_total);

        $data = array();
        foreach ($pages as $c) {
            $obj = new \stdClass;

            $obj->rID = $c->rID;
            $obj->rActive = $c->rActive;
            $obj->rIsWildCard = $c->rIsWildCard;
            $obj->rIsRegEx = $c->rIsRegEx;

            if (strlen($c->rFrom) > 0) $obj->rFrom = $c->rFrom;
            else $obj->rFrom = "[INVALID]";

            if ($c->rToCID > 0) {
                $page = Page::getByID($c->rToCID);
                if (is_object($page) && !$page->isError()) {
                    $pageName = $page->getCollectionName();
                    /* $pageUrl = $nh->getCollectionURL($page); */
                    $obj->rToURL = $pageName;
                } else {
                    $obj->rToURL = "[INVALID]";
                }
            }

            if ($c->rToFID > 0) {
                $file = File::getByID($c->rToFID);
                if (is_object($file) && !$file->isError()) {
                    $obj->rToURL = File::getRelativePathFromID($c->rToFID);
                } else {
                    $obj->rToURL = "[INVALID]";
                }
            }

            if (strlen($c->rToURL) > 0) {
                $obj->rToURL = $c->rToURL;
            }

            $obj->rResponseCode = $c->rResponseCode;
            $obj->rSort = $c->rSort;
            $obj->rCount = $c->rCount;
            $obj->rLastUsed = $c->rLastUsed->format('m/d/y h:i a');

            $data[] = $obj;
        }

        // Special sorting for ToURL
        if($page_sort == "rToURL") {
            if($page_direction == "asc") {
                usort($data, function ($a, $b) {
                    return strcmp($a->rToURL, $b->rToURL);
                });
            } else {
                usort($data, function ($a, $b) {
                    return strcmp($b->rToURL, $a->rToURL);
                });
            }
        }

        $results = array('ruleslist' => $data,
            'pagination' => $pagination,
            'entrydisplay' => $entrydisplay);

        echo json_encode($results);

        return;
    }

    public function bulkaction(Request $request)
    {
        $em = ORM::entityManager();
        $bulk_action = $request->get('bulkaction');
        $rIDs = explode(",", $request->get('rIDs'));

        if ($bulk_action == 'delete_all') {
            $db = Database::connection();
            $db->query('delete from afxRedirectRules');
            return;
        }

        foreach ($rIDs as $rID) {
            $rdr = RedirectRule::getByID((int)$rID);
            if (is_object($rdr)) {
                switch ($bulk_action) {
                    case 'activate_selected':
                        $rdr->rActive = true;
                        $em->persist($rdr);
                        break;
                    case 'deactivate_selected':
                        $rdr->rActive = false;
                        $em->persist($rdr);
                        break;
                    case 'delete_selected':
                        $em->remove($rdr);
                        break;
                }
            }
        }
        $em->flush();

        return;
    }

    public function sort(Request $request)
    {
        // Get AJAX Params
        $sortOrderStart = $request->post('sortOrderStart');
        $sortOrderEnd = $request->post('sortOrderEnd');
        $startIds = explode(",", $sortOrderStart);
        $endIds = explode(",", $sortOrderEnd);

        // Get Min/Max Sort
        $min_sort = PHP_INT_MAX;
        $max_sort = PHP_INT_MIN;
        foreach ($startIds as $id) {
            $rdr = RedirectRule::getByID((int)$id);
            if (is_object($rdr)) {
                if ($rdr->rSort <= $min_sort) $min_sort = $rdr->rSort;
                if ($rdr->rSort >= $max_sort) $max_sort = $rdr->rSort;
            }
        }

        // Resort
        $order = $min_sort;
        $em = ORM::entityManager();
        foreach ($endIds as $id) {
            $rdr = RedirectRule::getByID((int)$id);
            if (is_object($rdr)) {
                $rdr->rSort = $order++;
                $em->persist($rdr);
                $em->flush();
            }
        }

        return;
    }

    public function toggle(Request $request)
    {
        $rID = $request->get("rID");
        $em = ORM::entityManager();

        if ($rID !== null) {
            $rdr = RedirectRule::getByID((int)$rID);
            if (is_object($rdr)) {
                $rdr->rActive = $rdr->rActive ? false : true;
                $em->persist($rdr);
                $em->flush();
            }
        }

        return;
    }

    public function up(Request $request)
    {
        $rID = $request->get("rID");
        if ($rID !== null) {
            $em = ORM::entityManager();
            $rep = RedirectRule::getRepository();
            $rules = $rep->createQueryBuilder('r')->orderBy('r.rSort', 'ASC')->getQuery()->getResult();

            // Normalize Sort
            $order = 1;
            foreach ($rules as $rule) {
                $rule->rSort = $order++;
            }

            // Find Current Position
            $currentPosition = 1;
            $maxPosition = count($rules);

            foreach ($rules as $rule) {
                if ($rule->rID == $rID) {
                    $currentPosition = $rule->rSort;
                }
            }

            // Re-Position
            $newPosition = $currentPosition - 1; // up

            if ($newPosition > 0 && $newPosition <= $maxPosition) {
                foreach ($rules as $rule) {
                    if ($rule->rID == $rID) {
                        $rule->rSort = $newPosition;
                    } elseif ($rule->rSort == $newPosition) {
                        $rule->rSort = $currentPosition;
                    }
                    $em->persist($rule);
                }
            }

            $em->flush();
        }

        return;
    }

    public function down(Request $request)
    {
        $rID = $request->get("rID");
        if ($rID !== null) {
            $em = ORM::entityManager();
            $rep = RedirectRule::getRepository();
            $rules = $rep->createQueryBuilder('r')->orderBy('r.rSort', 'ASC')->getQuery()->getResult();

            // Normalize Sort
            $order = 1;
            foreach ($rules as $rule) {
                $rule->rSort = $order++;
            }

            // Find Current Position
            $currentPosition = 1;
            $maxPosition = count($rules);

            foreach ($rules as $rule) {
                if ($rule->rID == $rID) {
                    $currentPosition = $rule->rSort;
                }
            }

            // Re-Position
            $newPosition = $currentPosition + 1; // down

            if ($newPosition > 0 && $newPosition <= $maxPosition) {
                foreach ($rules as $rule) {
                    if ($rule->rID == $rID) {
                        $rule->rSort = $newPosition;
                    } elseif ($rule->rSort == $newPosition) {
                        $rule->rSort = $currentPosition;
                    }
                    $em->persist($rule);
                }
            }

            $em->flush();
        }

        return;
    }

    public function delete(Request $request)
    {
        $rID = $request->get("rID");
        $em = ORM::entityManager();
        $rep = RedirectRule::getRepository();

        if ($rID !== null) {
            $seoRedirect = $rep->find($rID);
            if (is_object($seoRedirect)) {
                $em->remove($seoRedirect);
                $em->flush();
            }

            // Normalize Sort
            $rules = $rep->createQueryBuilder('r')->orderBy('r.rSort', 'ASC')->getQuery()->getResult();
            $order = 1;
            foreach ($rules as $rule) {
                $rule->rSort = $order++;
                $em->persist($rule);
            }
            $em->flush();
        }

        return;
    }
}