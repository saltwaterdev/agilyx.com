<?php defined('C5_EXECUTE') or die("Access Denied.");

///////////////////////////////////////////////////////////////////////////////
/// Rule Editor

if (is_object($rule)) :

    $form = Core::make('helper/form');

    ?>

    <div class="ccm-ui">
        <form data-dialog-form="rule-editor" method="post" action="<?php echo $controller->action('submit') ?>">
            <?php echo $form->hidden('rID', $rule->rID); ?>
            <div id="rule-editor" class="ccm-dashboard-content-full">
                <div class="row" id="afx-editor-help" style="display: none;">
                    <div class="col-sm-12">
                        <div class="redirect-to-type-block alert alert-info">
                            <strong><?php echo t("Wildcards:") ?></strong><br/>
                            <?php echo t("Use a * as a wildcard indicator if you wish to redirect all URLs within a directory to the same URL.") ?>
                            <br /><br />
                            <strong><?php echo t("Example:") ?> </strong><br/>
                            <?php echo t("From URL = /old-dir/*") ?><br />
                            <?php echo t("To URL = /new-dir/page") ?><br /><br />
                            <small><?php echo t("Any url that comes after") ?>: <strong><?php echo t("/old-dir/") ?></strong><br/>
                                <?php echo t("Example") ?>: <strong><?php echo t("/old-dir/old-page") ?></strong><br />
                                <?php echo t("Would redirect to") ?>: <strong><?php echo t("/new-dir/page") ?></strong></small>
                        </div>
                        <div class="redirect-to-type-block alert alert-warning">
                            <strong><?php echo t("Warning") ?>:</strong><br />
                            <?php echo t("Too many wildcards could affect site performance."); ?>
                        </div>
                        <div class="redirect-toregex alert alert-info" style="display:none;">
                            <strong><?php echo t("Functionality based on") ?>: </strong><br />
                            <a href="http://php.net/manual/en/function.preg-replace.php" target="_blank">
                                <small><strong><?php echo t("preg_replace(From_URL, To_URL, Incoming_URL);"); ?></strong></small>
                            </a><br /><br />
                            <strong><?php echo t("Example:") ?> </strong><br/>
                            <?php echo t("From URL = /oldblog\\/post([0-9]+)/") ?><br />
                            <?php echo t("To URL = /newblog/post\\1") ?><br /><br />
                            <small><?php echo t("This will change URLs") ?><br />
                                <?php echo t("From") ?>: <strong><?php echo t("/oldblog/post1") ?></strong> <?php echo t("and") ?> <strong><?php echo t("/oldblog/post2") ?></strong><br />
                                <?php echo t("To") ?>: <strong><?php echo t("/newblog/post1") ?></strong> <?php echo t("and") ?> <strong><?php echo t("/newblog/post2") ?></strong></small>
                        </div>
                        <div class="redirect-toregex alert alert-warning" style="display:none;">
                            <strong><?php echo t("Warning") ?>: </strong><br />
                            <?php echo t("Regex redirects are intended for users experienced with regular expression search and replace. 
                        If you are inexperienced with regular expressions, please educate yourself before using this feature."); ?>
                            <a href="http://php.net/manual/en/function.preg-replace.php" target="_blank"><?php echo t('Learn More'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="btn-group radioBtn">
                                    <a class="btn btn-success btn-sm active" data-toggle="rIsRegEx"
                                       data-title="0"><?php echo t('Redirect'); ?></a>
                                    <a class="btn btn-success btn-sm notActive" data-toggle="rIsRegEx"
                                       data-title="1"><?php echo t('Regex Redirect'); ?></a>
                                    <?php echo $form->hidden('rIsRegEx', $rule->rIsRegEx ? "1" : "0") ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="pull-right">
                            <a id="btn-show-help" class="btn btn-primary btn-sm"><i class="fa fa-question-circle"></i>
                                <?php echo t('Help'); ?></a>
                            <a id="btn-hide-help" class="btn btn-primary btn-sm" style="display: none;"><i class="fa fa-question-circle"></i>
                                <?php echo t('Close Help'); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group redirect-oldurl" style="display: none;">
                            <?php echo $form->label('rFrom', t('From URL')) ?>
                            <div class="input-group">
                                <?php if (!empty($subfolder_install)) { ?>
                                    <div class="input-group-addon"><?php echo '/' . $subfolder_install . '/' ?></div>
                                <?php } else { ?>
                                    <div class="input-group-addon"><?php echo '/' ?></div>
                                <?php } ?>
                                <?php echo $form->text('rFrom', $rule->rFrom) ?>
                            </div>
                            <small><?php echo t('Only a relative URL is allowed. Asterisk * evaluates as a wildcard.'); ?></small>
                        </div>
                        <div class="form-group redirect-regex" style="display: none;">
                            <?php echo $form->label('rFromRegEx', t('From URL')) ?>
                            <div class="input-group">
                                <?php if (!empty($subfolder_install)) { ?>
                                    <div class="input-group-addon"><?php echo '/' . $subfolder_install . '/' ?></div>
                                <?php } else { ?>
                                    <div class="input-group-addon"><?php echo '/' ?></div>
                                <?php } ?>
                                <?php echo $form->text('rFromRegEx', $rule->rFrom) ?>
                            </div>
                            <small><?php echo t('Regex search pattern.'); ?> <a
                                        href='http://www.phpliveregex.com/p/gT0'
                                        target="_blank"><?php echo t('Test Tool'); ?></a></small>
                        </div>
                        <div class="form-group">
                            <?php echo $form->label('rResponseCode', t('HTTP Response Code')) ?>
                            <?php echo $form->select('rResponseCode', $rResponseCodeOptions, $rule->rResponseCode); ?>
                            <small><a href='https://en.wikipedia.org/wiki/List_of_HTTP_status_codes'
                                      target="_blank"><?php echo t('Learn more about HTTP response codes.'); ?></a>
                            </small>
                        </div>
                        <div class="form-group redirect-toregex-destination" style="display: none;">
                            <?php echo $form->label('rToRegEx', t('To URL')); ?>
                            <?php echo $form->text('rtoRegEx', $rule->rToURL); ?>
                            <small><?php echo t('Regex replacement pattern.'); ?></small>
                        </div>
                        <div class="redirect-to-type-block redirect-destination" style="display: none;">
                            <div class="form-group redirect-type">
                                <?php echo $form->label('redirectToType', t('Destination')) ?>
                                <?php

                                $rCID = $rule->rToCID;
                                $rFID = $rule->rToFID;
                                $rURL = $rule->rToURL;

                                $options = array();
                                if ($rCID == 0 && $rURL == '') {
                                    $options[''] = t('Please select');
                                    $selected = '';
                                }
                                $options['cid'] = t('Page');
                                if ($rCID != 0) {
                                    $selected = 'cid';
                                }
                                $options['fid'] = t('File');
                                if ($rFID != 0) {
                                    $selected = 'fid';
                                }
                                $options['url'] = t('URL');
                                if ($rCID == 0 && $rURL != '') {
                                    $selected = 'url';
                                }
                                echo $form->select('redirectToType', $options, $selected);

                                $file = false;
                                if ($rFID) {
                                    $file = File::getByID($rFID);
                                }
                                ?>
                            </div>
                            <div class="form-group redirect-to-type redirect-to-type-cid"<?php echo $selected == 'cid' ? '' : ' style="display: none;"'; ?>>
                                <?php echo $form->label('rToCID', t('Page')); ?>
                                <?php echo Core::make('helper/form/page_selector')->selectPage('rToCID', $rCID); ?>
                            </div>
                            <div class="form-group redirect-to-type redirect-to-type-fid"<?php echo $selected == 'fid' ? '' : ' style="display: none;"'; ?>>
                                <?php echo $form->label('rToFID', t('File')); ?>
                                <?php echo Core::make('helper/concrete/file_manager')->file('fid', 'rToFID', 'Choose File', $file); ?>
                            </div>
                            <div class="form-group redirect-to-type redirect-to-type-url"<?php echo $selected == 'url' ? '' : ' style="display: none;"'; ?>>
                                <?php echo $form->label('rToURL', t('To URL')); ?>
                                <?php echo $form->text('rToURL', $rURL, array('placeholder' => 'http://www.extdomain.com/customer/56')); ?>
                                <small><?php echo t('This can be an absolute or relative URL.'); ?></small>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                AfxRedirectRuleEditor();
                            });
                        </script>
                    </div>
                </div>
            </div>
            <div class="dialog-buttons">
                <button class="btn btn-default pull-left" data-dialog-action="cancel"><?php echo t('Cancel') ?></button>
                <button type="button" data-dialog-action="submit"
                        class="btn btn-primary pull-right"><?php echo t('Save') ?></button>
            </div>
        </form>
    </div>

<?php else : ?>

    <div class="ccm-ui">
        <form data-dialog-form="rule-editor" method="post" action="<?php echo $controller->action('submit') ?>">
            <div class="dialog-buttons">
                <button class="btn btn-default pull-left" data-dialog-action="cancel"><?php echo t('Cancel') ?></button>
            </div>
        </form>
    </div>

<?php endif; ?>

