<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<div class="alert alert-info">
    <div class="checkbox">
        <label>
            <input type="checkbox" name="remove_data" value="1">
            <?php echo t('Remove All Data/Redirects?') ?>
        </label>
    </div>
    <div class="checkbox disabled">
        <label>
            <input type="checkbox" name="keep_settings" value="1">
            <?php echo t('Keep Settings as they are in case you reinstall?') ?>
        </label>
    </div>
</div>
