<?php defined('C5_EXECUTE') or die("Access Denied.");

echo '<div class="alert alert-info">';
echo '<p>' .t('Find the SEO Redirects and other SEO Tools under %s > %s > %s after installation.', t('Dashboard'), t('SEO Tools'), t('SEO Redirects')) . '</p>';
echo '<p>' .t('SEO Redirects can also be found under %s > %s > %s > %s.', t('Dashboard'), t('System & Settings'), t('SEO & Statistics'), t('SEO Redirects')) . '</p>';
echo '</div>';

echo '<div class="alert alert-danger">';
echo '<p>';
echo t('For this add-on to work completely, please enable Pretty URLs (remove index.php) under %s > %s > %s.', t('Dashboard'), t('System & Settings'), t("URLs & Redirection"));
echo '</p>';
echo '</div>';

echo '<p><small>' . t('Learn more about moving website content here:') . ' <a href="https://webmasters.googleblog.com/2012/04/how-to-move-your-content-to-new.html" target="_blank">https://webmasters.googleblog.com/2012/04/how-to-move-your-content-to-new.html</a></small></p>';
echo '<p><small>' . t('Read more about this plugin and documentation here:') . ' <a href="https://www.concrete5.org/marketplace/addons/afixia-seo-redirects/" target="_blank">https://www.concrete5.org/marketplace/addons/afixia-seo-redirects/</a></small></p>';
