<?php

namespace Concrete\Package\AfixiaSeoRedirects\Src;

defined('C5_EXECUTE') or die("Access Denied.");

use Core;
use ORM;
use Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectUrlsChangedEntity;

class RedirectUrlsChanged extends RedirectUrlsChangedEntity
{
    public static function getRepository()
    {
        return $em = ORM::entityManager()
            ->getRepository('Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectUrlsChangedEntity');
    }

    public static function getByID($rID)
    {
        $rep = RedirectUrlsChanged::getRepository();
        $ret = false;

        if ($rID !== null) {
            $ret = $rep->find($rID);
        }

        return ($ret);
    }

    public static function insert($rFrom, $rReason) {
        $dh = Core::make('helper/date');
        $now = new \DateTime('now', $dh->getTimezone('user'));
        $em = ORM::entityManager();
        $entity = new RedirectUrlsChangedEntity();
        $entity->rFrom = $rFrom;
        $entity->rReason = $rReason;
        $entity->rChanged = $now;
        $em->persist($entity);
        $em->flush();
    }
}