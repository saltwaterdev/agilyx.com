<?php

namespace Concrete\Package\AfixiaSeoRedirects\Src;

defined('C5_EXECUTE') or die("Access Denied.");

use Concrete\Core\Search\Pagination\Pagination;
use Concrete\Core\Search\ItemList\Database\ItemList as DatabaseItemList;
use Pagerfanta\Adapter\DoctrineDbalAdapter;

class RedirectNotFoundList extends DatabaseItemList
{
    public function createQuery()
    {
        $this->query
            ->select('t.rID')
            ->orderBy('t.rCount', 'DESC')
            ->from('afxRedirectNotFound', 't');
    }

    public function getResult($queryRow)
    {
        return RedirectNotFound::getByID($queryRow['rID']);
    }

    protected function createPaginationObject()
    {
        $adapter = new DoctrineDbalAdapter($this->deliverQueryObject(),
            function ($query) {
                $query->select('count(distinct t.rID)')
                    ->setMaxResults(1);
            });

        $pagination = new Pagination($this, $adapter);
        return $pagination;
    }

    public function getTotalResults()
    {
        $query = $this->deliverQueryObject();
        return $query->select('count(distinct t.rID)')
            ->setMaxResults(1)
            ->execute()
            ->fetchColumn();
    }

    public function getGroupList()
    {
        $rep = RedirectNotFound::getRepository();
        $groupList = $rep->createQueryBuilder('r')
            ->orderBy('r.rCount', 'DESC')
            ->getQuery()
            ->getResult();

        return $groupList;
    }
}
