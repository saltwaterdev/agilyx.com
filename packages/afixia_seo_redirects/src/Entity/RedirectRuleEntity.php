<?php

namespace Concrete\Package\AfixiaSeoRedirects\Src\Entity;

defined('C5_EXECUTE') or die("Access Denied.");

use Concrete\Package\AfixiaSeoRedirects\Src\Entity;

/**
 * @Entity
 * @Table(name="afxRedirectRules")
 */
class RedirectRuleEntity extends Entity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $rID;

    /**
     * @Column(type="boolean", options={"default":0})
     */
    protected $rActive;

    /**
     * @Column(type="boolean", options={"default":0})
     */
    protected $rIsWildCard;

    /**
     * @Column(type="boolean", options={"default":0})
     */
    protected $rIsRegEx;

    /**
     * @Column(type="string")
     */
    protected $rFrom;

    /**
     * @Column(type="integer")
     */
    protected $rToCID;

    /**
     * @Column(type="integer")
     */
    protected $rToFID;

    /**
     * @Column(type="string")
     */
    protected $rToURL;

    /**
     * @Column(type="integer")
     */
    protected $rResponseCode;

    /**
     * @Column(type="integer")
     */
    protected $rSort;

    /**
     * @Column(type="integer")
     */
    protected $rCount;

    /**
     * @Column(type="datetime")
     */
    protected $rLastUsed;
}


