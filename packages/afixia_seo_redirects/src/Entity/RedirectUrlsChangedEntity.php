<?php

namespace Concrete\Package\AfixiaSeoRedirects\Src\Entity;

defined('C5_EXECUTE') or die("Access Denied.");

use Concrete\Package\AfixiaSeoRedirects\Src\Entity;

/**
 * @Entity
 * @Table(name="afxRedirectUrlsChanged")
 */
class RedirectUrlsChangedEntity extends Entity
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $rID;

    /**
     * @Column(type="string", nullable=true)
     */
    protected $rFrom;

    /**
     * @Column(type="string")
     */
    protected $rReason;

    /**
     * @Column(type="datetime")
     */
    protected $rChanged;
}
