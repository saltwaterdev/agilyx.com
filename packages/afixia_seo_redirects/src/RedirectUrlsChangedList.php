<?php

namespace Concrete\Package\AfixiaSeoRedirects\Src;

defined('C5_EXECUTE') or die("Access Denied.");

use Concrete\Core\Search\Pagination\Pagination;
use Concrete\Core\Search\ItemList\Database\ItemList as DatabaseItemList;
use Pagerfanta\Adapter\DoctrineDbalAdapter;

class RedirectUrlsChangedList extends DatabaseItemList
{
    public function createQuery()
    {
        $this->query
            ->select('t.rID')
            ->orderBy('t.rChanged', 'DESC')
            ->from('afxRedirectUrlsChanged', 't');
    }

    public function getResult($queryRow)
    {
        return RedirectUrlsChanged::getByID($queryRow['rID']);
    }

    protected function createPaginationObject()
    {
        $adapter = new DoctrineDbalAdapter($this->deliverQueryObject(),
            function ($query) {
                $query->select('count(distinct t.rID)')
                    ->setMaxResults(1);
            });

        $pagination = new Pagination($this, $adapter);
        return $pagination;
    }

    public function getTotalResults()
    {
        $query = $this->deliverQueryObject();
        return $query->select('count(distinct t.rID)')
            ->setMaxResults(1)
            ->execute()
            ->fetchColumn();
    }

    public function getGroupList()
    {
        $rep = RedirectNotFound::getRepository();
        $groupList = $rep->createQueryBuilder('r')
            ->orderBy('r.rChanged', 'DESC')
            ->getQuery()
            ->getResult();

        return $groupList;
    }
}
