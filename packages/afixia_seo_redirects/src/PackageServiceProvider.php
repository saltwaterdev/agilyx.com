<?php

namespace Concrete\Package\AfixiaSeoRedirects\Src;

defined('C5_EXECUTE') or die("Access Denied.");

use \Concrete\Core\Foundation\Service\Provider as ServiceProvider;

class PackageServiceProvider extends ServiceProvider
{
    public function register()
    {
        $pkgHandle = 'afixia_seo_redirects';
        $singletons = array(
            'helper/seo_redirector' => '\Concrete\Package\AfixiaSeoRedirects\Src\Service\SeoRedirector',
        );
        foreach ($singletons as $key => $value) {
            $this->app->singleton($pkgHandle . '/' . $key, $value);
        }
    }
}
