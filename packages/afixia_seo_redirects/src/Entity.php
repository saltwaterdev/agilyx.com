<?php

namespace Concrete\Package\AfixiaSeoRedirects\Src;

defined('C5_EXECUTE') or die("Access Denied.");

abstract class Entity
{
    protected $protect = array();
    protected $protectRead = array();
    protected $protectWrite = array();

    public function get($name)
    {
        if (property_exists($this, $name) && !in_array($name, $this->protect) && !in_array($name, $this->protectRead)) {
            return $this->$name;
        }
    }

    public function set($name, $value)
    {
        if (property_exists($this, $name) && !in_array($name, $this->protect) && !in_array($name, $this->protectWrite)) {
            $this->$name = $value;
        }
    }

    public function __get($name)
    {
        return $this->get($name);
    }

    public function __set($name, $value)
    {
        $this->set($name, $value);
    }
}
