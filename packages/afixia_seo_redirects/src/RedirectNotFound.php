<?php

namespace Concrete\Package\AfixiaSeoRedirects\Src;

defined('C5_EXECUTE') or die("Access Denied.");

use Config;
use Core;
use ORM;
use Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectNotFoundEntity;

class RedirectNotFound extends RedirectNotFoundEntity
{
    public static function getRepository()
    {
        return $em = ORM::entityManager()
            ->getRepository('Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectNotFoundEntity');
    }

    public static function getByID($rID)
    {
        $rep = RedirectNotFound::getRepository();
        $notfound = false;

        if ($rID !== null) {
            $notfound = $rep->find($rID);
        }

        return ($notfound);
    }

    public static function limitCleanup($max_urls)
    {
        $em = ORM::entityManager();

        $subQueryBuilder = $em->createQueryBuilder();
        $subQuery = $subQueryBuilder
            ->select(['rk.rID'])
            ->from('Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectNotFoundEntity', 'rk')
            ->orderBy('rk.rLastUsed', 'DESC')
            ->getQuery()
            ->setMaxResults($max_urls)
            ->getArrayResult()
        ;

        $queryBuilder = $em->createQueryBuilder();
        $query = $queryBuilder
            ->select(['r'])
            ->from('Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectNotFoundEntity', 'r')
            ->where($queryBuilder->expr()->notIn('r.rID', ':subQuery'))
            ->setParameter('subQuery', $subQuery)
            ->getQuery()
        ;

        $olds = $query->getResult();
        foreach ($olds as $old) {
            $em->remove($old);
        }
        $em->flush();
    }

    public static function insertOrCount($url)
    {
        $ignore_list = RedirectNotFound::convert_to_array(
            urldecode(Config::get('afixia_seo_redirects.ignore_list')));

        if(in_array($url,$ignore_list)) {
            return;
        }

        $dh = Core::make('helper/date');
        $now = new \DateTime('now', $dh->getTimezone('user'));
        $em = ORM::entityManager();
        $notfound = RedirectNotFound::getByUrl($url);
        if (isset($notfound)) {
            $notfound->rCount++;
			$notfound->rLastUsed = $now;
            $em->persist($notfound);
            $em->flush();
        } else {
            $notfound = new RedirectNotFoundEntity();
            $notfound->set('rFrom', $url);
            $notfound->set('rReferer', $_SERVER['HTTP_REFERER']);
            $notfound->set('rCount', 1);
			$notfound->set('rLastUsed', $now);
            $em->persist($notfound);
            $em->flush();
        }
    }

    public static function convert_to_array($json){
        $ignore_list_array = json_decode($json, true);
        if($ignore_list_array === null) {
            $ignore_list_array = json_decode('{"ignore_list":[]}', true);
        }
        $ret_array = array();
        foreach ($ignore_list_array["ignore_list"] as $item) {
            $url = $item["url"];
            $url = strtolower(trim($url));
            array_push($ret_array, $url);
        }
        return($ret_array);
    }

    public static function flipTrailingSlash($url)
    {
        $new_url = rtrim($url, "/");
        if ($new_url !== $url) {
            return $new_url;
        }
        return $new_url . '/';
    }

    public static function getByUrl($url)
    {
        $rep = RedirectNotFound::getRepository();
        $notfound = $rep->createQueryBuilder('r')
            ->where('r.rFrom = :url OR r.rFrom = :flipurl')
            ->setParameter(':url', $url)
            ->setParameter(':flipurl', RedirectRule::flipTrailingSlash($url))
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
        return ($notfound);
    }
}