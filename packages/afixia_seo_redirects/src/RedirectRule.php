<?php

namespace Concrete\Package\AfixiaSeoRedirects\Src;

defined('C5_EXECUTE') or die("Access Denied.");

use Core;
use ORM;
use Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectRuleEntity;

class RedirectRule extends RedirectRuleEntity
{
    function __construct() {
        $dh = Core::make('helper/date');
        $now = new \DateTime('now', $dh->getTimezone('user'));
        $this->set('rID', 0);
        $this->set('rActive', 1);
        $this->set('rIsWildCard', 0);
        $this->set('rIsRegEx', 0);
        $this->set('rFrom', '');
        $this->set('rToCID', 0);
        $this->set('rToFID', 0);
        $this->set('rToURL', '');
        $this->set('rResponseCode', 0);
        $this->set('rSort', 0);
        $this->set('rCount', 0);
		$this->set('rLastUsed', $now);
    }

    public static function getRepository()
    {
        return $em = ORM::entityManager()
            ->getRepository('Concrete\Package\AfixiaSeoRedirects\Src\Entity\RedirectRuleEntity');
    }

    public static function getByID($rID)
    {
        $rep = RedirectRule::getRepository();
        $redirectRule = false;

        if ($rID !== null) {
            $redirectRule = $rep->find($rID);
        }

        return ($redirectRule);
    }

    public static function flipTrailingSlash($url)
    {
        $new_url = rtrim($url, "/");
        if ($new_url !== $url) {
            return $new_url;
        }
        return $new_url . '/';
    }

    public static function getByUrl($url)
    {
        $rep = RedirectRule::getRepository();
        $rule = $rep->createQueryBuilder('r')
            ->where('(r.rFrom = :url OR r.rFrom = :flipurl) AND r.rActive = 1')
            ->setParameter(':url', $url)
            ->setParameter(':flipurl', RedirectRule::flipTrailingSlash($url))
            ->orderBy('r.rSort', 'ASC')
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();
        return ($rule);
    }

    public static function getWildcards()
    {
        $rep = RedirectRule::getRepository();
        $rules = $rep->createQueryBuilder('r')
            ->where('r.rIsWildCard = 1 AND r.rActive = 1')
            ->orderBy('r.rSort', 'ASC')
            ->getQuery()
            ->getResult();
        return ($rules);
    }

    public static function getRegExs()
    {
        $rep = RedirectRule::getRepository();
        $rules = $rep->createQueryBuilder('r')
            ->where('r.rIsRegEx = 1 AND r.rActive = 1')
            ->orderBy('r.rSort', 'ASC')
            ->getQuery()
            ->getResult();
        return ($rules);
    }

    public static function incrementCount($rule)
    {
        $dh = Core::make('helper/date');
        $now = new \DateTime('now', $dh->getTimezone('user'));
        $em = ORM::entityManager();
        $rule->rCount++;
		$rule->rLastUsed = $now;
        $em->persist($rule);
        $em->flush();
    }
}