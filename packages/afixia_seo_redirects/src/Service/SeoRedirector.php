<?php

namespace Concrete\Package\AfixiaSeoRedirects\Src\Service;

defined('C5_EXECUTE') or die("Access Denied.");

use Core;
use File;
use Page;
use User;
use Config;
use Concrete\Package\AfixiaSeoRedirects\Src\RedirectRule;
use Concrete\Package\AfixiaSeoRedirects\Src\RedirectNotFound;
use Concrete\Package\AfixiaSeoRedirects\Src\RedirectUrlsChanged;

class SeoRedirector
{
    public function on_request($ctrl, $event)
    {
        // Validate Args
        if (!isset($ctrl)) return;
        if (!isset($event)) return;
        if (get_class($ctrl) !== 'Concrete\Package\AfixiaSeoRedirects\Controller') return;

        // Get Event Data
        $view = $event->getArgument('view');
        if (!isset($view)) return;

        // Pages Only
        if (get_class($view) !== 'Concrete\Core\Page\View\PageView') return;
        $page = $view->getPageObject();
        if (!isset($page)) return;

        // Page Type
        $is_page_not_found = $this->is_page_not_found($page);
        //$is_page_forbidden = $this->is_page_forbidden($page); //For Future Development

        // Get Request Data
        $security = Core::make('helper/security');
        //$protocol = ($this->is_ssl() ? 'https://' : 'http://'); //For Future Development
        //$server_name = $security->sanitizeString($_SERVER['SERVER_NAME']); //For Future Development
        $request_url = $security->sanitizeString($_SERVER['REQUEST_URI']);
        $request_url = urldecode($request_url);
        $request_url_lowered = strtolower($request_url);

        // Skip AJAX and Dashboard Requests
        if ($this->is_ajax()) return;
        if ($this->is_dashboard($request_url_lowered)) return;

        // Skip Default Page
        $request_url = ltrim($request_url, "/");
        if (strlen($request_url) === 0) return;

        // Is a subfolder defined?  If so we need to remove the subfolder to search our database of relative urls
        $subfolder_install = Config::get('afixia_seo_redirects.subfolder_install');
        if(!empty($subfolder_install)) {
            $replace = '"'. Config::get('afixia_seo_redirects.subfolder_install') . '"';
            $request_url = preg_replace($replace, '', $request_url, 1);
            $request_url = ltrim($request_url,'/');
        }

        $request_url = "/" . $request_url;

        // Settings
        $allow_redirect = Config::get('afixia_seo_redirects.process_redirects');

        // Redirect only on 404 Page Not Found?
        if (!$is_page_not_found) {
            if (Config::get('afixia_seo_redirects.only_404s')) $allow_redirect = false;
        }

        // Allow Super Admin Redirects?
        $user = new User();
        $is_super_user = $user->isSuperUser();

        if ($is_super_user) {
            if (!Config::get('afixia_seo_redirects.redirect_super_admin')) $allow_redirect = false;
        }

        // Allow Redirects?
        if ($allow_redirect) {

            // Redirects
            $rule = RedirectRule::getByUrl($request_url);
            if ($rule !== null) {
                $this->redirectOnRule($rule, $request_url);
                return;
            }

            // Redirects Containing Wildcard*
            $rules = RedirectRule::getWildcards();
            foreach ($rules as $rule) {
                if ($this->checkWildcard($request_url, $rule->rFrom)) {
                    $this->redirectOnRule($rule, $request_url);
                    return;
                }
            }

            // RegEx Redirects
            $rules = RedirectRule::getRegExs();
            foreach ($rules as $rule) {
                if ($this->checkRegEx($request_url, $rule->rFrom)) {
                    $this->redirectOnRule($rule, $request_url);
                    return;
                }
            }
        }

        // 404 Page Not Found
        if ($is_page_not_found) {
            if (Config::get('afixia_seo_redirects.log_urls_not_found')) {
                RedirectNotFound::insertOrCount($request_url);
                RedirectNotFound::limitCleanup(Config::get('afixia_seo_redirects.not_found_limit'));
            }
        }

        return;
    }

    public function redirectOnRule($rule, $request_url)
    {
        RedirectRule::incrementCount($rule);
        $location_url = '';

        // Page
        if ($rule->rToCID > 0) {
            $page = Page::getByID($rule->rToCID);
            if (is_object($page) && !$page->isError()) {
                $location_url = $page->getCollectionLink();
            } else {
                // Error
            }
        }

        // File
        if ($rule->rToFID > 0) {
            $file = File::getByID($rule->rToFID);
            if (is_object($file)) {
                $location_url = File::getRelativePathFromID($rule->rToFID);
            } else {
                // Error
            }
        }

        // URL
        if (strlen($rule->rToURL) > 0) {

            //Check if relative and subfolder
            if(!$rule->rIsRegEx) {
                if(!empty(Config::get('afixia_seo_redirects.subfolder_install'))) {
                    $pUrl = parse_url($rule->rToURL);
                    //Check if this is a relative URL
                    if(empty($pUrl['scheme']) && empty($pUrl['host'])) {
                        //Check to see if they already included the subfolder
                        $pos = strpos($rule->rToURL, Config::get('afixia_seo_redirects.subfolder_install'));
                        if ($pos === false) {
                            $rule->rToURL = '/' . Config::get('afixia_seo_redirects.subfolder_install') . '/' . ltrim($rule->rToURL, '/');
                        }
                    }
                }
            }

            $location_url = $rule->rToURL;
        }

        // RegEx
        if ($rule->rIsRegEx) {
            $hasLeadingSlash = false;
            if (0 === strpos($rule->rToURL, '/')) $hasLeadingSlash = true;
            $location_url = @preg_replace($rule->rFrom, $rule->rToURL, $request_url);
            $location_url = ltrim($location_url, '/');
            if ($hasLeadingSlash) $location_url = '/' . $location_url;

            //if a subfolder is defined
            $subfolder_install = Config::get('afixia_seo_redirects.subfolder_install');
            if(!empty($subfolder_install)) {
                $location_url = '/' . $subfolder_install . $location_url;
            }
        }

        $response_code = '';
        switch ($rule->rResponseCode) {
            case 301:
                $response_code = 'HTTP/1.1 301 Moved Permanently';
                break;
            case 302:
                $response_code = 'HTTP/1.1 302 Found';
                break;
            case 307:
                $response_code = 'HTTP/1.1 307 Temporary Redirect';
                break;
            case 410:
                $response_code = 'HTTP/1.1 410 Gone';
                $location_url = '';
                break;
            case 451:
                $response_code = 'HTTP/1.1 451 Unavailable For Legal Reasons';
                $location_url = '';
                break;
        }

        if (strlen($response_code) > 0) {
            header($response_code);
            header("Cache-Control: no-cache");
            header('Expires: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');
            if (strlen($location_url) > 0) {
                header('Location: ' . $location_url);
            }
            exit;
        }

        return;
    }

    public function on_file_delete($ctrl, $event)
    {
        // Validate Args
        if (!isset($ctrl)) return;
        if (!isset($event)) return;

        $file = $event->getFileObject();
        RedirectUrlsChanged::insert(File::getRelativePathFromID($file->getFileID()), t('File Deleted'));
    }

    public function on_page_delete($ctrl, $event)
    {
        // Validate Args
        if (!isset($ctrl)) return;
        if (!isset($event)) return;

        $page = $event->getPageObject();
        if (strpos($page->getCollectionPath(), '!drafts') !== false) {
            return;
        }
        RedirectUrlsChanged::insert($page->getCollectionPath(), t('Page Deleted'));
    }

    public function on_page_move($ctrl, $event)
    {
        // Validate Args
        if (!isset($ctrl)) return;
        if (!isset($event)) return;

        $page = $event->getPageObject(); //Gets Current Page
        $newPage = $event->getNewParentPageObject(); //Gets New Page

        if (strpos($page->getCollectionPath(), '!drafts') !== false) {
            return;
        }
        else if (strpos($page->getCollectionPath(), '!trash') !== false) {
            return;
        }
        else if (strpos($newPage->getCollectionPath(), '!trash') !== false) {
            return;
        }
        else {
            RedirectUrlsChanged::insert($page->getCollectionPath(), t('Page Moved'));
        }
    }

    public function on_page_move_to_trash($ctrl, $event)
    {
        // Validate Args
        if (!isset($ctrl)) return;
        if (!isset($event)) return;

        $page = $event->getPageObject();
        if (strpos($page->getCollectionPath(), '!drafts') !== false) {
            return;
        }
        RedirectUrlsChanged::insert($page->getCollectionPath(), t('Page Moved to Trash'));
    }

    public function checkWildcard($request_url, $redirect_from)
    {
        $redirect_from = preg_quote($redirect_from, "/");
        $redirect_from = str_replace('\\*', '.*', $redirect_from);
        if (@preg_match('/^' . $redirect_from . '$/u', $request_url)) return true;
        return false;
    }

    public function checkRegEx($request_url, $redirect_from)
    {
        if (@preg_match($redirect_from, $request_url)) return true;
        return false;
    }

    public function is_page_not_found($page)
    {
        if ($page->cPath == '/page_not_found') return true;
        return false;
    }

    public function is_page_forbidden($page)
    {
        if ($page->cPath == '/page_forbidden') return true;
        return false;
    }

    public function is_dashboard($request_uri_lowered)
    {
        if (strpos($request_uri_lowered, '/dashboard/') !== false) return true;
        return false;
    }

    public function is_ajax()
    {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
    }

    public function is_ssl()
    {
        if (isset($_SERVER['HTTPS'])) {
            if ('on' == strtolower($_SERVER['HTTPS']))
                return true;
            if ('1' == $_SERVER['HTTPS'])
                return true;
        } elseif (isset($_SERVER['SERVER_PORT']) && ('443' == $_SERVER['SERVER_PORT'])) {
            return true;
        }
        return false;
    }
}