<?php defined('C5_EXECUTE') or die("Access Denied.");

///////////////////////////////////////////////////////////////////////////////
/// Rule List

?>

<div class="ccm-dashboard-header-buttons">
    <a id="afx-add-redirect" class="btn btn-primary"><i class="fa-plus fa"></i> <?php echo t("Add SEO Redirect") ?></a>
</div>

<div class="ccm-dashboard-content-full">
    <div class="content">

        <div class="row">
            <div class="col-md-12">

                <div id="afx-quick-add-validation"></div>

                <script type="text/template" class="validation-template">
                    <% _.each( validationList, function(  validationItem ){ %>
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <%- validationItem %>
                    </div>
                    <% }); %>
                </script>

                <div id="afx-quick-add-container">
                    <form id="afx-quick-add" action="<?php echo $view->action('quickadd') ?>" method='post'
                          class='form-inline'>
                        <div class="form-group">
                                <span class="afx-quick-add-title"><i
                                            class="fa-mail-forward fa"></i> <?php echo t('Quick Add 301') ?></span>
                        </div>
                        <?php echo $form->label('rFrom', t('From URL: ')) ?>
                        <div class="form-group">
                            <div class="input-group" id="from-url-group">
                                <?php if (!empty($subfolder_install)) { ?>
                                    <div class="input-group-addon"><?php echo '/' . $subfolder_install . '/' ?></div>
                                <?php } else { ?>
                                    <div class="input-group-addon"><?php echo '/' ?></div>
                                <?php } ?>
                                <?php echo $form->text('rFrom', '', array('class' => 'input-sm')) ?>
                            </div>
                        </div>
                        <div class="form-group redirect-type">
                            <label for="redirectToType"
                                   class="control-label"><?php echo t('Destination:') ?></label>
                            <?php
                            $selected = 'cid';
                            echo $form->select('redirectToType', $rRedirectToTypeOptions, $selected); ?>
                        </div>
                        <div
                                class="form-group afx-r-type afx-r-type-cid">
                            <?php echo $form->label('rToCID', t('Page:')); ?>
                            <div class="ccm-page-selector">
                                <input type="hidden" id="rToCID" name="rToCID" value="0">
                                <a href="#" data-selector-link="choose-page"></a>
                            </div>
                        </div>
                        <div
                                class="form-group afx-r-type afx-r-type-fid" style="display: none;">
                            <?php echo $form->label('rToFID', t('File:')); ?>
                            <div class="ccm-page-selector">
                                <input type="hidden" id="rToFID" name="rToFID" value="0">
                                <a href="#" data-selector-link="choose-file"></a>
                            </div>
                        </div>
                        <div
                                class="form-group afx-r-type afx-r-type-url" style="display: none;">
                            <?php echo $form->label('rToURL', t('URL:')); ?>
                            <?php echo $form->text('rToURL', $rURL, array('class' => 'input-sm', 'placeholder' => 'http://www.extdomain.com/products')); ?>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm"><i
                                    class="fa-plus fa"></i> <?php echo t('Quick Add') ?>
                        </button>
                    </form>
                </div>
            </div>
        </div>

        <div class="row afx-border-bottom">
            <div class="col-md-6">
                <div class="pull-left afx-ctrls-text afx-ctrls-bulk-action">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <select id="bulkaction"
                                class="form-control input-sm">
                            <option value=""><?php echo t("Bulk Action") ?></option>
                            <option value="activate_selected"><?php echo t("Activate Selected") ?></option>
                            <option value="deactivate_selected"><?php echo t("Deactivate Selected") ?></option>
                            <option value="delete_selected"><?php echo t("Delete Selected") ?></option>
                            <option value="delete_all"><?php echo t("Delete All") ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pull-right afx-ctrls-text afx-ctrls-entries-sel">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <select id="entryselect"
                                class="form-control input-sm">
                            <option value="5"><?php echo t("5 Entries") ?></option>
                            <option value="10"><?php echo t("10 Entries") ?></option>
                            <option value="25"><?php echo t("25 Entries") ?></option>
                            <option value="50"><?php echo t("50 Entries") ?></option>
                            <option value="100"><?php echo t("100 Entries") ?></option>
                        </select>
                    </div>
                </div>
                <div class="pull-right afx-ctrls-text afx-ctrls-search" style="display: none;">
                    <div class="form-group has-feedback">
                        <label class="control-label">&nbsp;</label>
                        <input type="text" class="form-control input-sm" placeholder="From Search">
                        <span class="fa fa-remove form-control-feedback"
                              style="line-height: 30px;"
                              onclick="$(this).prev('input').val('');return false;"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row afx-border-bottom">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="afx-table-1" class="afx-results-table ccm-search-results-table">
                        <thead>
                        <tr>
                            <th width="60" style="padding: 0 10px;">
                                <span class="afx-th-checkbox">
                                        <input type="checkbox" data-search-checkbox="select-all" class="ccm-flat-checkbox">
                                </span>
                            </th>
                            <th class="ccm-results-list-active-sort-asc" width="130"><a href="" data-sort="rSort"><?php echo t("Sorting") ?></a></th>
                            <th class=""><a href="" data-sort="rFrom"><?php echo t("From URL") ?></a></th>
                            <th class=""><a href="" data-sort="rToURL"><?php echo t("To URL") ?></a></th>
                            <th class="" width="80"><a href="" data-sort="rResponseCode"><?php echo t("Code") ?></a></th>
                            <th class="" width="80"><a href="" data-sort="rCount"><?php echo t("Uses") ?></a></th>
                            <th class="" width="150"><a href="" data-sort="rLastUsed"><?php echo t("Last Used") ?></a></th>
                            <th class="" width="280"></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div id="entrydisplay" class="pull-left afx-ctrls-text afx-ctrls-entries-display"></div>
            </div>
            <div class="col-md-6">
                <div class="afx-ctrls-pagination pull-right"></div>
            </div>
        </div>

    </div>

    <hr/>
    <div class="row">
        <div class="col-sm-12" style="color: #ccc; padding-left:35px; padding-right:35px;">
            <p>
                <small>
                    <?php
                    echo
                        t("Notes: Redirect rules do not apply to the super user by default.") . "<br />" .
                        t("Rules are applied in order (top first, bottom last); only the first matching rule is applied.") . "<br />" .
                        t("Rules can be rearranged by using the arrows or drag and drop.")
                    ?>
                </small>
            </p>

            <?php
            //Check to see if Pretty URLs has been set or not
            if (is_array($prettyUrlArray)) {
                echo '<div class="alert alert-danger">' . t('For this add-on to work completely, please enable Pretty URLs (remove index.php) under ') . $prettyUrlArray['pretty_urls_link_start'] . t('Dashboard > System & Settings > URLs & Redirection') . $prettyUrlArray['pretty_urls_link_end'] . '</div>';
            }

            //Check if there is a subfolder detected but not set in the settings
            if (is_array($subfolderUrlArray)) {
                echo '<div class="alert alert-warning">' . t('We believe you have Concrete5 installed in the following subdirectory: ') . '<strong> ' . $subfolderUrlArray['subfolder'] . '</strong><br/>' . t('For this plugin to work properly, and to save you from having to add your subfolder to every redirect rule, you should assign this as the Subfolder Installation Directory in the %s Redirect Settings %s page', $subfolderUrlArray['redirect_settings_link_start'], $subfolderUrlArray['redirect_settings_link_end']);
            }
            ?>
        </div>
    </div>

</div>

<script type="text/template" class="grid-template">
    <% _.each( ruleslist, function( listItem ){ %>
    <tr>
        <td data-id='<%- listItem.rID %>' data-sort='<%- listItem.rSort %>'
            class="afx-grid-cell ui-dnd-handle first-td">
            <input type="checkbox" data-search-checkbox="individual" class="ccm-flat-checkbox"
                   value="3">
        </td>
        <td class="ui-dnd-handle">
            <div class="btn btn-default btn-sm" style="cursor:move"><i class="fa-arrows-v fa"></i></div>
            <a href="<?php echo URL::to('/ajax/redirect_rules_ajax/up') ?>?rID=<%- listItem.rID %>"
               class="btn btn-success btn-sm grid-command hidden-md hidden-sm hidden-xs"><i
                        class="fa-arrow-up fa"></i></a>
            <a href="<?php echo URL::to('/ajax/redirect_rules_ajax/down') ?>?rID=<%- listItem.rID %>"
               class="btn btn-success btn-sm grid-command hidden-md hidden-sm hidden-xs"><i
                        class="fa-arrow-down fa"></i></a>
        </td>
        <td class="ui-dnd-cancel">
            <% if ( listItem.rIsWildCard ){ %>
            <%- listItem.rFrom %>
            <% } else if ( listItem.rIsRegEx ){ %>
            <%- listItem.rFrom %>
            <% } else { %>
            <?php if (!empty($subfolder_install)) { ?>
                <a href='<?php echo '/' . $subfolder_install ?><%- listItem.rFrom %>' target="_blank"><%- listItem.rFrom
                    %></a>
            <?php } else { ?>
                <a href='<%- listItem.rFrom %>' target="_blank"><%- listItem.rFrom %></a>
            <?php } ?>
            <% } %>
        </td>
        <td class="ui-dnd-handle"><%- listItem.rToURL %></td>
        <td class="ui-dnd-handle"><%- listItem.rResponseCode %></td>
        <td class="ui-dnd-handle"><%- listItem.rCount %></td>
        <td class="ui-dnd-handle"><%- listItem.rLastUsed %></td>
        <td>
            <div class="btn-group pull-right">
                <a class="btn btn-success btn-sm grid-command afx-edit" data-itemid="<%- listItem.rID %>"><i
                            class="fa-pencil-square-o fa"></i> <?php echo t('Edit') ?></a>
                <a href="<?php echo URL::to('/ajax/redirect_rules_ajax/toggle') ?>?rID=<%- listItem.rID %>"
                   class="btn btn-default btn-sm grid-command">
                    <% if ( listItem.rActive ){ %>
                    <i class="fa fa-toggle-on"></i> &nbsp;<?php echo t('Active') ?>&nbsp;
                    <% } else { %>
                    <i class="fa fa-toggle-off"></i></i> <?php echo t('Inactive') ?>
                    <% } %>
                </a>
                <a href="<?php echo URL::to('/ajax/redirect_rules_ajax/delete') ?>?rID=<%- listItem.rID %>"
                   class="btn btn-danger btn-sm grid-command afx-delete"
                   name="ccm-submit-button"><i class="fa-remove fa"></i> <?php echo t('Delete') ?></a>
            </div>
        </td>
    </tr>
    <% }); %>
</script>

<script type="text/javascript">

    $(document).ready(function () {
        AfxRedirectRulesList({
            page_num: 1,
            page_size: 5,
            page_sort: "rSort",
            page_direction: "asc",
            quickadd_url: '<?php echo URL::to('/ajax/redirect_rules_ajax/quickadd') ?>',
            getlist_url: '<?php echo URL::to('/ajax/redirect_rules_ajax/getlist') ?>?',
            sort_url: '<?php echo URL::to('/ajax/redirect_rules_ajax/sort') ?>',
            bulkaction_url: '<?php echo URL::to('/ajax/redirect_rules_ajax/bulkaction') ?>',
            add_url: '<?php echo URL::to('/afixia_seo_redirects/rule_editor/add'); ?>',
            edit_url: '<?php echo URL::to('/afixia_seo_redirects/rule_editor/edit'); ?>',
            edit_txt: '<?php echo t("SEO Redirect Rule") ?>',
            confirm_txt: '<?php echo t("Are you sure you want to {0} items?") ?>',
            alert_txt: '<?php echo t("Please select items below first.") ?>',
            file_txt: '<?php echo t("Choose a File") ?>',
            page_txt: '<?php echo t("Choose a Page") ?>',
            delete_txt: '<?php echo t('Are you sure you want to permanently remove this seo redirect rule?') ?>'
        });
    });

</script>

