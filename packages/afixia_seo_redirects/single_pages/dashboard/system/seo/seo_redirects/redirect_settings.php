<?php defined('C5_EXECUTE') or die("Access Denied.");

///////////////////////////////////////////////////////////////////////////////
/// Settings Editor

?>

<form action="<?php echo $view->action('save') ?>" method='post'>
    <?php echo $form->hidden('rID', $rule->rID); ?>

    <div class="ccm-dashboard-content-full" style="margin-top: 40px;">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div id="afx-settings-editor-validation"></div>
                <script type="text/template" class="validation-template">
                    <% _.each( validationList, function(  validationItem ){ %>
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <p style="white-space: pre-wrap;"><%- validationItem %></p>
                    </div>
                    <% }); %>
                </script>
            </div>
        </div>
        <div id="redirect-settings" class="row" style="display: none;">
            <div class="col-sm-4 col-sm-offset-1">
                <div class="form-group">
                    <?php echo $form->label('sProcessRedirects', t('Process Redirect Rules')); ?>
                    <div class="input-group">
                        <div class="btn-group radioBtn">
                            <a class="btn btn-success btn-sm active" data-toggle="sProcessRedirects"
                               data-title="1" style="width: 80px;"><?php echo t('On'); ?></a>
                            <a class="btn btn-danger btn-sm notActive" data-toggle="sProcessRedirects"
                               data-title="0" style="width: 80px;"><?php echo t('Off'); ?></a>
                            <?php echo $form->hidden('sProcessRedirects', $process_redirects ? "1" : "0") ?>
                        </div>
                    </div>
                    <small><?php echo t('Off will stop all redirect rules.'); ?></small>
                </div>
                <div class="form-group">
                    <?php echo $form->label('sOnly404s', t('Process All Incoming Requests')); ?>
                    <div class="input-group">
                        <div class="btn-group radioBtn">
                            <a class="btn btn-success btn-sm active" data-toggle="sOnly404s"
                               data-title="0" style="width: 80px;"><?php echo t('On'); ?></a>
                            <a class="btn btn-danger btn-sm notActive" data-toggle="sOnly404s"
                               data-title="1" style="width: 80px;"><?php echo t('Off'); ?></a>
                            <?php echo $form->hidden('sOnly404s', $only_404s ? "1" : "0") ?>
                        </div>
                    </div>
                    <small><?php echo t('Off will only apply redirect rules to requests resulting in \'404 - Page Not Found\'.'); ?></small>
                </div>
                <div class="form-group">
                    <?php echo $form->label('sRedirectSuperAdmin', t('Redirect the Super Admin')); ?>
                    <div class="input-group">
                        <div class="btn-group radioBtn">
                            <a class="btn btn-success btn-sm active" data-toggle="sRedirectSuperAdmin"
                               data-title="1" style="width: 80px;"><?php echo t('On'); ?></a>
                            <a class="btn btn-danger btn-sm notActive" data-toggle="sRedirectSuperAdmin"
                               data-title="0" style="width: 80px;"><?php echo t('Off'); ?></a>
                            <?php echo $form->hidden('sRedirectSuperAdmin', $redirect_super_admin ? "1" : "0") ?>
                        </div>
                    </div>
                    <small><?php echo t('When off the super admin is never redirected.'); ?></small>
                </div>
                <div class="form-group">
                    <?php echo $form->label('sLogURLsNotFound', t('Log URLs Not Found')); ?>
                    <div class="input-group">
                        <div class="btn-group radioBtn">
                            <a class="btn btn-success btn-sm active" data-toggle="sLogURLsNotFound"
                               data-title="1" style="width: 80px;"><?php echo t('On'); ?></a>
                            <a class="btn btn-danger btn-sm notActive" data-toggle="sLogURLsNotFound"
                               data-title="0" style="width: 80px;"><?php echo t('Off'); ?></a>
                            <?php echo $form->hidden('sLogURLsNotFound', $log_urls_not_found ? "1" : "0") ?>
                        </div>
                    </div>
                    <small><?php echo t('Turn on/off URLs not found logging.'); ?></small>
                </div>
                <div class="form-group">
                    <?php echo $form->label('sNotFoundLimit', t('URLs Not Found Log Limit')); ?>
                    <?php echo $form->text('sNotFoundLimit', $not_found_limit); ?>
                    <small><?php echo t('Max number of URLs to store. When this number is exceeded, an old record is automatically deleted to allow insertion of new record.'); ?></small>
                </div>
                <div class="form-group">
                    <?php echo $form->label('sSubfolderInstall', t('Subfolder Installation Directory')); ?>
                    <?php echo $form->text('sSubfolderInstall', $subfolder_install); ?>
                    <small><?php echo t('If Concrete5 is installed within a subfolder, you can prefix all of your redirects by filling in the subfolder field above.'); ?></small>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="addIgnore"
                                   class="control-label"><?php echo t('Ignore list for URLs Not Found Log'); ?></label>
                            <div class="input-group">
                                <span class="input-group-addon">/</span>
                                <input type="text" class="form-control afx-ignore-item" placeholder="favicon.ico">
                                <span class="input-group-btn">
                                <button class="btn btn-secondary afx-add-ignore"
                                        type="button"><?php echo t('Add'); ?></button>
                              </span>
                            </div>
                            <small><?php echo t('(Relative paths only. example: /favicon.ico)'); ?></small>
                        </div>
                        <table class="table table-striped afx-ignore-list">
                            <thead>
                            <tr>
                                <th>URL</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <div style="display: none;">
                            <?php echo $form->textarea('sIgnoreList', $ignore_list); ?>
                        </div>
                    </div>
                </div>
                <script type="text/template" class="grid-template">
                    <% _.each( ignore_list, function( list_item ){ %>
                    <tr>
                        <td>
                            <%- list_item.url %>
                        </td>
                        <td>
                            <div class="btn-group pull-right">
                                <a class="btn btn-danger btn-sm afx-delete"
                                   data-id='<%- list_item.id %>'><i class="fa-remove fa"></i> <?php echo t('Delete') ?>
                                </a>
                            </div>
                        </td>
                    </tr>
                    <% }); %>
                </script>
            </div>
        </div>
    </div>

    <script>

        $(document).ready(function () {
            AfxSettingsEditor({
                delete_txt: '<?php echo t('Are you sure you want remove this item?') ?>'
            }, <?php echo $validation ?>);
        });

    </script>

    <div class="ccm-dashboard-form-actions-wrapper">
        <div class="ccm-dashboard-form-actions">
            <?php echo $form->submit('add', t('Save'), array('class' => 'btn btn-primary pull-right')) ?>
        </div>
    </div>

</form>

