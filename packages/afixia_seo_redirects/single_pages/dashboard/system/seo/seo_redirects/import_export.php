<?php defined('C5_EXECUTE') or die("Access Denied.");

///////////////////////////////////////////////////////////////////////////////
/// Import/Export

$valt = Core::make('helper/validation/token');
$th = Core::make('helper/text');

?>

<div class="ccm-dashboard-content-full" style="margin-top: 40px;">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div id="afx-import-export-validation"></div>
            <script type="text/template" class="validation-template">
                <% _.each( validationList, function( validationItem ){ %>
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <p style="white-space: pre-wrap;"><%- validationItem %></p>
                </div>
                <% }); %>
            </script>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row">
                <div class="col-sm-4">
                    <h4>Import</h4>
                    <p><?php echo t('Import SEO Redirect Rules from a CSV data file. Please refer to import sample file to correctly format your CSV for importing.') ?></p>
                    <form method="post" action="<?php echo $view->action('import', $valt->generate()) ?>"
                          id="afx-import-form" enctype="multipart/form-data">
                        <label class="btn btn-primary" for="import-file-selector">
                            <input id="import-file-selector" name="import-file-selector" type="file" style="display:none;">
                            Browse...
                        </label>
                        <button type="submit" class="btn btn-primary"><i class='fa fa-upload'></i> <?php echo t('Import Rules') ?></button>
                        <span class='label label-info' id="import-file-info"></span>
                    </form>
                    <p style="margin-top: 10px;"><a href="<?php echo $view->action('import_sample_csv', $valt->generate()) ?>"><i class='fa fa-download'></i> <?php echo t('Import Sample File') ?></a></p>
                </div>
	            <div class="col-sm-8">
		            <div class="row well">
			            <div class="col-sm-6">
				            <h4>Backup</h4>
				            <p><?php echo t('Generate a backup file of your SEO Redirect Rules.  The backup file is intended for restoring the SEO Redirect Rules data table.') ?></p>
				            <a id="ccm-export-results" class="btn btn-primary" href="<?php echo $view->action('backup', $valt->generate()) ?>?<?php echo $query ?>">
					            <i class='fa fa-download'></i> <?php echo t('Backup Rules') ?>
				            </a>
			            </div>
			            <div class="col-sm-6">
				            <h4>Restore</h4>
				            <p><?php echo t('Use this feature to restore your SEO Redirect Rules from a previously generated backup file of the SEO Redirect Rules data table.') ?></p>
				            <form method="post" action="<?php echo $view->action('restore', $valt->generate()) ?>" id="afx-restore-form" enctype="multipart/form-data">
					            <label class="btn btn-primary" for="restore-file-selector">
						            <input id="restore-file-selector" name="restore-file-selector" type="file" style="display:none;">
						            Browse...
					            </label>
					            <button type="submit" class="btn btn-primary"><i class='fa fa-upload'></i> <?php echo t('Restore Rules') ?></button>
					            <span class='label label-info' id="restore-file-info"></span>
				            </form>
			            </div>
		            </div>
	            </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        AfxImportExport({
            import_file_error:  '<?php echo t("Click [Browse...] to select a .CSV data file, then click [Import Rules]") ?>',
            restore_file_error:  '<?php echo t("Click [Browse...] to select your backup file, then click [Restore Rules]") ?>',
            import_warning: '<?php echo t("Notice: The import process will add all new items at the end of the SEO Redirect Rules list. Do you want to continue?") ?>',
            restore_warning: '<?php echo t("Warning: The restore process will delete all existing SEO Redirect Rules. Do you want to continue?") ?>',
        }, <?php echo $validation ?>);
    });

</script>



