<?php defined('C5_EXECUTE') or die("Access Denied.");

///////////////////////////////////////////////////////////////////////////////
/// Urls Not Found List

?>

<div class="ccm-dashboard-content-full">
    <div class="content">

        <div class="row afx-border-bottom">
            <div class="col-md-6">
                <div class="pull-left afx-ctrls-text afx-ctrls-bulk-action">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <select id="bulkaction"
                                class="form-control input-sm">
                            <option value=""><?php echo t("Bulk Action") ?></option>
                            <option value="delete_selected"><?php echo t("Delete Selected") ?></option>
                            <option value="delete_all"><?php echo t("Delete All") ?></option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="pull-right afx-ctrls-text afx-ctrls-entries-sel">
                    <div class="form-group">
                        <label class="control-label">&nbsp;</label>
                        <select id="entryselect"
                                class="form-control input-sm">
                            <option value="5"><?php echo t("5 Entries") ?></option>
                            <option value="10"><?php echo t("10 Entries") ?></option>
                            <option value="25"><?php echo t("25 Entries") ?></option>
                            <option value="50"><?php echo t("50 Entries") ?></option>
                            <option value="100"><?php echo t("100 Entries") ?></option>
                        </select>
                    </div>
                </div>
                <div class="pull-right afx-ctrls-text afx-ctrls-search" style="display: none;">
                    <div class="form-group has-feedback">
                        <label class="control-label">&nbsp;</label>
                        <input type="text" class="form-control input-sm" placeholder="From Search">
                        <span class="fa fa-remove form-control-feedback"
                              style="line-height: 30px;"
                              onclick="$(this).prev('input').val('');return false;"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row afx-border-bottom">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="afx-table-1" class="afx-results-table ccm-search-results-table">
                        <thead>
                        <tr>
                            <th width="60" style="padding: 0 10px;"><span class="afx-th-checkbox"><input type="checkbox"
                                                                                                 data-search-checkbox="select-all"
                                                                                                 class="ccm-flat-checkbox"></span>
                            </th>
                            <th class=""><a href="" data-sort="rFrom"><?php echo t("From URL") ?></a></th>
                            <th class=""><a href="" data-sort="rReferer"><?php echo t("Referer URL") ?></a></th>
                            <th width="80" class="ccm-results-list-active-sort-desc" width="6%"><a href="" data-sort="rCount"><?php echo t("Uses") ?></a></th>
                            <th width="150" class=""><a href="" data-sort="rLastUsed"><?php echo t("Last Used") ?></a></th>
                            <th width="240"></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div id="entrydisplay" class="pull-left afx-ctrls-text afx-ctrls-entries-display"></div>
            </div>
            <div class="col-md-6">
                <div class="afx-ctrls-pagination pull-right"></div>
            </div>
        </div>

    </div>
</div>

<script type="text/template" class="grid-template">
    <% _.each( notfoundlist, function( listItem ){ %>
    <tr>
        <td data-id='<%- listItem.rID %>' data-sort='<%- listItem.rSort %>' class="afx-grid-cell first-td">
            <input type="checkbox" data-search-checkbox="individual" class="ccm-flat-checkbox"
                   value="3">
        </td>
        <td><%- listItem.rFrom %></td>
        <td><%- listItem.rReferer %></td>
        <td><%- listItem.rCount %></td>
        <td><%- listItem.rLastUsed %></td>
        <td>
            <div class="btn-group pull-right">
                <a class="btn btn-default btn-sm grid-command afx-create" data-item="<%- listItem.rFromEncoded %>"><i
                            class="fa-plus fa"></i> Create Rule</a>
                <a href="<?php echo URL::to('/ajax/urls_not_found_ajax/delete') ?>?rID=<%- listItem.rID %>"
                   class="btn btn-danger btn-sm grid-command afx-delete"
                   name="ccm-submit-button"><i class="fa-remove fa"></i> <?php echo t('Delete') ?></a>
            </div>
        </td>
    </tr>
    <% }); %>
</script>

<script type="text/javascript">

    $(document).ready(function () {
        AfxUrlsNotFoundList({
            page_num: 1,
            page_size: 5,
            page_sort: "rCount",
            page_direction: "desc",
            getlist_url: '<?php echo URL::to('/ajax/urls_not_found_ajax/getlist') ?>?',
            bulkaction_url: '<?php echo URL::to('/ajax/urls_not_found_ajax/bulkaction') ?>',
            add_url: '<?php echo URL::to('/afixia_seo_redirects/rule_editor/add'); ?>',
            edit_txt: '<?php echo t("SEO Redirect Rule") ?>',
            confirm_txt: '<?php echo t("Are you sure you want to {0} items?") ?>',
            alert_txt: '<?php echo t("Please select items below first.") ?>',
            delete_txt: '<?php echo t('Are you sure you want to permanently remove this?') ?>'
        });
    });

</script>
