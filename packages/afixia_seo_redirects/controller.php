<?php

namespace Concrete\Package\AfixiaSeoRedirects;

defined('C5_EXECUTE') or die("Access Denied.");

use Core;
use Database;
use Events;
use Config;
use Package;
use \Concrete\Core\Page\Single as SinglePage;
use \Concrete\Core\Support\Facade\Route as Route;
use \Concrete\Package\AfixiaSeoRedirects\Src\PackageServiceProvider;

class Controller extends Package
{
    //////////////////////////////////////////////////////////////////////////////////////////
    // Package Meta

    protected $pkgHandle = 'afixia_seo_redirects';
    protected $appVersionRequired = '5.7.4';
    protected $pkgVersion = '1.1.2';

    //////////////////////////////////////////////////////////////////////////////////////////
    // Install / Uninstall

    public function getPackageDescription()
    {
        return t("Redirect an http[s] request to another page, file or URL.");
    }

    public function getPackageName()
    {
        return t("Afixia: SEO Redirects");
    }

    public function install()
    {
        $pkg = parent::install();
        if(!$this->SinglePageExists('/dashboard/seo_tools')) {
            $seo_tools = SinglePage::add('/dashboard/seo_tools', $pkg);
            $seo_tools->update(array('cName' => 'SEO Tools'));
        }
        $seo_redirects = SinglePage::add('/dashboard/system/seo/seo_redirects', $pkg);
        $seo_redirects->update(array('cName' => 'SEO Redirects'));
        SinglePage::add('/dashboard/system/seo/seo_redirects', $pkg);
        SinglePage::add('/dashboard/system/seo/seo_redirects/redirect_rules', $pkg);
        SinglePage::add('/dashboard/system/seo/seo_redirects/urls_changed', $pkg);
        SinglePage::add('/dashboard/system/seo/seo_redirects/urls_not_found', $pkg);
        SinglePage::add('/dashboard/system/seo/seo_redirects/redirect_settings', $pkg);
        $import_export = SinglePage::add('/dashboard/system/seo/seo_redirects/import_export', $pkg);
        $import_export->update(array('cName' => 'Import and Export'));
        $this->installData();
    }

    public function upgrade()
    {
        parent::upgrade();
        $pkg = Package::getByHandle('afixia_seo_redirects');

        if(!$this->SinglePageExists('/dashboard/seo_tools')) {
            $seo_tools = SinglePage::add('/dashboard/seo_tools', $pkg);
            $seo_tools->update(array('cName' => 'SEO Tools'));
        }
        if(!$this->SinglePageExists('/dashboard/system/seo/seo_redirects/import_export')) {
            $import_export = SinglePage::add('/dashboard/system/seo/seo_redirects/import_export', $pkg);
            $import_export->update(array('cName' => 'Import and Export'));
        }
        $this->installData();
    }

    public function installData()
    {
        //Check to see if settings exist, if so, keep them as they are, as they may have chosen to keep them on uninstall.

        if(!Config::has($this->pkgHandle.'.process_redirects', true)){
            Config::save($this->pkgHandle . '.process_redirects', true);
        }
        if(!Config::has($this->pkgHandle.'.only_404s', true)) {
            Config::save($this->pkgHandle.'.only_404s', false);
        }
        if(!Config::has($this->pkgHandle.'.redirect_super_admin', true)){
            Config::save($this->pkgHandle.'.redirect_super_admin', true);
        }
        if(!Config::has($this->pkgHandle.'.log_urls_not_found', true)){
            Config::save($this->pkgHandle.'.log_urls_not_found', true);
        }
        if(!Config::has($this->pkgHandle.'.not_found_limit', true)){
            Config::save($this->pkgHandle.'.not_found_limit', 1000);
        }
        if(!Config::has($this->pkgHandle.'.subfolder_install', true)){
            Config::save($this->pkgHandle.'.subfolder_install', '');
        }
        if(!Config::has($this->pkgHandle.'.ignore_list', true)) {
            Config::save($this->pkgHandle . '.ignore_list',
                urlencode('{"ignore_list":[{"id":4161696812,"url":"\/favicon.ico"}]}'));
        }
    }

    public function uninstall()
    {
        parent::uninstall();

        $r = \Request::getInstance();

        //Delete all data
        if ($r->request->get('remove_data')) {
            $db = Database::connection();
            $db->query('drop table afxRedirectRules');
            $db->query('drop table afxRedirectNotFound');
            $db->query('drop table afxRedirectUrlsChanged');
        }

        //Reset settings to default for the next install
        if (!$r->request->get('keep_settings')) {
            Config::save($this->pkgHandle.'.process_redirects', true);
            Config::save($this->pkgHandle.'.only_404s', false);
            Config::save($this->pkgHandle.'.redirect_super_admin', true);
            Config::save($this->pkgHandle.'.not_found_limit', 1000);
            Config::save($this->pkgHandle.'.subfolder_install', '');
        }

    }

    public function SinglePageExists($cPath) {
        $ret = false;
        $handles = Package::getInstalledHandles();
        foreach ($handles as $handle) {
            $package = Package::getByHandle($handle);
            $single_pages = SinglePage::getListByPackage($package);
            foreach ($single_pages as $single_page) {
                if($single_page->cPath == $cPath) $ret = true;
            }
        }
        return($ret);
    }

    public function on_start()
    {
        // Register Services
        $app = Core::getFacadeApplication();
        $sp = new PackageServiceProvider($app);
        $sp->register();

        // Register AJAX Routes
        Route::register('/ajax/redirect_rules_ajax/bulkaction', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\RedirectRulesAjax::bulkaction');
        Route::register('/ajax/redirect_rules_ajax/delete', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\RedirectRulesAjax::delete');
        Route::register('/ajax/redirect_rules_ajax/down', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\RedirectRulesAjax::down');
        Route::register('/ajax/redirect_rules_ajax/getlist', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\RedirectRulesAjax::getlist');
        Route::register('/ajax/redirect_rules_ajax/quickadd', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\RedirectRulesAjax::quickadd');
        Route::register('/ajax/redirect_rules_ajax/sort', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\RedirectRulesAjax::sort');
        Route::register('/ajax/redirect_rules_ajax/toggle', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\RedirectRulesAjax::toggle');
        Route::register('/ajax/redirect_rules_ajax/up', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\RedirectRulesAjax::up');
        Route::register('/ajax/urls_not_found_ajax/bulkaction', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\UrlsNotFoundAjax::bulkaction');
        Route::register('/ajax/urls_not_found_ajax/delete', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\UrlsNotFoundAjax::delete');
        Route::register('/ajax/urls_not_found_ajax/getlist', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\UrlsNotFoundAjax::getlist');
        Route::register('/ajax/urls_changed_ajax/bulkaction', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\UrlsChangedAjax::bulkaction');
        Route::register('/ajax/urls_changed_ajax/delete', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\UrlsChangedAjax::delete');
        Route::register('/ajax/urls_changed_ajax/getlist', '\Concrete\Package\AfixiaSeoRedirects\Controller\Ajax\UrlsChangedAjax::getlist');

        // Register Dialog Routes
        Route::register('/afixia_seo_redirects/rule_editor/add','\Concrete\Package\AfixiaSeoRedirects\Controller\Dialog\RuleEditor::add');
        Route::register('/afixia_seo_redirects/rule_editor/add/submit','\Concrete\Package\AfixiaSeoRedirects\Controller\Dialog\RuleEditor::submit');
        Route::register('/afixia_seo_redirects/rule_editor/edit','\Concrete\Package\AfixiaSeoRedirects\Controller\Dialog\RuleEditor::edit');
        Route::register('/afixia_seo_redirects/rule_editor/edit/submit','\Concrete\Package\AfixiaSeoRedirects\Controller\Dialog\RuleEditor::submit');

        // Listen For CMS Changes
        Events::addListener(
            'on_file_delete', function ($event) { Core::make('afixia_seo_redirects/helper/seo_redirector')->on_file_delete($this, $event); }
        );

        Events::addListener(
            'on_page_delete', function ($event) { Core::make('afixia_seo_redirects/helper/seo_redirector')->on_page_delete($this, $event); }
        );

        Events::addListener(
            'on_page_move', function ($event) { Core::make('afixia_seo_redirects/helper/seo_redirector')->on_page_move($this, $event); }
        );

        Events::addListener(
            'on_page_move_to_trash', function ($event) { Core::make('afixia_seo_redirects/helper/seo_redirector')->on_page_move_to_trash($this, $event); }
        );

        // Listen For Requests
        Events::addListener(
            'on_start', function ($event) { Core::make('afixia_seo_redirects/helper/seo_redirector')->on_request($this, $event); }
        );

	    // Register Assets
	    $al = \AssetList::getInstance();
	    $al->register('javascript', 'afixia_seo_redirects', 'js/afixia_seo_redirects.js', array('minify' => false, 'position' => \Concrete\Core\Asset\Asset::ASSET_POSITION_HEADER, 'local'=>true, 'combine'=> false), 'afixia_seo_redirects');
	    $al->register('css', 'afixia_seo_redirects', 'css/afixia_seo_redirects.css', array('minify' => false, 'position' => \Concrete\Core\Asset\Asset::ASSET_POSITION_FOOTER, 'local'=>true, 'combine'=> false), 'afixia_seo_redirects');
        $al->registerGroup('afixia_seo_redirects', array(
			array('css', 'afixia_seo_redirects'),
			array('javascript', 'afixia_seo_redirects')
		));
    }
}
