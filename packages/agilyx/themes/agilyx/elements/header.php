<!DOCTYPE HTML>
<html lang="en">
	
	<head>
		<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,300i,400,400i,500,500i,600,700,700i,800,900" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $view->getThemePath(); ?>/js/fancybox/jquery.fancybox.min.css">
		<link rel="stylesheet" media="screen" type="text/css" href="<?php echo $view->getThemePath(); ?>/css/styles.css" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, maximum-scale=1.0, minimum-scale=1.0, initial-scale=1.0" />
		<?php Loader::element('header_required'); ?>
		<?php 
			global $u;
			$page = Page::getCurrentPage();
			$pagename = "page-".strtolower($page->getCollectionName());
			$pagename = str_replace(" ", "-", $pagename);
			global $page;
			$isLoggedIn = User::isLoggedIn();
			$ih = Loader::helper('image');
			$hero = $c->getAttribute('hero');
			$hero_height = str_replace("%", "", $c->getAttribute('hero_height'));
			$heroclass = "";
			if (empty($hero_height)){
				$hero_height = 30;
			}
			if ($hero_height == 30){
				$heroclass = "smaller";
			}
			if ($hero) {
				$heroimg = $ih->getThumbnail($hero, 2500, 2500, true);
		    	$herourl = 'style="background-image: url('.$heroimg->src.'); background-position: center center; background-size: cover; height: '.$hero_height.'vh"';
		    } else {
			    $herourl = 'style="background-image: url(/application/files/7215/2450/9586/AGX18501-Hero-VectorArt-2-SecondaryPgs-1500x628.jpg); background-position: center center; background-size: cover; height: '.$hero_height.'vh"';
		    }
		?>
		<script type="text/javascript" src="https://tools.euroland.com/tools/common/eurolandiframeautoheight/eurolandtoolsintegrationobject.js"></script>
	</head>

<body id="agilyx" class="<?php echo $pagename; if ($isLoggedIn) { echo " loggedin"; } if ($c->isEditMode()) { echo " in_edit_mode "; } ?>">
	<div id="content">
	<?php
		$headerClass = "";
		if($c->getAttribute('transparentHeaderBackground')){
			$headerClass = "transparentBackground";
		}
	?>
	<header class="<?php echo($headerClass); ?>">
		
		<?php if (!$c->isEditMode()) { ?>
			<a href="/" class="logo">
				<?php 
					$a = new GlobalArea('Site Logo');
					$a->display();
				?>
			</a>
		<?php } ?>
		<?php 
			$a = new GlobalArea('Header Navigation');
			$a->display();
		?>
		<a href="#" class="menu">
			<i class="fa fa-bars" aria-hidden="true"></i>
		</a>
	</header>
	
	<div id="mobile_menu">
		<a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a>
		<?php 
			$a = new GlobalArea('Header Navigation');
			$a->display();
		?>
<!--
		<ul role="navigation">
			<li><a href="#" class="active">Home</a></li>
			<li><a href="#">Our Vision</a></li>
			<li><a href="#">Our Technology</a></li>
			<li><a href="#">Newsroom</a></li>
			<li><a href="#">About Us</a></li>
			<li><a href="#">Contact Us</a></li>
		</ul>
-->
	</div>
	
	<div id="hero" class="<?php echo $heroclass; ?>" <?php echo $herourl; ?>>
		<div class="container">
			<?php $a = new Area('Hero Content'); $a->enableGridContainer(); $headerblocks = $a->getTotalBlocksInArea($c); 
				if ($headerblocks < 1 && !$c->isEditMode()){
					echo '<h1 style="text-align: center; text-transform: uppercase;">'.$c->getCollectionName().'</h1>';
				} else {
					$a->display($c);
				}	
			?>
		</div>
		<?php if ($pagename == "page-home") {
			echo '<a href="#" class="scroll"></a>';
		} ?>
	</div>