<?php Loader::element('footer_required'); 
	
	$page = Page::getCurrentPage();
	$parentId=Page::getByID($page->getCollectionParentID()); //notice how I use $childId	

?>

<div class="footer">
	<div class="container">
		<?php $a = new GlobalArea('Footer Contact Updated'); $a->enableGridContainer(); $a->display($c); ?>
	</div>
</div>

<footer>
	<div class="container">
<!-- 		<?php $stack = Stack::getByName('Footer Contact'); if ($stack){ $stack->display(); }  ?> -->
		<hr>
			<div class="nav">
				<div class="columns col-sm-8">
					<?php $stack = Stack::getByName('Footer Column 1'); if ($stack){ $stack->display(); }  ?>
				</div>
				<div class="columns col-sm-8">
					<?php $stack = Stack::getByName('Footer Column 2'); if ($stack){ $stack->display(); }  ?>
				</div>
				<div class="columns col-sm-8">
					<?php $stack = Stack::getByName('Footer Column 3'); if ($stack){ $stack->display(); }  ?>
				</div>
				<div class="columns col-sm-8">
					<?php $stack = Stack::getByName('Footer Column 4'); if ($stack){ $stack->display(); }  ?>
				</div>
			</div>
		<hr>
		<div class="copyright">
			<p>&copy;<?php echo date("Y"); ?> Agilyx, Inc</p>
			<?php $stack = Stack::getByName('Footer Utility Nav'); if ($stack){ $stack->display(); }  ?>
			<?php if (!$c->isEditMode()) { ?>
				<a href="/" class="logo">
					<?php 
						$a = new GlobalArea('Site Logo');
						$a->display();
					?>
				</a>
			<?php } ?>
		</div>
	</div>
	<script type="text/javascript" charset="utf-8" src="<?php echo $view->getThemePath(); ?>/js/fancybox/jquery.fancybox.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="<?php echo $view->getThemePath(); ?>/js/custom.js"></script>
	
	
</footer>
</div>
</body>

</html>