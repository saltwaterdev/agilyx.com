<?php $this->inc('elements/header.php'); $page = Page::getCurrentPage();?>
	
<div id="main_contents" class="container <?php if ($page->getAttribute('no_margin') == 1){echo 'no-pad';} ?>">
	<?php $a = new Area('Main'); $a->enableGridContainer(); $a->display($c); ?>
</div>

<?php $this->inc('elements/footer.php'); ?>