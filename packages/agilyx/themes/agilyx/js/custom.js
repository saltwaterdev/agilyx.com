$(document).ready(function() {
	
	$(document).ready(function() {			
		$('iframe.EurolandTool').EurolandIFrameAutoHeight();
	});
	
	up = 0;
	down = 0;
	direction = 0;
	
	$('body').bind('mousewheel', function(e){
		up = 0;
		down = 0;
        if(e.originalEvent.wheelDelta /120 > 0) {
//            console.log('scrolling up !');
           up = up + 1;
//            console.log(up);
        }
        else{
//             console.log('scrolling down !');
        }
    });
    
    var event = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
	window.addEventListener(event, callback);

	function report(ammout) {
		if (ammout > 1.5 || ammout < -1.5){
			hideNav();
		}
	}
	scrollTop();
	function scrollTop(){
		if ($(document).scrollTop() > 150){
			$("header").addClass("sticky");
			if ($("body").hasClass("loggedin")){
				topmargin = "48px";
			} else{
				topmargin = "0px";
			}
			$("header.sticky").animate({
				top: topmargin
			}, 150, function() {
			});
		} else {
			$("header").removeClass("sticky");
		}
	}
	$( document ).scroll(function() {
		scrollTop();
	});
	function callback(event) {
	    var normalized;
	    if (event.wheelDelta) {
	        normalized = (event.wheelDelta % 120 - 0) == -0 ? event.wheelDelta / 120 : event.wheelDelta / 12;
	    } else {
	        var rawAmmount = event.deltaY ? event.deltaY : event.detail;
	        normalized = -(rawAmmount % 3 ? rawAmmount * 10 : rawAmmount / 3);
	    }
	    report(normalized);
	}
	
	$("body").on("click","header a.menu, #mobile_menu a.close",function(e) {
		e.preventDefault();
		menuMove();
	});
	$("body").on("click","#hero a.scroll",function(e) {
		e.preventDefault();
		$('html,body').animate({
		   scrollTop: $("#hero").next(".container").offset().top
		});
	});
	$( "header ul.nav li" ).mouseover(function() {
		if ( $(this).children("ul").length){
			elem = $(this).children("ul");
			elem.removeClass("right").removeClass("left");
			var p = $(this);
			var offset = p.offset();
			var menuWidth = elem.width();
			if($(window).width() - offset.left - menuWidth < 100){
				elem.addClass("right");
			} else {
				elem.addClass("left");
			}
			
			menuitemHeight = elem.data("height");
			if (elem.hasClass("open")){
				
			} else {
				$("header ul.nav li").children("ul").removeClass("open");
				elem.addClass("open");
				$("header ul.nav li").children("ul").animate({
					height: "0px"
				}, 150, function() {
				});
				elem.animate({
					height: menuitemHeight+"px"
				}, 150, function() {
				});
			}
		} else {
			if (!$(this).parent("ul").hasClass("found")){
				$("header ul.nav li").children("ul").removeClass("open");
				$("header ul.nav li").children("ul").animate({
					height: "0px"
				}, 150, function() {
				});
			}
		}
	});
	$( "#main_contents" ).mouseover(function() {
		hideNav();
	});
	function hideNav(){
		$("header ul.nav li").children("ul").removeClass("open");
		$(".found").removeClass("open");
		$("header ul.nav li").children("ul").css("height","0");
	}
	$( window ).resize(function() {
		if ($(window).width() > 767) {
			if ($("body").hasClass("menu")){
				menuMove();
			}
		}
		setMenuHeight();
		if ($(".expand_form").find(".hidden").hasClass("open")){
			$(".expand_form").find(".hidden").removeClass("open");
			$(".expand_form").find(".hidden").attr("style","");
			expandHeight();
			$(".expand_form").find(".hidden").addClass("open");
			moveHeight = parseInt($( ".expand_form" ).find(".hidden").attr("data-height"));
// 			console.log(moveHeight);
			$(".expand_form").find(".hidden").css("height",moveHeight);
		} else {
// 			console.log("Test2");
			$(".expand_form").find(".hidden").attr("style","");
			expandHeight();
			$(".expand_form").find(".hidden").css("height","0");
		}
	});
	if ($(this).find(".expand_form")[0]){
		if ($(".expand_form").find(".hidden").hasClass("open")){
			$(".expand_form").find(".hidden").removeClass("open");
			$(".expand_form").find(".hidden").attr("style","");
			expandHeight();
			$(".expand_form").find(".hidden").addClass("open");
			moveHeight = parseInt($( ".expand_form" ).find(".hidden").attr("data-height"));
// 			console.log(moveHeight);
			$(".expand_form").find(".hidden").css("height",moveHeight);
		} else {
			$(".expand_form").find(".hidden").attr("style","");
			expandHeight();
			$(".expand_form").find(".hidden").css("height","0");
		}
	}
	$("body").on("click",".expand_form .expand, .expand_form .collapse",function(e) {
		e.preventDefault();
		$(".expand_form").find(".hidden").toggleClass("open");
		if ($(".expand_form").find(".hidden").hasClass("open")){
			$(".expand_form .expand").hide();
			$(".expand_form #question").addClass("more");
			moveHeight = parseInt($( ".expand_form" ).find(".hidden").attr("data-height"));
			$( ".expand_form" ).find(".hidden").animate({
				height: moveHeight+"px"
			}, 150, function() {
			});
		} else {
			$(".expand_form #question").removeClass("more");
			$( ".expand_form" ).find(".hidden").animate({
				height: "0"
			}, 150, function() {
				$(".expand_form .expand").show();
			});
		}
	});
	function expandHeight(){
		formHeight = $(".expand_form").find(".hidden").outerHeight();
		$(".expand_form").find(".hidden").attr("data-height",formHeight);
	}
	$('input[type=radio][name=category1], input[type=radio][name=category2], input[type=radio][name=category3]').change(function() {
		$(this).parent(".col").children('input[type=radio]').removeClass("active");
		$(this).addClass("active");
	});
	function menuMove(){
		$("body").toggleClass("menu");
		if ($("body").hasClass("menu")){
			$("body, html").css("overflow","hidden");
			$( "#mobile_menu" ).animate({
				left: "0"
			}, 150, function() {
			});
		} else {
			$("body, html").attr("style","");
			$( "#mobile_menu" ).animate({
				left: "100%"
			}, 150, function() {
			});
		}
	}
	function setMenuHeight(){
		$( "header ul.nav li ul" ).attr("style","").removeClass("found");
		$( "header ul.nav li" ).each(function( index ) {
			menuHeight = $(this).children("ul").outerHeight();
			if (navigator.userAgent.indexOf('Edge') >= 0){
				menuHeight = menuHeight + 30;
			}
			$(this).children("ul").attr("data-height",menuHeight);
			$(this).children("ul").addClass("found");
			$(this).children("ul").css("height","0");
		});
	}
	setMenuHeight();
	setTimeout(function(){ 
		setMenuHeight();
	}, 1000);

	$( "#mobile_menu ul.nav li" ).each(function( index ) {
		if ( $(this).children("ul").length){
			$(this).children("ul").attr("data-height",$(this).children("ul").outerHeight()).css("height",0);
			$(this).children("a").addClass("expand").append('<i class="fa fa-angle-down" aria-hidden="true"></i>');
		}
	});
	$("body").on("click","#mobile_menu a.expand i",function(e) {
		e.preventDefault();
		elem = $(this).parent("a").next("ul");
		if (elem.hasClass("open")){
			$( "#mobile_menu ul.nav li a").removeClass("opened");
			$( "#mobile_menu ul.nav li ul" ).removeClass("open");
			$( "#mobile_menu ul.nav li ul" ).animate({
				height: 0
			}, 150, function() {
			});
		} else {
			$( "#mobile_menu ul.nav li a").removeClass("opened");
			$( "#mobile_menu ul.nav li ul" ).removeClass("open");
			$( "#mobile_menu ul.nav li ul" ).animate({
				height: 0
			}, 150, function() {
			});
			elem.addClass("open");
			$(this).parent("a").addClass("opened");
			elem.animate({
				height: elem.data("height")
			}, 150, function() {
			});
		}
	});
});


/**
* jQuery scroroller Plugin 1.0
*
* http://www.tinywall.net/
* 
* Developers: Arun David, Boobalan
* Copyright (c) 2014 
*/
(function($){
    $(window).on("load",function(){
        $(document).scrollzipInit();
        $(document).rollerInit();
    });
    $(window).on("load scroll resize", function(){
        $('.numscroller').scrollzip({
            showFunction    :   function() {
                                    numberRoller($(this).attr('data-slno'));
                                },
            wholeVisible    :     false,
        });
    });
    $.fn.scrollzipInit=function(){
        $('body').prepend("<div style='position:fixed;top:0px;left:0px;width:0;height:0;' id='scrollzipPoint'></div>" );
    };
    $.fn.rollerInit=function(){
        var i=0;
        $('.numscroller').each(function() {
            i++;
           $(this).attr('data-slno',i); 
           $(this).addClass("roller-title-number-"+i);
        });        
    };
    $.fn.scrollzip = function(options){
        var settings = $.extend({
            showFunction    : null,
            hideFunction    : null,
            showShift       : 0,
            wholeVisible    : false,
            hideShift       : 0,
        }, options);
        return this.each(function(i,obj){
            $(this).addClass('scrollzip');
            if ( $.isFunction( settings.showFunction ) ){
                if(
                    !$(this).hasClass('isShown')&&
                    ($(window).outerHeight()+$('#scrollzipPoint').offset().top-settings.showShift)>($(this).offset().top+((settings.wholeVisible)?$(this).outerHeight():0))&&
                    ($('#scrollzipPoint').offset().top+((settings.wholeVisible)?$(this).outerHeight():0))<($(this).outerHeight()+$(this).offset().top-settings.showShift)
                ){
                    $(this).addClass('isShown');
                    settings.showFunction.call( this );
                }
            }
            if ( $.isFunction( settings.hideFunction ) ){
                if(
                    $(this).hasClass('isShown')&&
                    (($(window).outerHeight()+$('#scrollzipPoint').offset().top-settings.hideShift)<($(this).offset().top+((settings.wholeVisible)?$(this).outerHeight():0))||
                    ($('#scrollzipPoint').offset().top+((settings.wholeVisible)?$(this).outerHeight():0))>($(this).outerHeight()+$(this).offset().top-settings.hideShift))
                ){
                    $(this).removeClass('isShown');
                    settings.hideFunction.call( this );
                }
            }
            return this;
        });
    };
    function numberRoller(slno){
            var min=$('.roller-title-number-'+slno).attr('data-min');
            var max=$('.roller-title-number-'+slno).attr('data-max');
            var timediff=$('.roller-title-number-'+slno).attr('data-delay');
            var increment=$('.roller-title-number-'+slno).attr('data-increment');
            var numdiff=max-min;
            var timeout=(timediff*1000)/numdiff;
            //if(numinc<10){
                //increment=Math.floor((timediff*1000)/10);
            //}//alert(increment);
            numberRoll(slno,min,max,increment,timeout);
            
    }
    function numberRoll(slno,min,max,increment,timeout){//alert(slno+"="+min+"="+max+"="+increment+"="+timeout);
        if(min<=max){
            var commaMin = min.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('.roller-title-number-'+slno).html(commaMin);
            min=parseInt(min)+parseInt(increment);
            setTimeout(function(){numberRoll(eval(slno),eval(min),eval(max),eval(increment),eval(timeout))},timeout);
        }else{
        	var commaMax = max.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('.roller-title-number-'+slno).html(commaMax);
        }
    }
})(jQuery);