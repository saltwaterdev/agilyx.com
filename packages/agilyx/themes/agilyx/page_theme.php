<?php 
namespace Concrete\Package\Agilyx\Theme\Agilyx;
use Concrete\Core\Page\Theme\Theme;
use Concrete\Core\Area\Layout\Preset\Provider\ThemeProviderInterface;
class PageTheme extends Theme implements ThemeProviderInterface {

	public function registerAssets() {

        $this->requireAsset('css', 'font-awesome');
       	$this->requireAsset('javascript', 'jquery');
        //$this->requireAsset('javascript', 'bootstrap/*');
	}

   	protected $pThemeGridFrameworkHandle = 'agilyx';

    public function getThemeBlockClasses(){
    		
    	// this adds custom classes as a dropdown option on blocks.
		return array(
            'social_links' => array("singleColor","flatFullColor","roundedGradient","roundedFlat"),
            'content' => array('blue-background')
        );
    }

    public function getThemeAreaClasses()
    {
       	// this adds custom classes to be added to an area.
	    /*
		return array(
            'Main' => array('area-content-accent')
        );
		*/
    }

    public function getThemeEditorClasses()
    {
        // classes available in WYSIWYG	        
        return array(
            array('title' => t('Site Title'), 'menuClass' => 'site-title', 'spanClass' => 'site-title'),
            array('title' => t('Blue Button'), 'menuClass' => 'blue-button', 'spanClass' => 'btn blue')
        );
		 
		 
    }
    
    public function getThemeAreaLayoutPresets()
	{
	    $presets = array(
	        array(
	            'handle' => 'wide_container',
	            'name' => 'Wide Container',
	            'container' => '<div class="row wide"></div>',
	            'columns' => array(
	                '<div class="col-sm-30"></div>'
	            ),
	        ),
	        array(
	            'handle' => 'two_col_left_sidebar',
	            'name' => 'Two Columns (Left Sidebar)',
	            'container' => '<div class=""></div>',
	            'columns' => array(
	                '<div class="columns col-sm-7 sidebar"></div>',
	                '<div class="columns col-sm-23 notsidebar"></div>'
	            ),
	        ),
	        array(
	            'handle' => 'three_col',
	            'name' => 'Three Columns',
	            'container' => '<div class=""></div>',
	            'columns' => array(
	                '<div class="columns col-sm-10"></div>',
	                '<div class="columns col-sm-10"></div>',
	                '<div class="columns col-sm-10"></div>'
	            ),
	        ),
	        array(
	            'handle' => 'three_col_padding',
	            'name' => 'Three Columns With Padding',
	            'container' => '<div class="with_padding"></div>',
	            'columns' => array(
	                '<div class="columns col-sm-10"></div>',
	                '<div class="columns col-sm-10"></div>',
	                '<div class="columns col-sm-10"></div>'
	            ),
	        ),
			array(
	            'handle' => 'four_col',
	            'name' => 'Four Columns',
	            'container' => '<div class=""></div>',
	            'columns' => array(
	                '<div class="columns col-sm-7 first"></div>',
	                '<div class="columns col-sm-7"></div>',
	                '<div class="columns col-sm-7"></div>',
					'<div class="columns col-sm-7 last"></div>'
	            ),
	        ),
                array(
	            'handle' => 'four_col_padding',
	            'name' => 'Four Columns With Padding',
	            'container' => '<div class="with_padding"></div>',
	            'columns' => array(
	                '<div class="columns col-sm-7 first"></div>',
	                '<div class="columns col-sm-7"></div>',
	                '<div class="columns col-sm-7"></div>',
					'<div class="columns col-sm-7 last"></div>'
	            ),
	        ),
	    );
	    return $presets;
	}

}
