<?php       

namespace Concrete\Package\Agilyx;
use Package;
use BlockType;
use SinglePage;
use PageTheme;
use BlockTypeSet;
use CollectionAttributeKey;
use Concrete\Core\Attribute\Type as AttributeType;
use Concrete\Package\Agilyx\Src\AgilyxGridFramework;
use Core;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package
{

	protected $pkgHandle = 'agilyx';
	protected $appVersionRequired = '5.7.1';
	protected $pkgVersion = '1.0';
	
	
	
	public function getPackageDescription()
	{
		return t("Barebone Theme to Place Content and Arrange IA");
	}

	public function getPackageName()
	{
		return t("Agilyx");
	}
	
	public function on_start()
	{
	    $manager = Core::make('manager/grid_framework');
	    $manager->extend('agilyx', function($app) {
	        return new AgilyxGridFramework();
	    });
	}
	
	public function install()
	{
		$pkg = parent::install();
		BlockTypeSet::add("agilyx","Agilyx", $pkg);
		PageTheme::add('agilyx', $pkg);				
		
		$imgAttr = AttributeType::getByHandle('image_file'); 
  		$thumb = CollectionAttributeKey::getByHandle('thumbnail'); 
		if(!is_object($thumb)) {
	     	CollectionAttributeKey::add($imgAttr, 
	     	array(
	     		'akHandle' => 'thumbnail', 
	     		'akName' => t('Thumbnail Image'), 
	     	),$pkg);
	  	}
	}
}
?>